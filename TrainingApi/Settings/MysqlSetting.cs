namespace TrainingApi.Settings
{
    public class MysqlSetting
    {
        public string host { get; set; } = "";
        public string user { get; set; }  = "";
        public string password { get; set; } = "";
        public string database { get; set; } = "";
        public string port { get; set; } = "";
        public string sslM { get; set; } = "";

        public MysqlSetting()
        {
            host = Environment.GetEnvironmentVariable("mysql_host");
            user = Environment.GetEnvironmentVariable("mysql_user");
            password = Environment.GetEnvironmentVariable("mysql_password");
            database = Environment.GetEnvironmentVariable("mysql_database");
            port = Environment.GetEnvironmentVariable("mysql_port");
            sslM = Environment.GetEnvironmentVariable("mysql_sslM");
        }

        public string GetConnectionString()
        {
            return string.Format("server={0}; port={1}; user id={2}; password={3}; database={4}; SslMode={5}; allowPublicKeyRetrieval=true; allowUserVariables=true", host, port, user, password, database, sslM);
        }
    }
}
