using System.Globalization;

namespace TrainingApi.Utils
{
    public class DayUtils
    {
        private TimeZoneInfo? timeZone = null;
        private DateTime nowUtc;

        public DayUtils()
        {
            timeZone = TimeZoneInfo.FindSystemTimeZoneById("Asia/Taipei");
            nowUtc = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone);
        }

        /// <summary>
        /// 取得完整日期
        /// </summary>
        /// <returns></returns>
        public string GetFullDate()
        {
            return nowUtc.ToString("yyyyMMdd", CultureInfo.GetCultureInfo("zh-TW"));
        }

        /// <summary>
        /// 取得完整時間
        /// </summary>
        /// <returns></returns>
        public string GetFullTime()
        {
            return nowUtc.ToString("HHmmss", CultureInfo.GetCultureInfo("zh-TW"));
        }
    }
}
