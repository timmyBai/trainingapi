namespace TrainingApi.Utils
{
    /// <summary>
    /// 常模類型判斷
    /// </summary>
    public class NormUtils
    {
        /// <summary>
        /// 男女 bmi 百分等級計算
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="bmi">bmi 指數</param>
        /// <returns></returns>
        public string BmiPercentageLevel(string gender, int age, double bmi)
        {
            string bmiLevel1 = "過輕";
            string bmiLevel2 = "稍輕";
            string bmiLevel3 = "適當";
            string bmiLevel4 = "稍重";
            string bmiLevel5 = "過重";

            // 男性中間百分比
            double middlePercentageMale_21_To_25_Level1 = (20.12 + 20.52) / 2.0;
            double middlePercentageMale_21_To_25_Level2 = (21.67 + 21.95) / 2.0;
            double middlePercentageMale_21_To_25_Level3 = (23.01 + 23.38) / 2.0;
            double middlePercentageMale_21_To_25_Level4 = (25.02 + 25.94) / 2.0;
            double middlePercentageMale_26_To_30_Level1 = (20.91 + 21.30) / 2.0;
            double middlePercentageMale_26_To_30_Level2 = (22.33 + 26.65) / 2.0;
            double middlePercentageMale_26_To_30_Level3 = (23.87 + 24.21) / 2.0;
            double middlePercentageMale_26_To_30_Level4 = (26.02 + 26.63) / 2.0;
            double middlePercentageMale_31_To_35_Level1 = (21.15 + 21.83) / 2.0;
            double middlePercentageMale_31_To_35_Level2 = (23.03 + 23.46) / 2.0;
            double middlePercentageMale_31_To_35_Level3 = (24.70 + 25.08) / 2.0;
            double middlePercentageMale_31_To_35_Level4 = (26.34 + 27.20) / 2.0;
            double middlePercentageMale_36_To_40_Level1 = (22.05 + 22.36) / 2.0;
            double middlePercentageMale_36_To_40_Level2 = (23.57 + 23.87) / 2.0;
            double middlePercentageMale_36_To_40_Level3 = (24.91 + 25.26) / 2.0;
            double middlePercentageMale_36_To_40_Level4 = (26.48 + 27.28) / 2.0;
            double middlePercentageMale_41_To_45_Level1 = (22.14 + 22.41) / 2.0;
            double middlePercentageMale_41_To_45_Level2 = (23.73 + 24.07) / 2.0;
            double middlePercentageMale_41_To_45_Level3 = (25.06 + 25.48) / 2.0;
            double middlePercentageMale_41_To_45_Level4 = (26.56 + 27.36) / 2.0;
            double middlePercentageMale_46_To_50_Level1 = (22.27 + 22.53) / 2.0;
            double middlePercentageMale_46_To_50_Level2 = (23.86 + 24.16) / 2.0;
            double middlePercentageMale_46_To_50_Level3 = (25.19 + 25.54) / 2.0;
            double middlePercentageMale_46_To_50_Level4 = (26.67 + 27.42) / 2.0;
            double middlePercentageMale_51_To_55_Level1 = (22.34 + 22.64) / 2.0;
            double middlePercentageMale_51_To_55_Level2 = (24.09 + 24.24) / 2.0;
            double middlePercentageMale_51_To_55_Level3 = (25.34 + 25.66) / 2.0;
            double middlePercentageMale_51_To_55_Level4 = (26.98 + 27.54) / 2.0;
            double middlePercentageMale_56_To_60_Level1 = (22.41 + 22.79) / 2.0;
            double middlePercentageMale_56_To_60_Level2 = (24.13 + 24.38) / 2.0;
            double middlePercentageMale_56_To_60_Level3 = (25.46 + 25.75) / 2.0;
            double middlePercentageMale_56_To_60_Level4 = (27.07 + 27.81) / 2.0;
            double middlePercentageMale_61_To_65_Level1 = (22.47 + 22.83) / 2.0;
            double middlePercentageMale_61_To_65_Level2 = (24.24 + 24.51) / 2.0;
            double middlePercentageMale_61_To_65_Level3 = (25.58 + 25.91) / 2.0;
            double middlePercentageMale_61_To_65_Level4 = (27.34 + 27.98) / 2.0;
            double middlePercentageMale_65_To_69_Level1 = (22.41 + 22.95) / 2.0;
            double middlePercentageMale_65_To_69_Level2 = (24.17 + 24.54) / 2.0;
            double middlePercentageMale_65_To_69_Level3 = (25.69 + 26.08) / 2.0;
            double middlePercentageMale_65_To_69_Level4 = (27.66 + 25.53) / 2.0;
            double middlePercentageMale_70_To_74_Level1 = (22.31 + 22.75) / 2.0;
            double middlePercentageMale_70_To_74_Level2 = (23.92 + 24.31) / 2.0;
            double middlePercentageMale_70_To_74_Level3 = (25.51 + 25.91) / 2.0;
            double middlePercentageMale_70_To_74_Level4 = (27.34 + 28.09) / 2.0;
            double middlePercentageMale_75_To_79_Level1 = (22.10 + 22.63) / 2.0;
            double middlePercentageMale_75_To_79_Level2 = (23.83 + 24.17) / 2.0;
            double middlePercentageMale_75_To_79_Level3 = (25.46 + 25.97) / 2.0;
            double middlePercentageMale_75_To_79_Level4 = (27.53 + 28.21) / 2.0;
            double middlePercentageMale_80_To_84_Level1 = (21.76 + 22.30) / 2.0;
            double middlePercentageMale_80_To_84_Level2 = (23.50 + 23.92) / 2.0;
            double middlePercentageMale_80_To_84_Level3 = (25.08 + 25.40) / 2.0;
            double middlePercentageMale_80_To_84_Level4 = (26.78 + 27.51) / 2.0;
            double middlePercentageMale_90OverLevel1 = (21.17 + 21.45) / 2.0;
            double middlePercentageMale_90OverLevel2 = (23.87 + 24.19) / 2.0;
            double middlePercentageMale_90OverLevel3 = (25.42 + 25.93) / 2.0;
            double middlePercentageMale_90OverLevel4 = (27.21 + 27.41) / 2.0;

            // 女性中間百分比
            double middlePercentageFemale_21_To_25Level1 = (18.63 + 18.71) / 2.0;
            double middlePercentageFemale_21_To_25Level2 = (19.84 + 19.90) / 2.0;
            double middlePercentageFemale_21_To_25Level3 = (21.17 + 22.14) / 2.0;
            double middlePercentageFemale_21_To_25Level4 = (22.69 + 23.02) / 2.0;
            double middlePercentageFemale_26_To_30Level1 = (18.75 + 19.14) / 2.0;
            double middlePercentageFemale_26_To_30Level2 = (20.04 + 20.32) / 2.0;
            double middlePercentageFemale_26_To_30Level3 = (21.64 + 22.38) / 2.0;
            double middlePercentageFemale_26_To_30Level4 = (22.83 + 23.39) / 2.0;
            double middlePercentageFemale_31_To_35Level1 = (19.45 + 19.76) / 2.0;
            double middlePercentageFemale_31_To_35Level2 = (20.68 + 20.99) / 2.0;
            double middlePercentageFemale_31_To_35Level3 = (21.85 + 22.68) / 2.0;
            double middlePercentageFemale_31_To_35Level4 = (23.84 + 24.55) / 2.0;
            double middlePercentageFemale_36_To_40Level1 = (20.00 + 20.28) / 2.0;
            double middlePercentageFemale_36_To_40Level2 = (21.23 + 21.62) / 2.0;
            double middlePercentageFemale_36_To_40Level3 = (22.55 + 22.92) / 2.0;
            double middlePercentageFemale_36_To_40Level4 = (24.41 + 25.16) / 2.0;
            double middlePercentageFemale_41_To_45Level1 = (20.45 + 20.81) / 2.0;
            double middlePercentageFemale_41_To_45Level2 = (21.81 + 22.18) / 2.0;
            double middlePercentageFemale_41_To_45Level3 = (23.22 + 23.53) / 2.0;
            double middlePercentageFemale_41_To_45Level4 = (25.30 + 25.98) / 2.0;
            double middlePercentageFemale_46_To_50Level1 = (21.09 + 21.50) / 2.0;
            double middlePercentageFemale_46_To_50Level2 = (22.51 + 22.86) / 2.0;
            double middlePercentageFemale_46_To_50Level3 = (23.96 + 24.53) / 2.0;
            double middlePercentageFemale_46_To_50Level4 = (25.91 + 26.55) / 2.0;
            double middlePercentageFemale_51_To_55Level1 = (21.64 + 21.99) / 2.0;
            double middlePercentageFemale_51_To_55Level2 = (23.07 + 23.40) / 2.0;
            double middlePercentageFemale_51_To_55Level3 = (24.47 + 24.75) / 2.0;
            double middlePercentageFemale_51_To_55Level4 = (26.22 + 26.65) / 2.0;
            double middlePercentageFemale_56_To_60Level1 = (21.72 + 22.22) / 2.0;
            double middlePercentageFemale_56_To_60Level2 = (23.30 + 23.62) / 2.0;
            double middlePercentageFemale_56_To_60Level3 = (24.69 + 25.15) / 2.0;
            double middlePercentageFemale_56_To_60Level4 = (26.80 + 27.70) / 2.0;
            double middlePercentageFemale_61_To_65Level1 = (22.21 + 22.49) / 2.0;
            double middlePercentageFemale_61_To_65Level2 = (23.72 + 24.12) / 2.0;
            double middlePercentageFemale_61_To_65Level3 = (25.30 + 25.56) / 2.0;
            double middlePercentageFemale_61_To_65Level4 = (27.22 + 27.86) / 2.0;
            double middlePercentageFemale_65_To_69Level1 = (21.80 + 22.35) / 2.0;
            double middlePercentageFemale_65_To_69Level2 = (23.66 + 24.06) / 2.0;
            double middlePercentageFemale_65_To_69Level3 = (25.34 + 25.81) / 2.0;
            double middlePercentageFemale_65_To_69Level4 = (27.63 + 28.45) / 2.0;
            double middlePercentageFemale_70_To_74Level1 = (22.07 + 22.64) / 2.0;
            double middlePercentageFemale_70_To_74Level2 = (23.83 + 24.34) / 2.0;
            double middlePercentageFemale_70_To_74Level3 = (25.64 + 26.11) / 2.0;
            double middlePercentageFemale_70_To_74Level4 = (27.83 + 28.69) / 2.0;
            double middlePercentageFemale_75_To_79Level1 = (21.91 + 22.44) / 2.0;
            double middlePercentageFemale_75_To_79Level2 = (23.92 + 24.34) / 2.0;
            double middlePercentageFemale_75_To_79Level3 = (25.78 + 26.23) / 2.0;
            double middlePercentageFemale_75_To_79Level4 = (27.89 + 28.72) / 2.0;
            double middlePercentageFemale_80_To_84Level1 = (21.36 + 22.04) / 2.0;
            double middlePercentageFemale_80_To_84Level2 = (23.46 + 23.80) / 2.0;
            double middlePercentageFemale_80_To_84Level3 = (25.07 + 25.57) / 2.0;
            double middlePercentageFemale_80_To_84Level4 = (27.33 + 28.25) / 2.0;
            double middlePercentageFemale_85_To_89Level1 = (21.31 + 21.72) / 2.0;
            double middlePercentageFemale_85_To_89Level2 = (22.83 + 23.20) / 2.0;
            double middlePercentageFemale_85_To_89Level3 = (24.46 + 24.89) / 2.0;
            double middlePercentageFemale_85_To_89Level4 = (26.53 + 27.11) / 2.0;
            double middlePercentageFemale_90OverLevel1 = (21.17 + 21.45) / 2.0;
            double middlePercentageFemale_90OverLevel2 = (23.87 + 24.19) / 2.0;
            double middlePercentageFemale_90OverLevel3 = (25.42 + 25.93) / 2.0;
            double middlePercentageFemale_90OverLevel4 = (27.21 + 27.41) / 2.0;

            if (gender == "男")
            {
                // 21 到 25 歲
                if (age >= 21 && age <= 25)
                {
                    if (bmi <= 20.12)
                    {
                        return bmiLevel1;
                    }

                    else if (bmi > 21.95 && bmi < 23.01)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 23.38 && bmi < 25.02)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 25.94)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 20.12 && bmi < middlePercentageMale_21_To_25_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_26_To_30_Level1 && bmi < 20.52)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 21.67 && bmi < middlePercentageMale_26_To_30_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 21.67 && bmi < middlePercentageMale_21_To_25_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_21_To_25_Level2 && bmi < 21.95)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 23.01 && bmi < middlePercentageMale_21_To_25_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_21_To_25_Level3 && bmi < 23.38)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 25.02 && bmi < middlePercentageMale_21_To_25_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_21_To_25_Level4 && bmi < 25.94)
                    {
                        return bmiLevel5;
                    }
                }
                // 26 到 30 歲
                else if (age >= 26 && age <= 30)
                {
                    if (bmi <= 20.91)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 21.30 && bmi < 22.33)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 22.65 && bmi < 23.87)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.21 && bmi < 26.02)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 26.63)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 20.91 && bmi < middlePercentageMale_26_To_30_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_26_To_30_Level1 && bmi < 21.30)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 22.33 && bmi < middlePercentageMale_26_To_30_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_26_To_30_Level2 && bmi < 22.65)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 23.87 && bmi < middlePercentageMale_26_To_30_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_26_To_30_Level3 && bmi < 24.21)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.02 && bmi < middlePercentageMale_26_To_30_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_26_To_30_Level4 && bmi < 26.63)
                    {
                        return bmiLevel5;
                    }
                }
                // 31 到 35 歲
                else if (age >= 31 && age <= 35)
                {
                    if (bmi <= 21.15)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 21.63 && bmi < 23.03)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.46 && bmi < 24.70)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.08 && bmi < 26.34)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.20)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.15 && bmi < middlePercentageMale_31_To_35_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_31_To_35_Level1 && bmi < 21.83)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.03 && bmi < middlePercentageMale_31_To_35_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_31_To_35_Level2 && bmi < 23.46)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.70 && bmi < middlePercentageMale_31_To_35_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_31_To_35_Level3 && bmi < 25.08)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.34 && bmi < middlePercentageMale_31_To_35_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_31_To_35_Level4 && bmi < 27.20)
                    {
                        return bmiLevel5;
                    }
                }
                // 36 到 40 歲
                else if (age >= 36 && age <= 40)
                {
                    if (bmi <= 22.05)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.36 && bmi < 23.57)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.87 && bmi < 24.91)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.26 && bmi < 26.48)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.28)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.05 && bmi < middlePercentageMale_36_To_40_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_36_To_40_Level1 && bmi < 22.36)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.57 && bmi < middlePercentageMale_36_To_40_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_36_To_40_Level2 && bmi < 23.87)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.91 && bmi < middlePercentageMale_36_To_40_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_36_To_40_Level3 && bmi < 25.26)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.48 && bmi < middlePercentageMale_36_To_40_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_36_To_40_Level4 && bmi < 27.28)
                    {
                        return bmiLevel5;
                    }
                }
                // 41 到 45 歲
                else if (age >= 41 && age <= 45)
                {
                    if (bmi <= 22.41)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.41 && bmi < 23.73)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.07 && bmi < 25.06)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.48 && bmi < 26.56)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.36)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.14 && bmi < middlePercentageMale_41_To_45_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_41_To_45_Level1 && bmi < 22.41)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.73 && bmi < middlePercentageMale_41_To_45_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_41_To_45_Level2 && bmi < 24.07)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.06 && bmi < middlePercentageMale_41_To_45_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_41_To_45_Level3 && bmi < 25.48)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.56 && bmi < middlePercentageMale_41_To_45_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_41_To_45_Level4 && bmi < 27.36)
                    {
                        return bmiLevel5;
                    }
                }
                // 46 到 50 歲
                else if (age >= 46 && age <= 50)
                {
                    if (bmi <= 22.27)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.53 && bmi < 23.86)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.16 && bmi < 25.19)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.54 && bmi < 26.67)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.42)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.27 && bmi < middlePercentageMale_46_To_50_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_46_To_50_Level1 && bmi < 22.53)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.86 && bmi < middlePercentageMale_46_To_50_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_46_To_50_Level2 && bmi < 24.16)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.19 && bmi < middlePercentageMale_46_To_50_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_46_To_50_Level3 && bmi < 25.54)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.67 && bmi < middlePercentageMale_46_To_50_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_46_To_50_Level4 && bmi < 27.42)
                    {
                        return bmiLevel5;
                    }
                }
                // 51 到 55 歲
                else if (age >= 51 && age <= 55)
                {
                    if (bmi <= 22.34)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.64 && bmi < 24.09)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.24 && bmi < 25.34)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.66 && bmi < 26.98)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.54)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.34 && bmi < middlePercentageMale_51_To_55_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_51_To_55_Level1 && bmi < 22.64)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.09 && bmi < middlePercentageMale_51_To_55_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_51_To_55_Level2 && bmi < 24.24)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.34 && bmi < middlePercentageMale_51_To_55_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_51_To_55_Level3 && bmi < 25.66)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.98 && bmi < middlePercentageMale_51_To_55_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_51_To_55_Level4 && bmi < 27.54)
                    {
                        return bmiLevel5;
                    }
                }
                // 56 到 60 歲
                else if (age >= 56 && age <= 60)
                {
                    if (bmi <= 22.41)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.79 && bmi < 24.13)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.38 && bmi < 25.46)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.75 && bmi < 27.07)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.81)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.41 && bmi < middlePercentageMale_56_To_60_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_56_To_60_Level1 && bmi < 22.79)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.13 && bmi < middlePercentageMale_56_To_60_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_56_To_60_Level2 && bmi < 24.38)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.46 && bmi < middlePercentageMale_56_To_60_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_56_To_60_Level3 && bmi < 25.75)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.07 && bmi < middlePercentageMale_56_To_60_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_56_To_60_Level4 && bmi < 27.81)
                    {
                        return bmiLevel5;
                    }
                }
                // 61 到 65 歲
                else if (age >= 61 && age <= 65)
                {
                    if (bmi <= 22.47)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.83 && bmi < 24.24)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.51 && bmi < 25.58)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.91 && bmi < 27.34)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.98)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.47 && bmi < middlePercentageMale_61_To_65_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_61_To_65_Level1 && bmi < 22.83)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.24 && bmi < middlePercentageMale_61_To_65_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_61_To_65_Level2 && bmi < 24.51)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.58 && bmi < middlePercentageMale_61_To_65_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_61_To_65_Level3 && bmi < 25.91)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.34 && bmi < middlePercentageMale_61_To_65_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_61_To_65_Level4 && bmi < 27.98)
                    {
                        return bmiLevel5;
                    }
                }
                // 65 歲以上 到 70  歲
                else if (age > 65 && age <= 69)
                {
                    if (bmi <= 22.41)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.95 && bmi < 24.17)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.54 && bmi < 25.69)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 26.08 && bmi < 27.66)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 28.53)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.41 && bmi < middlePercentageMale_65_To_69_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_65_To_69_Level1 && bmi < 22.95)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.17 && bmi < middlePercentageMale_65_To_69_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_65_To_69_Level2 && bmi < 24.54)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.69 && bmi < middlePercentageMale_65_To_69_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_65_To_69_Level3 && bmi < 26.08)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.66 && bmi < middlePercentageMale_65_To_69_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_65_To_69_Level4 && bmi < 28.83)
                    {
                        return bmiLevel5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (bmi <= 22.31)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.75 && bmi < 23.92)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.31 && bmi < 25.51)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.91 && bmi < 27.34)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 28.09)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.31 && bmi < middlePercentageMale_70_To_74_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_70_To_74_Level1 && bmi < 22.75)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.92 && bmi < middlePercentageMale_70_To_74_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_70_To_74_Level2 && bmi < 24.31)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.51 && bmi < middlePercentageMale_70_To_74_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_70_To_74_Level3 && bmi < 25.91)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.34 && bmi < middlePercentageMale_70_To_74_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_70_To_74_Level4 && bmi < 28.09)
                    {
                        return bmiLevel5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (bmi <= 22.10)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.63 && bmi < 23.83)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.17 && bmi < 25.46)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.97 && bmi < 27.53)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 28.21)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.10 && bmi < middlePercentageMale_75_To_79_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_75_To_79_Level1 && bmi < 22.63)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.83 && bmi < middlePercentageMale_75_To_79_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_75_To_79_Level2 && bmi < 24.17)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.46 && bmi < middlePercentageMale_75_To_79_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_75_To_79_Level3 && bmi < 25.97)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.53 && bmi < middlePercentageMale_75_To_79_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_75_To_79_Level4 && bmi < 28.21)
                    {
                        return bmiLevel5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (bmi <= 21.76)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.30 && bmi < 23.50)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.92 && bmi < 25.08)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.40 && bmi < 26.78)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.51)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.76 && bmi < middlePercentageMale_70_To_74_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_70_To_74_Level1 && bmi < 22.30)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.50 && bmi < middlePercentageMale_70_To_74_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_70_To_74_Level2 && bmi < 23.92)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.08 && bmi < middlePercentageMale_70_To_74_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_70_To_74_Level3 && bmi < 25.40)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.78 && bmi < middlePercentageMale_70_To_74_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_70_To_74_Level4 && bmi < 27.51)
                    {
                        return bmiLevel5;
                    }
                }
                // 85 到 89 歲
                else if (age > 85 && age <= 89)
                {
                    if (bmi <= 21.48)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 21.96 && bmi < 23.15)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.48 && bmi < 24.61)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.04 && bmi < 26.67)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.41)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.48 && bmi < middlePercentageMale_80_To_84_Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_80_To_84_Level1 && bmi < 21.96)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.15 && bmi < middlePercentageMale_80_To_84_Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_80_To_84_Level2 && bmi < 23.48)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.61 && bmi < middlePercentageMale_80_To_84_Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_80_To_84_Level3 && bmi < 25.04)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.67 && bmi < middlePercentageMale_80_To_84_Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_80_To_84_Level4 && bmi < 27.41)
                    {
                        return bmiLevel5;
                    }
                }
                // 90 歲以上
                else
                {
                    if (bmi <= 21.17)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 21.45 && bmi < 23.87)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.19 && bmi < 25.42)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.93 && bmi < 27.21)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.41)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.17 && bmi < middlePercentageMale_90OverLevel1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageMale_90OverLevel1 && bmi < 21.45)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.87 && bmi < middlePercentageMale_90OverLevel2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageMale_80_To_84_Level2 && bmi < 24.19)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.42 && bmi < middlePercentageMale_90OverLevel3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageMale_90OverLevel3 && bmi < 25.93)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.21 && bmi < middlePercentageMale_90OverLevel4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageMale_90OverLevel4 && bmi < 27.41)
                    {
                        return bmiLevel5;
                    }
                }
            }
            else
            {
                // 21 到 25 歲
                if (age >= 21 && age <= 25)
                {
                    if (bmi <= 18.63)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 18.71 && bmi < 19.84)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 19.90 && bmi < 21.17)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 22.14 && bmi < 22.69)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 23.02)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 18.63 && bmi < middlePercentageFemale_21_To_25Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_21_To_25Level1 && bmi < 18.71)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 19.84 && bmi < middlePercentageFemale_21_To_25Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_21_To_25Level2 && bmi < 19.90)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 21.17 && bmi < middlePercentageFemale_21_To_25Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_21_To_25Level3 && bmi < 22.14)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 22.69 && bmi < middlePercentageFemale_21_To_25Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_21_To_25Level4 && bmi < 23.02)
                    {
                        return bmiLevel5;
                    }
                }
                // 26 到 30 歲
                else if (age >= 26 && age <= 30)
                {
                    if (bmi <= 18.75)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 19.14 && bmi < 20.04)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 20.32 && bmi < 21.64)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 22.38 && bmi < 22.83)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 23.39)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 18.75 && bmi < middlePercentageFemale_26_To_30Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_26_To_30Level1 && bmi < 19.14)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 20.04 && bmi < middlePercentageFemale_26_To_30Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_26_To_30Level2 && bmi < 20.32)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 21.64 && bmi < middlePercentageFemale_26_To_30Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_26_To_30Level3 && bmi < 22.38)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 22.83 && bmi < middlePercentageFemale_26_To_30Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_26_To_30Level4 && bmi < 23.39)
                    {
                        return bmiLevel5;
                    }
                }
                // 31 到 35 歲
                else if (age >= 31 && age <= 35)
                {
                    if (bmi <= 19.45)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 19.76 && bmi < 20.68)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 20.99 && bmi < 21.85)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 22.68 && bmi < 23.84)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 24.55)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 19.45 && bmi < middlePercentageFemale_31_To_35Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_31_To_35Level1 && bmi < 19.76)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 20.68 && bmi < middlePercentageFemale_31_To_35Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_31_To_35Level2 && bmi < 20.99)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 21.85 && bmi < middlePercentageFemale_31_To_35Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_31_To_35Level3 && bmi < 22.68)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 23.84 && bmi < middlePercentageFemale_31_To_35Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_31_To_35Level4 && bmi < 24.55)
                    {
                        return bmiLevel5;
                    }
                }
                // 36 到 40 歲
                else if (age >= 36 && age <= 40)
                {
                    if (bmi <= 20.00)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 20.28 && bmi < 21.23)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 21.62 && bmi < 22.55)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 22.92 && bmi < 24.41)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 25.16)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 20.00 && bmi < middlePercentageFemale_36_To_40Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_36_To_40Level1 && bmi < 20.28)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 21.23 && bmi < middlePercentageFemale_36_To_40Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_36_To_40Level2 && bmi < 21.62)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 22.55 && bmi < middlePercentageFemale_36_To_40Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_36_To_40Level3 && bmi < 22.92)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 24.41 && bmi < middlePercentageFemale_36_To_40Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_36_To_40Level4 && bmi < 25.16)
                    {
                        return bmiLevel5;
                    }
                }
                // 41 到 45 歲
                else if (age >= 41 && age <= 45)
                {
                    if (bmi <= 20.45)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 20.81 && bmi < 21.81)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 22.18 && bmi < 23.22)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 23.52 && bmi < 25.30)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 25.98)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 20.45 && bmi < middlePercentageFemale_41_To_45Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_41_To_45Level1 && bmi < 20.81)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 21.81 && bmi < middlePercentageFemale_41_To_45Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_41_To_45Level2 && bmi < 22.18)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 23.22 && bmi < middlePercentageFemale_41_To_45Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_41_To_45Level3 && bmi < 23.53)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 25.30 && bmi < middlePercentageFemale_41_To_45Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_41_To_45Level4 && bmi < 25.98)
                    {
                        return bmiLevel5;
                    }
                }
                // 46 到 50 歲
                else if (age >= 46 && age <= 50)
                {
                    if (bmi <= 21.09)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 21.50 && bmi < 22.51)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 22.86 && bmi < 23.96)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.53 && bmi < 25.91)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 26.55)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.09 && bmi < middlePercentageFemale_46_To_50Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_46_To_50Level1 && bmi < 21.50)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 22.51 && bmi < middlePercentageFemale_46_To_50Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_46_To_50Level2 && bmi < 22.86)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 23.96 && bmi < middlePercentageFemale_46_To_50Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_46_To_50Level3 && bmi < 24.53)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 25.91 && bmi < middlePercentageFemale_46_To_50Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_46_To_50Level4 && bmi < 26.55)
                    {
                        return bmiLevel5;
                    }
                }
                // 51 到 55 歲
                else if (age >= 51 && age <= 55)
                {
                    if (bmi <= 21.64)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 21.99 && bmi < 23.07)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.40 && bmi < 24.47)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.75 && bmi < 26.22)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 26.65)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.64 && bmi < middlePercentageFemale_51_To_55Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_51_To_55Level1 && bmi < 21.99)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.07 && bmi < middlePercentageFemale_51_To_55Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_51_To_55Level2 && bmi < 23.40)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.47 && bmi < middlePercentageFemale_51_To_55Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_51_To_55Level3 && bmi < 24.75)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.22 && bmi < middlePercentageFemale_51_To_55Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_51_To_55Level4 && bmi < 26.65)
                    {
                        return bmiLevel5;
                    }
                }
                // 56 到 60 歲
                else if (age >= 56 && age <= 60)
                {
                    if (bmi <= 21.72)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.22 && bmi < 23.30)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.62 && bmi < 24.69)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.15 && bmi < 26.80)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.70)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.72 && bmi < middlePercentageFemale_56_To_60Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_56_To_60Level1 && bmi < 22.22)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.30 && bmi < middlePercentageFemale_56_To_60Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_56_To_60Level2 && bmi < 23.62)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.69 && bmi < middlePercentageFemale_56_To_60Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_56_To_60Level3 && bmi < 25.15)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.80 && bmi < middlePercentageFemale_56_To_60Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_56_To_60Level4 && bmi < 27.70)
                    {
                        return bmiLevel5;
                    }
                }
                // 60 到 65 歲
                else if (age >= 61 && age <= 65)
                {
                    if (bmi <= 22.21)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.49 && bmi < 23.72)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.12 && bmi < 25.30)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.56 && bmi < 27.22)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.86)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.21 && bmi < middlePercentageFemale_61_To_65Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_61_To_65Level1 && bmi < 22.49)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.72 && bmi < middlePercentageFemale_61_To_65Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_61_To_65Level2 && bmi < 24.12)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.30 && bmi < middlePercentageFemale_61_To_65Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_61_To_65Level3 && bmi < 25.56)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.22 && bmi < middlePercentageFemale_61_To_65Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_61_To_65Level4 && bmi < 27.86)
                    {
                        return bmiLevel5;
                    }
                }
                // 65 歲以上到 69 歲
                else if (age > 65 && age <= 69)
                {
                    if (bmi <= 21.80)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.35 && bmi < 23.66)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.06 && bmi < 25.34)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.81 && bmi < 27.63)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 28.45)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.80 && bmi < middlePercentageFemale_65_To_69Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_65_To_69Level1 && bmi < 22.35)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.66 && bmi < middlePercentageFemale_65_To_69Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_65_To_69Level2 && bmi < 24.06)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.34 && bmi < middlePercentageFemale_65_To_69Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_65_To_69Level3 && bmi < 25.81)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.63 && bmi < middlePercentageFemale_65_To_69Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_65_To_69Level4 && bmi < 28.45)
                    {
                        return bmiLevel5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (bmi <= 22.07)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.64 && bmi < 23.83)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.34 && bmi < 25.64)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 26.11 && bmi < 27.83)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 28.69)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 22.07 && bmi < middlePercentageFemale_70_To_74Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_70_To_74Level1 && bmi < 22.64)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.83 && bmi < middlePercentageFemale_70_To_74Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_70_To_74Level2 && bmi < 24.34)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.64 && bmi < middlePercentageFemale_70_To_74Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_70_To_74Level3 && bmi < 26.11)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.83 && bmi < middlePercentageFemale_70_To_74Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_70_To_74Level4 && bmi < 28.69)
                    {
                        return bmiLevel5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (bmi <= 21.91)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.44 && bmi < 23.92)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 24.34 && bmi < 25.78)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 26.23 && bmi < 27.89)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 28.72)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.91 && bmi < middlePercentageFemale_75_To_79Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_75_To_79Level1 && bmi < 22.44)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.92 && bmi < middlePercentageFemale_75_To_79Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_75_To_79Level2 && bmi < 24.34)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.78 && bmi < middlePercentageFemale_75_To_79Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_75_To_79Level3 && bmi < 26.23)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.89 && bmi < middlePercentageFemale_75_To_79Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_75_To_79Level4 && bmi < 28.72)
                    {
                        return bmiLevel5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (bmi <= 21.36)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 22.04 && bmi < 23.46)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.80 && bmi < 25.07)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.57 && bmi < 27.33)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 28.25)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.36 && bmi < middlePercentageFemale_80_To_84Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_80_To_84Level1 && bmi < 22.04)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.46 && bmi < middlePercentageFemale_80_To_84Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_80_To_84Level2 && bmi < 23.80)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 25.07 && bmi < middlePercentageFemale_80_To_84Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_80_To_84Level3 && bmi < 25.57)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 27.33 && bmi < middlePercentageFemale_80_To_84Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_80_To_84Level4 && bmi < 28.25)
                    {
                        return bmiLevel5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (bmi <= 21.31)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 21.72 && bmi < 22.83)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 23.20 && bmi < 24.46)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.89 && bmi < 26.53)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.11)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 21.31 && bmi < middlePercentageFemale_85_To_89Level1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_85_To_89Level1 && bmi < 21.72)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 22.83 && bmi < middlePercentageFemale_85_To_89Level2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_85_To_89Level2 && bmi < 23.20)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.46 && bmi < middlePercentageFemale_85_To_89Level3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_85_To_89Level3 && bmi < 24.89)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.53 && bmi < middlePercentageFemale_85_To_89Level4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_85_To_89Level4 && bmi < 27.11)
                    {
                        return bmiLevel5;
                    }
                }
                // 90 歲以上
                else
                {
                    if (bmi <= 19.91)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > 20.67 && bmi < 21.96)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 22.34 && bmi < 23.77)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 24.08 && bmi < 26.25)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi >= 27.27)
                    {
                        return bmiLevel5;
                    }


                    else if (bmi > 19.91 && bmi < middlePercentageFemale_90OverLevel1)
                    {
                        return bmiLevel1;
                    }
                    else if (bmi > middlePercentageFemale_90OverLevel1 && bmi < 20.67)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > 21.96 && bmi < middlePercentageFemale_90OverLevel2)
                    {
                        return bmiLevel2;
                    }
                    else if (bmi > middlePercentageFemale_90OverLevel2 && bmi < 22.34)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > 23.77 && bmi < middlePercentageFemale_90OverLevel3)
                    {
                        return bmiLevel3;
                    }
                    else if (bmi > middlePercentageFemale_90OverLevel3 && bmi < 24.08)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > 26.25 && bmi < middlePercentageFemale_90OverLevel4)
                    {
                        return bmiLevel4;
                    }
                    else if (bmi > middlePercentageFemale_90OverLevel4 && bmi < 27.27)
                    {
                        return bmiLevel5;
                    }
                }
            }

            return "";
        }

        /// <summary>
        /// 男女 體指率百分等級計算
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="bmi">bmi 指數</param>
        /// <returns></returns>
        public string BodyFatPercentageLevel(string gender, int age, double bmi)
        {
            string bodyFatLevel1 = "非常好";
            string bodyFatLevel2 = "很好";
            string bodyFatLevel3 = "普通";
            string bodyFatLevel4 = "過重";
            string bodyFatLevel5 = "肥胖";

            if (gender == "男")
            {
                int maleBodyFatFormula = Convert.ToInt32((1.2 * bmi) + (0.23 * age) - 16.2);

                // 20 到 29 歲
                if (age >= 20 && age <= 29)
                {
                    if (maleBodyFatFormula <= 13.0)
                    {
                        return bodyFatLevel1;
                    }
                    else if (maleBodyFatFormula >= 13.1 && maleBodyFatFormula <= 18.0)
                    {
                        return bodyFatLevel2;
                    }
                    else if (maleBodyFatFormula >= 18.1 && maleBodyFatFormula <= 23.0)
                    {
                        return bodyFatLevel3;
                    }
                    else if (maleBodyFatFormula >= 23.1 && maleBodyFatFormula <= 28.0)
                    {
                        return bodyFatLevel4;
                    }
                    else if (maleBodyFatFormula >= 27.1)
                    {
                        return bodyFatLevel5;
                    }
                }
                // 30 到 39 歲
                else if (age >= 30 && age <= 39)
                {
                    if (maleBodyFatFormula <= 14.0)
                    {
                        return bodyFatLevel1;
                    }
                    else if (maleBodyFatFormula >= 14.1 && maleBodyFatFormula <= 19.0)
                    {
                        return bodyFatLevel2;
                    }
                    else if (maleBodyFatFormula >= 19.1 && maleBodyFatFormula <= 24.0)
                    {
                        return bodyFatLevel3;
                    }
                    else if (maleBodyFatFormula >= 24.1 && maleBodyFatFormula <= 29.0)
                    {
                        return bodyFatLevel4;
                    }
                    else if (maleBodyFatFormula >= 29.1)
                    {
                        return bodyFatLevel5;
                    }
                }
                // 40 到 49 歲
                else if (age > 40 && age <= 49)
                {
                    if (maleBodyFatFormula <= 15.0)
                    {
                        return bodyFatLevel1;
                    }
                    else if (maleBodyFatFormula >= 15.1 && maleBodyFatFormula <= 20.0)
                    {
                        return bodyFatLevel2;
                    }
                    else if (maleBodyFatFormula >= 20.1 && maleBodyFatFormula <= 25.0)
                    {
                        return bodyFatLevel3;
                    }
                    else if (maleBodyFatFormula >= 25.1 && maleBodyFatFormula <= 30.0)
                    {
                        return bodyFatLevel4;
                    }
                    else if (maleBodyFatFormula >= 30.1)
                    {
                        return bodyFatLevel5;
                    }
                }
                // 50 歲以上
                else if (age >= 50)
                {
                    if (maleBodyFatFormula <= 17.0)
                    {
                        return bodyFatLevel1;
                    }
                    else if (maleBodyFatFormula >= 16.1 && maleBodyFatFormula <= 21.0)
                    {
                        return bodyFatLevel2;
                    }
                    else if (maleBodyFatFormula >= 21.1 && maleBodyFatFormula <= 26.0)
                    {
                        return bodyFatLevel3;
                    }
                    else if (maleBodyFatFormula >= 26.1 && maleBodyFatFormula <= 31.0)
                    {
                        return bodyFatLevel4;
                    }
                    else if (maleBodyFatFormula >= 31.1)
                    {
                        return bodyFatLevel5;
                    }
                }
            }
            else
            {
                double femaleBodyFatFormula = (1.2 * bmi) + (0.23 * age) - 5.4;

                // 20 到 29 歲
                if (age >= 20 && age <= 29)
                {
                    if (femaleBodyFatFormula <= 18.0)
                    {
                        return bodyFatLevel1;
                    }
                    else if (femaleBodyFatFormula >= 18.1 && femaleBodyFatFormula <= 23.0)
                    {
                        return bodyFatLevel2;
                    }
                    else if (femaleBodyFatFormula >= 23.1 && femaleBodyFatFormula <= 28.0)
                    {
                        return bodyFatLevel3;
                    }
                    else if (femaleBodyFatFormula >= 28.1 && femaleBodyFatFormula <= 33.0)
                    {
                        return bodyFatLevel4;
                    }
                    else if (femaleBodyFatFormula >= 33.1)
                    {
                        return bodyFatLevel5;
                    }
                }
                // 30 到 39 歲
                else if (age >= 30 && age <= 39)
                {
                    if (femaleBodyFatFormula <= 19.0)
                    {
                        return bodyFatLevel1;
                    }
                    else if (femaleBodyFatFormula >= 19.1 && femaleBodyFatFormula <= 24.0)
                    {
                        return bodyFatLevel2;
                    }
                    else if (femaleBodyFatFormula >= 24.1 && femaleBodyFatFormula <= 29.0)
                    {
                        return bodyFatLevel3;
                    }
                    else if (femaleBodyFatFormula >= 29.1 && femaleBodyFatFormula <= 34.0)
                    {
                        return bodyFatLevel4;
                    }
                    else if (femaleBodyFatFormula >= 34.1)
                    {
                        return bodyFatLevel5;
                    }
                }
                // 40 到 49 歲
                else if (age >= 40 && age <= 49)
                {
                    if (femaleBodyFatFormula <= 20.0)
                    {
                        return bodyFatLevel1;
                    }
                    else if (femaleBodyFatFormula >= 20.1 && femaleBodyFatFormula <= 25.0)
                    {
                        return bodyFatLevel2;
                    }
                    else if (femaleBodyFatFormula >= 25.1 && femaleBodyFatFormula <= 30.0)
                    {
                        return bodyFatLevel3;
                    }
                    else if (femaleBodyFatFormula >= 30.1 && femaleBodyFatFormula <= 35.0)
                    {
                        return bodyFatLevel4;
                    }
                    else if (femaleBodyFatFormula >= 35.1)
                    {
                        return bodyFatLevel5;
                    }
                }
                // 50 歲以上
                else if (age >= 50)
                {
                    if (femaleBodyFatFormula <= 21.0)
                    {
                        return bodyFatLevel1;
                    }
                    else if (femaleBodyFatFormula >= 21.1 && femaleBodyFatFormula <= 26.0)
                    {
                        return bodyFatLevel2;
                    }
                    else if (femaleBodyFatFormula >= 26.1 && femaleBodyFatFormula <= 31.0)
                    {
                        return bodyFatLevel3;
                    }
                    else if (femaleBodyFatFormula >= 31.1 && femaleBodyFatFormula <= 36.0)
                    {
                        return bodyFatLevel4;
                    }
                    else if (femaleBodyFatFormula >= 36.1)
                    {
                        return bodyFatLevel5;
                    }
                }
            }

            return "";
        }

        /// <summary>
        /// 內臟脂肪 百分等級計算
        /// </summary>
        /// <param name="visceralFat">內臟脂肪</param>
        /// <returns></returns>
        public string VisceralFatPercentageLevel(double visceralFat)
        {
            string visceralFatLevel1 = "標準";
            string visceralFatLevel2 = "過量";
            string visceralFatLevel3 = "危險";
            double middleVisceralFatLevel1 = (9.5 + 9.0) / 2.0;
            double middleVisceralFatLevel2 = (15.0 - 14.5) / 2.0;

            if (visceralFat >= 1 && visceralFat <= 9)
            {
                return visceralFatLevel1;
            }
            else if (visceralFat >= 9.5 && visceralFat < 14.5)
            {
                return visceralFatLevel2;
            }
            else if (visceralFat > 15.0)
            {
                return visceralFatLevel3;
            }
            else if (visceralFat > 9 && visceralFat < middleVisceralFatLevel1)
            {
                return visceralFatLevel1;
            }
            else if (visceralFat > middleVisceralFatLevel1 && visceralFat <= 9.5)
            {
                return visceralFatLevel2;
            }
            else if (visceralFat > 14.5 && visceralFat < middleVisceralFatLevel2)
            {
                return visceralFatLevel2;
            }
            else if (visceralFat > middleVisceralFatLevel2 && visceralFat <= 15)
            {
                return visceralFatLevel3;
            }

            return "";
        }

        /// <summary>
        /// 骨骼心肌率
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="skeletalMuscleRate">骨骼心肌率</param>
        /// <returns></returns>
        public string SkeletalMuscleRatePercentageLevel(string gender, double skeletalMuscleRate)
        {
            string skeletalMuscleRateLevel = "低";
            string skeletalMuscleRateLeve2 = "標準";
            string skeletalMuscleRateLeve3 = "偏高";
            string skeletalMuscleRateLeve4 = "高";

            if (gender == "男")
            {
                if (skeletalMuscleRate <= 32.8)
                {
                    return skeletalMuscleRateLevel;
                }
                else if (skeletalMuscleRate >= 32.9 && skeletalMuscleRate <= 35.7)
                {
                    return skeletalMuscleRateLeve2;
                }
                else if (skeletalMuscleRate >= 35.8 && skeletalMuscleRate <= 37.3)
                {
                    return skeletalMuscleRateLeve3;
                }
                else if (skeletalMuscleRate >= 37.4)
                {
                    return skeletalMuscleRateLeve4;
                }
            }
            else
            {
                if (skeletalMuscleRate <= 25.8)
                {
                    return skeletalMuscleRateLevel;
                }
                else if (skeletalMuscleRate >= 25.9 && skeletalMuscleRate <= 27.9)
                {
                    return skeletalMuscleRateLeve2;
                }
                else if (skeletalMuscleRate >= 28.0 && skeletalMuscleRate <= 29.0)
                {
                    return skeletalMuscleRateLeve3;
                }
                else if (skeletalMuscleRate >= 29.1)
                {
                    return skeletalMuscleRateLeve4;
                }
            }

            return "";
        }

        /// <summary>
        /// 體適能-心肺適能(登階測驗)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="setUpTest">登階測驗體力指數</param>
        /// <returns></returns>
        public int PhysicalFitnessSetUpTestLevel(string gender, int age, double setUpTest)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;
            
            // 男性中間百分比
            double middlePercentageMale21_To_25Level1 = (49.34 + 50.56) / 2.0;
            double middlePercentageMale21_To_25Level2 = (53.57 + 54.55) / 2.0;
            double middlePercentageMale21_To_25Level3 = (57.69 + 58.82) / 2.0;
            double middlePercentageMale21_To_25Level4 = (63.38 + 65.69) / 2.0;
            double middlePercentageMale26_To_30Level1 = (48.39 + 49.72) / 2.0;
            double middlePercentageMale26_To_30Level2 = (52.94 + 54.22) / 2.0;
            double middlePercentageMale26_To_30Level3 = (57.32 + 58.52) / 2.0;
            double middlePercentageMale26_To_30Level4 = (62.67 + 65.22) / 2.0;
            double middlePercentageMale31_To_35Level1 = (49.45 + 50.56) / 2.0;
            double middlePercentageMale31_To_35Level2 = (53.57 + 54.55) / 2.0;
            double middlePercentageMale31_To_35Level3 = (57.32 + 58.82) / 2.0;
            double middlePercentageMale31_To_35Level4 = (63.38 + 65.38) / 2.0;
            double middlePercentageMale36_To_40Level1 = (49.72 + 51.14) / 2.0;
            double middlePercentageMale36_To_40Level2 = (54.22 + 55.21) / 2.0;
            double middlePercentageMale36_To_40Level3 = (57.69 + 58.82) / 2.0;
            double middlePercentageMale36_To_40Level4 = (63.83 + 66.18) / 2.0;
            double middlePercentageMale41_To_45Level1 = (50.00 + 51.14) / 2.0;
            double middlePercentageMale41_To_45Level2 = (54.55 + 55.56) / 2.0;
            double middlePercentageMale41_To_45Level3 = (58.06 + 59.21) / 2.0;
            double middlePercentageMale41_To_45Level4 = (63.83 + 66.44) / 2.0;
            double middlePercentageMale46_To_50Level1 = (49.18 + 50.56) / 2.0;
            double middlePercentageMale46_To_50Level2 = (53.57 + 54.84) / 2.0;
            double middlePercentageMale46_To_50Level3 = (58.64 + 59.60) / 2.0;
            double middlePercentageMale46_To_50Level4 = (63.83 + 66.67) / 2.0;
            double middlePercentageMale51_To_55Level1 = (49.18 + 50.56) / 2.0;
            double middlePercentageMale51_To_55Level2 = (53.89 + 54.88) / 2.0;
            double middlePercentageMale51_To_55Level3 = (58.82 + 60.40) / 2.0;
            double middlePercentageMale51_To_55Level4 = (63.83 + 67.16) / 2.0;
            double middlePercentageMale55_To_60Level1 = (48.13 + 50.35) / 2.0;
            double middlePercentageMale55_To_60Level2 = (54.39 + 55.56) / 2.0;
            double middlePercentageMale55_To_60Level3 = (59.60 + 60.81) / 2.0;
            double middlePercentageMale55_To_60Level4 = (65.22 + 68.31) / 2.0;
            double middlePercentageMale61_To_65Level1 = (45.51 + 49.86) / 2.0;
            double middlePercentageMale61_To_65Level2 = (54.02 + 55.69) / 2.0;
            double middlePercentageMale61_To_65Level3 = (60.00 + 61.64) / 2.0;
            double middlePercentageMale61_To_65Level4 = (67.47 + 69.28) / 2.0;

            // 女性中間百分比
            double middlePercentageFemale_21_To_25Level1 = (47.87 + 49.18) / 2.0;
            double middlePercentageFemale_21_To_25Level2 = (51.72 + 52.94) / 2.0;
            double middlePercentageFemale_21_To_25Level3 = (56.25 + 57.32) / 2.0;
            double middlePercentageFemale_21_To_25Level4 = (61.22 + 63.38) / 2.0;
            double middlePercentageFemale_26_To_30Level1 = (49.72 + 50.78) / 2.0;
            double middlePercentageFemale_26_To_30Level2 = (53.57 + 54.55) / 2.0;
            double middlePercentageFemale_26_To_30Level3 = (57.32 + 58.44) / 2.0;
            double middlePercentageFemale_26_To_30Level4 = (62.50 + 64.75) / 2.0;
            double middlePercentageFemale_31_To_35Level1 = (49.72 + 50.64) / 2.0;
            double middlePercentageFemale_31_To_35Level2 = (53.57 + 54.55) / 2.0;
            double middlePercentageFemale_31_To_35Level3 = (57.32 + 58.82) / 2.0;
            double middlePercentageFemale_31_To_35Level4 = (62.50 + 64.29) / 2.0;
            double middlePercentageFemale_36_To_40Level1 = (49.43 + 50.56) / 2.0;
            double middlePercentageFemale_36_To_40Level2 = (53.25 + 54.55) / 2.0;
            double middlePercentageFemale_36_To_40Level3 = (57.69 + 58.82) / 2.0;
            double middlePercentageFemale_36_To_40Level4 = (62.94 + 64.29) / 2.0;
            double middlePercentageFemale_41_To_45Level1 = (49.34 + 50.28) / 2.0;
            double middlePercentageFemale_41_To_45Level2 = (53.19 + 54.88) / 2.0;
            double middlePercentageFemale_41_To_45Level3 = (58.06 + 59.21) / 2.0;
            double middlePercentageFemale_41_To_45Level4 = (63.38 + 65.65) / 2.0;
            double middlePercentageFemale_46_To_50Level1 = (48.91 + 50.28) / 2.0;
            double middlePercentageFemale_46_To_50Level2 = (53.19 + 54.88) / 2.0;
            double middlePercentageFemale_46_To_50Level3 = (58.82 + 60.00) / 2.0;
            double middlePercentageFemale_46_To_50Level4 = (64.29 + 66.67) / 2.0;
            double middlePercentageFemale_51_To_55Level1 = (46.18 + 49.05) / 2.0;
            double middlePercentageFemale_51_To_55Level2 = (53.08 + 54.91) / 2.0;
            double middlePercentageFemale_51_To_55Level3 = (59.60 + 60.40) / 2.0;
            double middlePercentageFemale_51_To_55Level4 = (65.69 + 68.34) / 2.0;
            double middlePercentageFemale_56_To_60Level1 = (45.99 + 48.91) / 2.0;
            double middlePercentageFemale_56_To_60Level2 = (52.78 + 55.48) / 2.0;
            double middlePercentageFemale_56_To_60Level3 = (60.00 + 61.71) / 2.0;
            double middlePercentageFemale_56_To_60Level4 = (66.67 + 69.23) / 2.0;
            double middlePercentageFemale_61_To_65Level1 = (39.12 + 44.45) / 2.0;
            double middlePercentageFemale_61_To_65Level2 = (52.45 + 56.96) / 2.0;
            double middlePercentageFemale_61_To_65Level3 = (60.81 + 62.07) / 2.0;
            double middlePercentageFemale_61_To_65Level4 = (68.60 + 70.51) / 2.0;

            if (gender == "男")
            {
                // 21 到 25 歲
                if (age >= 21 && age <= 25)
                {
                    if (setUpTest <= 49.34)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.56 && setUpTest <= 53.57)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.55 && setUpTest <= 57.69)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 58.82 && setUpTest <= 63.38)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 65.69)
                    {
                        return level5;
                    }


                    else if (setUpTest > 49.34 && setUpTest < middlePercentageMale21_To_25Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageMale21_To_25Level1 && setUpTest < 50.56)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.57 && setUpTest < middlePercentageMale21_To_25Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageMale21_To_25Level2 && setUpTest < 54.55)
                    {
                        return level3;
                    }
                    else if (setUpTest > 57.69 && setUpTest < middlePercentageMale21_To_25Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageMale21_To_25Level3 && setUpTest < 58.82)
                    {
                        return level4;
                    }
                    else if (setUpTest > 63.38 && setUpTest < middlePercentageMale21_To_25Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageMale21_To_25Level4 && setUpTest < 65.69)
                    {
                        return level5;
                    }
                }
                // 26 到 30 歲
                else if (age >= 26 && age <= 30)
                {
                    if (setUpTest <= 48.39)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 49.72 && setUpTest <= 52.94)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.22 && setUpTest <= 57.32)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 58.52 && setUpTest <= 62.67)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 65.22)
                    {
                        return level5;
                    }


                    else if (setUpTest > 48.39 && setUpTest < middlePercentageMale26_To_30Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageMale26_To_30Level1 && setUpTest < 49.72)
                    {
                        return level2;
                    }
                    else if (setUpTest > 52.94 && setUpTest < middlePercentageMale26_To_30Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageMale26_To_30Level2 && setUpTest < 54.22)
                    {
                        return level3;
                    }
                    else if (setUpTest > 57.32 && setUpTest < middlePercentageMale26_To_30Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageMale26_To_30Level3 && setUpTest < 58.52)
                    {
                        return level4;
                    }
                    else if (setUpTest > 62.67 && setUpTest < middlePercentageMale26_To_30Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageMale26_To_30Level4 && setUpTest < 65.22)
                    {
                        return level5;
                    }
                }
                // 31 到 35 歲
                else if (age >= 31 && age <= 35)
                {
                    if (setUpTest <= 49.45)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.56 && setUpTest <= 53.57)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.55 && setUpTest <= 57.32)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 58.82 && setUpTest <= 63.38)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 65.38)
                    {
                        return level5;
                    }


                    else if (setUpTest > 49.45 && setUpTest < middlePercentageMale31_To_35Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageMale31_To_35Level1 && setUpTest < 50.56)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.57 && setUpTest < middlePercentageMale31_To_35Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageMale31_To_35Level2 && setUpTest < 54.55)
                    {
                        return level3;
                    }
                    else if (setUpTest > 57.32 && setUpTest < middlePercentageMale31_To_35Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageMale31_To_35Level3 && setUpTest < 58.82)
                    {
                        return level4;
                    }
                    else if (setUpTest > 63.38 && setUpTest < middlePercentageMale31_To_35Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageMale31_To_35Level4 && setUpTest < 65.38)
                    {
                        return level5;
                    }
                }
                // 36 到 40 歲
                else if (age >= 36 && age <= 40)
                {
                    if (setUpTest <= 49.72)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 51.14 && setUpTest <= 54.22)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 55.21 && setUpTest <= 57.69)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 58.82 && setUpTest <= 63.83)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 66.18)
                    {
                        return level5;
                    }


                    else if (setUpTest > 49.72 && setUpTest < middlePercentageMale36_To_40Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageMale36_To_40Level1 && setUpTest < 51.14)
                    {
                        return level2;
                    }
                    else if (setUpTest > 54.22 && setUpTest < middlePercentageMale36_To_40Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageMale36_To_40Level2 && setUpTest < 55.21)
                    {
                        return level3;
                    }
                    else if (setUpTest > 57.69 && setUpTest < middlePercentageMale36_To_40Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageMale36_To_40Level3 && setUpTest < 58.82)
                    {
                        return level4;
                    }
                    else if (setUpTest > 63.83 && setUpTest < middlePercentageMale36_To_40Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageMale36_To_40Level4 && setUpTest < 66.18)
                    {
                        return level5;
                    }
                }
                // 41 到 45 歲
                else if (age >= 41 && age <= 45)
                {
                    if (setUpTest <= 50.00)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 51.14 && setUpTest <= 54.55)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 55.56 && setUpTest <= 58.06)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 59.21 && setUpTest <= 63.83)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 66.44)
                    {
                        return level5;
                    }


                    else if (setUpTest > 50.00 && setUpTest < middlePercentageMale41_To_45Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageMale41_To_45Level1 && setUpTest < 51.14)
                    {
                        return level2;
                    }
                    else if (setUpTest > 54.55 && setUpTest < middlePercentageMale41_To_45Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageMale41_To_45Level2 && setUpTest < 55.56)
                    {
                        return level3;
                    }
                    else if (setUpTest > 58.06 && setUpTest < middlePercentageMale41_To_45Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageMale41_To_45Level3 && setUpTest < 59.21)
                    {
                        return level4;
                    }
                    else if (setUpTest > 63.83 && setUpTest < middlePercentageMale41_To_45Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageMale41_To_45Level4 && setUpTest < 66.44)
                    {
                        return level5;
                    }
                }
                // 46 到 50 歲
                else if (age >= 46 && age <= 50)
                {
                    if (setUpTest <= 49.18)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.56 && setUpTest <= 53.57)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.84 && setUpTest <= 58.64)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 59.60 && setUpTest <= 63.83)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 66.67)
                    {
                        return level5;
                    }


                    else if (setUpTest > 49.18 && setUpTest < middlePercentageMale46_To_50Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageMale46_To_50Level1 && setUpTest < 50.56)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.57 && setUpTest < middlePercentageMale46_To_50Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageMale46_To_50Level2 && setUpTest < 54.84)
                    {
                        return level3;
                    }
                    else if (setUpTest > 58.64 && setUpTest < middlePercentageMale46_To_50Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageMale46_To_50Level3 && setUpTest < 59.60)
                    {
                        return level4;
                    }
                    else if (setUpTest > 63.83 && setUpTest < middlePercentageMale46_To_50Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageMale46_To_50Level4 && setUpTest < 66.67)
                    {
                        return level5;
                    }
                }
                // 51 到 55 歲
                else if (age >= 51 && age <= 55)
                {
                    if (setUpTest <= 49.18)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.56 && setUpTest <= 53.89)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.88 && setUpTest <= 58.82)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 60.40 && setUpTest <= 63.83)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 67.16)
                    {
                        return level5;
                    }


                    else if (setUpTest > 49.18 && setUpTest < middlePercentageMale51_To_55Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageMale51_To_55Level1 && setUpTest < 50.56)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.89 && setUpTest < middlePercentageMale51_To_55Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageMale51_To_55Level2 && setUpTest < 54.88)
                    {
                        return level3;
                    }
                    else if (setUpTest > 58.82 && setUpTest < middlePercentageMale51_To_55Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageMale51_To_55Level3 && setUpTest < 60.40)
                    {
                        return level4;
                    }
                    else if (setUpTest > 63.83 && setUpTest < middlePercentageMale51_To_55Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageMale51_To_55Level4 && setUpTest < 67.16)
                    {
                        return level5;
                    }
                }
                // 56 到 60 歲
                else if (age >= 56 && age <= 60)
                {
                    if (setUpTest <= 48.13)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.35 && setUpTest <= 54.39)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 55.56 && setUpTest <= 59.60)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 60.81 && setUpTest <= 65.22)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 68.31)
                    {
                        return level5;
                    }


                    else if (setUpTest > 48.13 && setUpTest < middlePercentageMale55_To_60Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageMale55_To_60Level1 && setUpTest < 50.35)
                    {
                        return level2;
                    }
                    else if (setUpTest > 54.39 && setUpTest < middlePercentageMale55_To_60Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageMale55_To_60Level2 && setUpTest < 55.56)
                    {
                        return level3;
                    }
                    else if (setUpTest > 59.60 && setUpTest < middlePercentageMale55_To_60Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageMale55_To_60Level3 && setUpTest < 60.81)
                    {
                        return level4;
                    }
                    else if (setUpTest > 65.22 && setUpTest < middlePercentageMale55_To_60Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageMale55_To_60Level4 && setUpTest < 68.31)
                    {
                        return level5;
                    }
                }
                // 61 到 65 歲
                else if (age >= 61 && age <= 65)
                {
                    if (setUpTest <= 45.51)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 49.86 && setUpTest <= 54.02)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 55.69 && setUpTest <= 60.00)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 61.64 && setUpTest <= 67.47)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 69.28)
                    {
                        return level5;
                    }


                    else if (setUpTest > 45.51 && setUpTest < middlePercentageMale61_To_65Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageMale61_To_65Level1 && setUpTest < 49.86)
                    {
                        return level2;
                    }
                    else if (setUpTest > 54.02 && setUpTest < middlePercentageMale61_To_65Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageMale61_To_65Level2 && setUpTest < 55.69)
                    {
                        return level3;
                    }
                    else if (setUpTest > 60.00 && setUpTest < middlePercentageMale61_To_65Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageMale61_To_65Level3 && setUpTest < 61.64)
                    {
                        return level4;
                    }
                    else if (setUpTest > 67.47 && setUpTest < middlePercentageMale61_To_65Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageMale61_To_65Level4 && setUpTest < 69.28)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 21 到 25 歲
                if (age >= 21 && age <= 25)
                {
                    if (setUpTest <= 47.87)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 49.18 && setUpTest <= 51.72)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 52.94 && setUpTest <= 56.25)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 57.32 && setUpTest <= 61.22)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 63.38)
                    {
                        return level5;
                    }


                    else if (setUpTest > 47.87 && setUpTest < middlePercentageFemale_21_To_25Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageFemale_21_To_25Level1 && setUpTest < 49.18)
                    {
                        return level2;
                    }
                    else if (setUpTest > 51.72 && setUpTest < middlePercentageFemale_21_To_25Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageFemale_21_To_25Level2 && setUpTest < 52.94)
                    {
                        return level3;
                    }
                    else if (setUpTest > 56.25 && setUpTest < middlePercentageFemale_21_To_25Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageFemale_21_To_25Level3 && setUpTest < 57.32)
                    {
                        return level4;
                    }
                    else if (setUpTest > 61.22 && setUpTest < middlePercentageFemale_21_To_25Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageFemale_21_To_25Level4 && setUpTest < 63.38)
                    {
                        return level5;
                    }
                }
                // 26 到 30 歲
                else if (age > 26 && age < 30)
                {
                    if (setUpTest <= 49.72)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.78 && setUpTest <= 53.57)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.55 && setUpTest <= 57.32)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 58.44 && setUpTest <= 62.50)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 64.75)
                    {
                        return level5;
                    }


                    else if (setUpTest > 49.72 && setUpTest < middlePercentageFemale_26_To_30Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageFemale_26_To_30Level1 && setUpTest < 50.78)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.57 && setUpTest < middlePercentageFemale_26_To_30Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageFemale_26_To_30Level2 && setUpTest < 54.55)
                    {
                        return level3;
                    }
                    else if (setUpTest > 57.32 && setUpTest < middlePercentageFemale_26_To_30Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageFemale_26_To_30Level3 && setUpTest < 58.44)
                    {
                        return level4;
                    }
                    else if (setUpTest > 62.50 && setUpTest < middlePercentageFemale_26_To_30Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageFemale_26_To_30Level4 && setUpTest < 64.75)
                    {
                        return level5;
                    }
                }
                // 31 到 35 歲
                else if (age >= 31 && age <= 35)
                {
                    if (setUpTest <= 49.72)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.64 && setUpTest <= 53.57)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.55 && setUpTest <= 57.32)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 58.82 && setUpTest <= 62.50)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 64.29)
                    {
                        return level5;
                    }


                    else if (setUpTest > 49.72 && setUpTest < middlePercentageFemale_31_To_35Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageFemale_31_To_35Level1 && setUpTest < 50.64)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.57 && setUpTest < middlePercentageFemale_31_To_35Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageFemale_31_To_35Level2 && setUpTest < 54.55)
                    {
                        return level3;
                    }
                    else if (setUpTest > 57.32 && setUpTest < middlePercentageFemale_31_To_35Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageFemale_31_To_35Level3 && setUpTest < 58.82)
                    {
                        return level4;
                    }
                    else if (setUpTest > 62.50 && setUpTest < middlePercentageFemale_31_To_35Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageFemale_31_To_35Level4 && setUpTest < 64.29)
                    {
                        return level5;
                    }
                }
                // 36 到 40 歲
                else if (age >= 36 && age <= 40)
                {
                    if (setUpTest <= 49.43)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.56 && setUpTest <= 53.25)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.55 && setUpTest <= 57.69)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 58.82 && setUpTest <= 62.94)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 64.29)
                    {
                        return level5;
                    }


                    else if (setUpTest > 49.43 && setUpTest < middlePercentageFemale_36_To_40Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageFemale_36_To_40Level1 && setUpTest < 50.56)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.25 && setUpTest < middlePercentageFemale_36_To_40Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageFemale_36_To_40Level2 && setUpTest < 54.55)
                    {
                        return level3;
                    }
                    else if (setUpTest > 57.69 && setUpTest < middlePercentageFemale_36_To_40Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageFemale_36_To_40Level3 && setUpTest < 58.82)
                    {
                        return level4;
                    }
                    else if (setUpTest > 62.94 && setUpTest < middlePercentageFemale_36_To_40Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageFemale_36_To_40Level4 && setUpTest < 64.29)
                    {
                        return level5;
                    }
                }
                // 41 到 45 歲
                else if (age >= 41 && age <= 45)
                {
                    if (setUpTest <= 49.34)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.28 && setUpTest <= 53.19)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.88 && setUpTest <= 58.06)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 59.21 && setUpTest <= 63.38)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 65.65)
                    {
                        return level5;
                    }


                    else if (setUpTest > 49.34 && setUpTest < middlePercentageFemale_41_To_45Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageFemale_41_To_45Level1 && setUpTest < 50.28)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.19 && setUpTest < middlePercentageFemale_41_To_45Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageFemale_41_To_45Level2 && setUpTest < 54.88)
                    {
                        return level3;
                    }
                    else if (setUpTest > 58.06 && setUpTest < middlePercentageFemale_41_To_45Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageFemale_41_To_45Level3 && setUpTest < 59.21)
                    {
                        return level4;
                    }
                    else if (setUpTest > 63.38 && setUpTest < middlePercentageFemale_41_To_45Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageFemale_41_To_45Level4 && setUpTest < 65.65)
                    {
                        return level5;
                    }
                }
                // 46 到 50 歲
                else if (age >= 46 && age <= 50)
                {
                    if (setUpTest <= 48.91)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 50.28 && setUpTest <= 53.19)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.88 && setUpTest <= 58.82)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 60.00 && setUpTest <= 64.29)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 66.67)
                    {
                        return level5;
                    }


                    else if (setUpTest > 48.91 && setUpTest < middlePercentageFemale_46_To_50Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageFemale_46_To_50Level1 && setUpTest < 50.28)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.19 && setUpTest < middlePercentageFemale_46_To_50Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageFemale_46_To_50Level2 && setUpTest < 54.88)
                    {
                        return level3;
                    }
                    else if (setUpTest > 58.82 && setUpTest < middlePercentageFemale_46_To_50Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageFemale_46_To_50Level3 && setUpTest < 60.00)
                    {
                        return level4;
                    }
                    else if (setUpTest > 64.29 && setUpTest < middlePercentageFemale_46_To_50Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageFemale_46_To_50Level4 && setUpTest < 66.67)
                    {
                        return level5;
                    }
                }
                // 51 到 55 歲
                else if (age >= 51 && age <= 55)
                {
                    if (setUpTest <= 46.18)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 49.05 && setUpTest <= 53.08)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 54.91 && setUpTest <= 59.60)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 60.40 && setUpTest <= 65.69)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 68.34)
                    {
                        return level5;
                    }


                    else if (setUpTest > 46.18 && setUpTest < middlePercentageFemale_51_To_55Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageFemale_51_To_55Level1 && setUpTest < 49.05)
                    {
                        return level2;
                    }
                    else if (setUpTest > 53.08 && setUpTest < middlePercentageFemale_51_To_55Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageFemale_51_To_55Level2 && setUpTest < 54.91)
                    {
                        return level3;
                    }
                    else if (setUpTest > 59.60 && setUpTest < middlePercentageFemale_51_To_55Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageFemale_51_To_55Level3 && setUpTest < 60.40)
                    {
                        return level4;
                    }
                    else if (setUpTest > 65.69 && setUpTest < middlePercentageFemale_51_To_55Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageFemale_51_To_55Level4 && setUpTest < 68.34)
                    {
                        return level5;
                    }
                }
                // 56 到 60 歲
                else if (age >= 56 && age <= 60)
                {
                    if (setUpTest <= 45.99)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 48.91 && setUpTest <= 52.78)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 55.48 && setUpTest <= 60.00)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 61.71 && setUpTest <= 66.67)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 69.23)
                    {
                        return level5;
                    }


                    else if (setUpTest > 45.99 && setUpTest < middlePercentageFemale_56_To_60Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageFemale_56_To_60Level1 && setUpTest < 48.91)
                    {
                        return level2;
                    }
                    else if (setUpTest > 52.78 && setUpTest < middlePercentageFemale_56_To_60Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageFemale_56_To_60Level2 && setUpTest < 55.48)
                    {
                        return level3;
                    }
                    else if (setUpTest > 60.00 && setUpTest < middlePercentageFemale_56_To_60Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageFemale_56_To_60Level3 && setUpTest < 61.71)
                    {
                        return level4;
                    }
                    else if (setUpTest > 66.67 && setUpTest < middlePercentageFemale_56_To_60Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageFemale_56_To_60Level4 && setUpTest < 69.23)
                    {
                        return level5;
                    }
                }
                // 61 到 65 歲
                else if (age >= 61 && age <= 65)
                {
                    if (setUpTest <= 39.12)
                    {
                        return level1;
                    }
                    else if (setUpTest >= 44.45 && setUpTest <= 52.45)
                    {
                        return level2;
                    }
                    else if (setUpTest >= 56.96 && setUpTest <= 60.81)
                    {
                        return level3;
                    }
                    else if (setUpTest >= 62.07 && setUpTest <= 68.60)
                    {
                        return level4;
                    }
                    else if (setUpTest >= 70.51)
                    {
                        return level5;
                    }


                    else if (setUpTest > 39.12 && setUpTest < middlePercentageFemale_61_To_65Level1)
                    {
                        return level1;
                    }
                    else if (setUpTest > middlePercentageFemale_61_To_65Level1 && setUpTest < 44.45)
                    {
                        return level2;
                    }
                    else if (setUpTest > 52.45 && setUpTest < middlePercentageFemale_61_To_65Level2)
                    {
                        return level2;
                    }
                    else if (setUpTest > middlePercentageFemale_61_To_65Level2 && setUpTest < 56.96)
                    {
                        return level3;
                    }
                    else if (setUpTest > 60.81 && setUpTest < middlePercentageFemale_61_To_65Level3)
                    {
                        return level3;
                    }
                    else if (setUpTest > middlePercentageFemale_61_To_65Level3 && setUpTest < 62.07)
                    {
                        return level4;
                    }
                    else if (setUpTest > 68.60 && setUpTest < middlePercentageFemale_61_To_65Level4)
                    {
                        return level4;
                    }
                    else if (setUpTest > middlePercentageFemale_61_To_65Level4 && setUpTest < 70.51)
                    {
                        return level5;
                    }
                }
            }

            return 100;
        }

        /// <summary>
        /// 體適能-心肺適能(原地站立抬膝)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="standingKneeRaise">原地站立抬膝次數</param>
        /// <returns></returns>
        public int PhysicalFitnessStandingKneeRaiseLevel(string gender, int age, int standingKneeRaise)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;

            // 男性中間百分比
            double middlePercentageMale65_To_69Level1 = (78 + 82) / 2;
            double middlePercentageMale65_To_69Level2 = (90 + 93) / 2;
            double middlePercentageMale65_To_69Level3 = (101 + 103) / 2;
            double middlePercentageMale65_To_69Level4 = (111 + 115) / 2;
            double middlePercentageMale70_To_74Level1 = (71 + 76) / 2;
            double middlePercentageMale70_To_74Level2 = (85 + 89) / 2;
            double middlePercentageMale70_To_74Level3 = (96 + 99) / 2;
            double middlePercentageMale70_To_74Level4 = (107 + 112) / 2;
            double middlePercentageMale75_To_79Level1 = (61 + 67) / 2;
            double middlePercentageMale75_To_79Level2 = (78 + 82) / 2;
            double middlePercentageMale75_To_79Level3 = (91 + 94) / 2;
            double middlePercentageMale75_To_79Level4 = (103 + 107) / 2;
            double middlePercentageMale80_To_84Level1 = (52 + 59) / 2;
            double middlePercentageMale80_To_84Level2 = (72 + 76) / 2;
            double middlePercentageMale80_To_84Level3 = (86 + 89) / 2;
            double middlePercentageMale80_To_84Level4 = (100 + 104) / 2;
            double middlePercentageMale85_To_89Level1 = (48 + 54) / 2;
            double middlePercentageMale85_To_89Level2 = (69 + 74) / 2;
            double middlePercentageMale85_To_89Level3 = (82 + 87) / 2;
            double middlePercentageMale85_To_89Level4 = (97 + 101) / 2;
            double middlePercentageMale90OverLevel1 = (42 + 47) / 2;
            double middlePercentageMale90OverLevel2 = (59 + 66) / 2;
            double middlePercentageMale90OverLevel3 = (78 + 86) / 2;
            double middlePercentageMale90OverLevel4 = (96 + 99) / 2;

            // 女性中間百分比
            double middlePercentageFemale_65_To_69Level1 = (72 + 76) / 2;
            double middlePercentageFemale_65_To_69Level2 = (87 + 90) / 2;
            double middlePercentageFemale_65_To_69Level3 = (97 + 100) / 2;
            double middlePercentageFemale_65_To_69Level4 = (108 + 112) / 2;
            double middlePercentageFemale_70_To_74Level1 = (64 + 69) / 2;
            double middlePercentageFemale_70_To_74Level2 = (80 + 83) / 2;
            double middlePercentageFemale_70_To_74Level3 = (93 + 96) / 2;
            double middlePercentageFemale_70_To_74Level4 = (104 + 108) / 2;
            double middlePercentageFemale_75_To_79Level1 = (56 + 62) / 2;
            double middlePercentageFemale_75_To_79Level2 = (74 + 78) / 2;
            double middlePercentageFemale_75_To_79Level3 = (87 + 91) / 2;
            double middlePercentageFemale_75_To_79Level4 = (100 + 104) / 2;
            double middlePercentageFemale_80_To_84Level1 = (46 + 51) / 2;
            double middlePercentageFemale_80_To_84Level2 = (66 + 70) / 2;
            double middlePercentageFemale_80_To_84Level3 = (82 + 86) / 2;
            double middlePercentageFemale_80_To_84Level4 = (96 + 100) / 2;
            double middlePercentageFemale_85_To_89Level1 = (34 + 42) / 2;
            double middlePercentageFemale_85_To_89Level2 = (58 + 61) / 2;
            double middlePercentageFemale_85_To_89Level3 = (76 + 80) / 2;
            double middlePercentageFemale_85_To_89Level4 = (95 + 99) / 2;
            double middlePercentageFemale90OverLevel1 = (29 + 31) / 2;
            double middlePercentageFemale90OverLevel2 = (45 + 50) / 2;
            double middlePercentageFemale90OverLevel3 = (66 + 71) / 2;
            double middlePercentageFemale90OverLevel4 = (84 + 90) / 2;

            if (gender == "男")
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (standingKneeRaise <= 78)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 82 && standingKneeRaise <= 90)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 93 && standingKneeRaise <= 101)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 103 && standingKneeRaise <= 111)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 115)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 78 && standingKneeRaise < middlePercentageMale65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageMale65_To_69Level1 && standingKneeRaise < 82)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 90 && standingKneeRaise < middlePercentageMale65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageMale65_To_69Level2 && standingKneeRaise < 93)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 101 && standingKneeRaise < middlePercentageMale65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageMale65_To_69Level3 && standingKneeRaise < 103)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 111 && standingKneeRaise < middlePercentageMale65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageMale65_To_69Level4 && standingKneeRaise < 115)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (standingKneeRaise <= 71)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 76 && standingKneeRaise <= 85)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 89 && standingKneeRaise <= 96)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 99 && standingKneeRaise <= 107)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 112)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 71 && standingKneeRaise < middlePercentageMale70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageMale70_To_74Level1 && standingKneeRaise < 76)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 85 && standingKneeRaise < middlePercentageMale70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageMale70_To_74Level2 && standingKneeRaise < 89)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 96 && standingKneeRaise < middlePercentageMale70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageMale70_To_74Level3 && standingKneeRaise < 99)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 107 && standingKneeRaise < middlePercentageMale70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageMale70_To_74Level4 && standingKneeRaise < 112)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (standingKneeRaise <= 61)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 67 && standingKneeRaise <= 78)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 82 && standingKneeRaise <= 91)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 94 && standingKneeRaise <= 103)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 107)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 61 && standingKneeRaise < middlePercentageMale75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageMale75_To_79Level1 && standingKneeRaise < 67)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 78 && standingKneeRaise < middlePercentageMale75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageMale75_To_79Level2 && standingKneeRaise < 82)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 91 && standingKneeRaise < middlePercentageMale75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageMale75_To_79Level3 && standingKneeRaise < 94)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 103 && standingKneeRaise < middlePercentageMale75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageMale75_To_79Level4 && standingKneeRaise < 107)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (standingKneeRaise <= 52)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 59 && standingKneeRaise <= 72)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 76 && standingKneeRaise <= 86)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 89 && standingKneeRaise <= 100)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 104)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 52 && standingKneeRaise < middlePercentageMale80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageMale80_To_84Level1 && standingKneeRaise < 59)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 72 && standingKneeRaise < middlePercentageMale80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageMale80_To_84Level2 && standingKneeRaise < 76)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 86 && standingKneeRaise < middlePercentageMale80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageMale80_To_84Level3 && standingKneeRaise < 89)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 100 && standingKneeRaise < middlePercentageMale80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageMale80_To_84Level4 && standingKneeRaise < 104)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 84)
                {
                    if (standingKneeRaise <= 48)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 54 && standingKneeRaise <= 69)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 74 && standingKneeRaise <= 82)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 87 && standingKneeRaise <= 97)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 101)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 48 && standingKneeRaise < middlePercentageMale85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageMale85_To_89Level1 && standingKneeRaise < 54)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 69 && standingKneeRaise < middlePercentageMale85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageMale85_To_89Level2 && standingKneeRaise < 74)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 82 && standingKneeRaise < middlePercentageMale85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageMale85_To_89Level3 && standingKneeRaise < 87)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 97 && standingKneeRaise < middlePercentageMale85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageMale85_To_89Level4 && standingKneeRaise < 101)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else
                {
                    if (standingKneeRaise <= 42)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 47 && standingKneeRaise <= 59)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 66 && standingKneeRaise <= 78)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 86 && standingKneeRaise <= 96)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 99)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 42 && standingKneeRaise < middlePercentageMale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageMale90OverLevel1 && standingKneeRaise < 47)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 59 && standingKneeRaise < middlePercentageMale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageMale90OverLevel2 && standingKneeRaise < 66)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 78 && standingKneeRaise < middlePercentageMale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageMale90OverLevel3 && standingKneeRaise < 86)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 96 && standingKneeRaise < middlePercentageMale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageMale90OverLevel4 && standingKneeRaise < 99)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (standingKneeRaise <= 72)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 76 && standingKneeRaise <= 87)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 90 && standingKneeRaise <= 97)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 100 && standingKneeRaise <= 108)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 112)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 72 && standingKneeRaise < middlePercentageFemale_65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_65_To_69Level1 && standingKneeRaise < 76)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 87 && standingKneeRaise < middlePercentageFemale_65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_65_To_69Level2 && standingKneeRaise < 90)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 97 && standingKneeRaise < middlePercentageFemale_65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_65_To_69Level3 && standingKneeRaise < 100)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 108 && standingKneeRaise < middlePercentageFemale_65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_65_To_69Level4 && standingKneeRaise < 112)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (standingKneeRaise <= 64)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 69 && standingKneeRaise <= 80)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 83 && standingKneeRaise <= 93)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 96 && standingKneeRaise <= 104)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 108)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 64 && standingKneeRaise < middlePercentageFemale_70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_70_To_74Level1 && standingKneeRaise < 69)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 80 && standingKneeRaise < middlePercentageFemale_70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_70_To_74Level2 && standingKneeRaise < 83)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 93 && standingKneeRaise < middlePercentageFemale_70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_70_To_74Level3 && standingKneeRaise < 96)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 104 && standingKneeRaise < middlePercentageFemale_70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_70_To_74Level4 && standingKneeRaise < 108)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (standingKneeRaise <= 56)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 62 && standingKneeRaise <= 74)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 78 && standingKneeRaise <= 87)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 91 && standingKneeRaise <= 100)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 104)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 56 && standingKneeRaise < middlePercentageFemale_75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_75_To_79Level1 && standingKneeRaise < 62)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 74 && standingKneeRaise < middlePercentageFemale_75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_75_To_79Level2 && standingKneeRaise < 78)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 87 && standingKneeRaise < middlePercentageFemale_75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_75_To_79Level3 && standingKneeRaise < 91)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 100 && standingKneeRaise < middlePercentageFemale_75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_75_To_79Level4 && standingKneeRaise < 104)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (standingKneeRaise <= 56)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 62 && standingKneeRaise <= 74)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 78 && standingKneeRaise <= 87)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 91 && standingKneeRaise <= 100)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 104)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 56 && standingKneeRaise < middlePercentageFemale_80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_80_To_84Level1 && standingKneeRaise < 62)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 74 && standingKneeRaise < middlePercentageFemale_80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_80_To_84Level2 && standingKneeRaise < 78)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 87 && standingKneeRaise < middlePercentageFemale_80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_80_To_84Level3 && standingKneeRaise < 91)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 100 && standingKneeRaise < middlePercentageFemale_80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_80_To_84Level4 && standingKneeRaise < 104)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (standingKneeRaise <= 34)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 42 && standingKneeRaise <= 58)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 61 && standingKneeRaise <= 76)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 80 && standingKneeRaise <= 95)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 99)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 34 && standingKneeRaise < middlePercentageFemale_85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_85_To_89Level1 && standingKneeRaise < 42)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 58 && standingKneeRaise < middlePercentageFemale_85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_85_To_89Level2 && standingKneeRaise < 61)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 76 && standingKneeRaise < middlePercentageFemale_85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_85_To_89Level3 && standingKneeRaise < 80)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 95 && standingKneeRaise < middlePercentageFemale_85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageFemale_85_To_89Level4 && standingKneeRaise < 99)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else
                {
                    if (standingKneeRaise <= 29)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise >= 31 && standingKneeRaise <= 45)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise >= 50 && standingKneeRaise <= 66)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise >= 71 && standingKneeRaise <= 84)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise >= 90)
                    {
                        return level5;
                    }


                    else if (standingKneeRaise > 29 && standingKneeRaise < middlePercentageFemale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (standingKneeRaise > middlePercentageFemale90OverLevel1 && standingKneeRaise < 31)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > 45 && standingKneeRaise < middlePercentageFemale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (standingKneeRaise > middlePercentageFemale90OverLevel2 && standingKneeRaise < 50)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > 66 && standingKneeRaise < middlePercentageFemale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (standingKneeRaise > middlePercentageFemale90OverLevel3 && standingKneeRaise < 71)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > 84 && standingKneeRaise < middlePercentageFemale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (standingKneeRaise > middlePercentageFemale90OverLevel4 && standingKneeRaise < 90)
                    {
                        return level5;
                    }
                }
            }

            return 100;
        }

        /// <summary>
        /// 體適能-肌力與肌耐力(一分鐘屈膝仰臥起坐)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="kneeCrunchesFrequency">一分鐘屈膝仰臥起坐次數</param>
        /// <returns></returns>
        public int PhysicalFitnessKneeCrunchesFrequencyLevel(string gender, int age, int kneeCrunchesFrequency)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;

            // 男性中間百分比
            double middlePercentageMale21_To_25Level1 = (30 + 31) / 2;
            double middlePercentageMale21_To_25Level2 = (34 + 35) / 2;
            double middlePercentageMale21_To_25Level3 = (38 + 39) / 2;
            double middlePercentageMale21_To_25Level4 = (43 + 45) / 2;
            double middlePercentageMale26_To_30Level1 = (26 + 27) / 2;
            double middlePercentageMale26_To_30Level2 = (31 + 32) / 2;
            double middlePercentageMale26_To_30Level3 = (35 + 36) / 2;
            double middlePercentageMale26_To_30Level4 = (40 + 41) / 2;
            double middlePercentageMale31_To_35Level1 = (22 + 23) / 2;
            double middlePercentageMale31_To_35Level2 = (26 + 27) / 2;
            double middlePercentageMale31_To_35Level3 = (30 + 31) / 2;
            double middlePercentageMale31_To_35Level4 = (35 + 36) / 2;
            double middlePercentageMale36_To_40Level1 = (19 + 20) / 2;
            double middlePercentageMale36_To_40Level2 = (23 + 24) / 2;
            double middlePercentageMale36_To_40Level3 = (27 + 28) / 2;
            double middlePercentageMale36_To_40Level4 = (31 + 33) / 2;
            double middlePercentageMale41_To_45Level1 = (19 + 20) / 2;
            double middlePercentageMale41_To_45Level2 = (23 + 24) / 2;
            double middlePercentageMale41_To_45Level3 = (27 + 28) / 2;
            double middlePercentageMale41_To_45Level4 = (31 + 32) / 2;
            double middlePercentageMale46_To_50Level1 = (16 + 17) / 2;
            double middlePercentageMale46_To_50Level2 = (20 + 21) / 2;
            double middlePercentageMale46_To_50Level3 = (24 + 25) / 2;
            double middlePercentageMale46_To_50Level4 = (28 + 30) / 2;
            double middlePercentageMale51_To_55Level1 = (14 + 15) / 2;
            double middlePercentageMale51_To_55Level2 = (18 + 19) / 2;
            double middlePercentageMale51_To_55Level3 = (22 + 23) / 2;
            double middlePercentageMale51_To_55Level4 = (27 + 29) / 2;
            double middlePercentageMale56_To_60Level1 = (10 + 12) / 2;
            double middlePercentageMale56_To_60Level2 = (15 + 16) / 2;
            double middlePercentageMale56_To_60Level3 = (19 + 20) / 2;
            double middlePercentageMale56_To_60Level4 = (24 + 26) / 2;
            double middlePercentageMale61_To_65Level1 = (0 + 2) / 2;
            double middlePercentageMale61_To_65Level2 = (9 + 11) / 2;
            double middlePercentageMale61_To_65Level3 = (15 + 16) / 2;
            double middlePercentageMale61_To_65Level4 = (20 + 22) / 2;

            // 女性中間百分比
            double middlePercentageFemale_21_To_25Level1 = (18 + 19) / 2;
            double middlePercentageFemale_21_To_25Level2 = (23 + 24) / 2;
            double middlePercentageFemale_21_To_25Level3 = (27 + 28) / 2;
            double middlePercentageFemale_21_To_25Level4 = (32 + 34) / 2;
            double middlePercentageFemale_26_To_30Level1 = (15 + 16) / 2;
            double middlePercentageFemale_26_To_30Level2 = (19 + 20) / 2;
            double middlePercentageFemale_26_To_30Level3 = (23 + 24) / 2;
            double middlePercentageFemale_26_To_30Level4 = (28 + 30) / 2;
            double middlePercentageFemale_31_To_35Level1 = (12 + 14) / 2;
            double middlePercentageFemale_31_To_35Level2 = (17 + 18) / 2;
            double middlePercentageFemale_31_To_35Level3 = (21 + 22) / 2;
            double middlePercentageFemale_31_To_35Level4 = (25 + 27) / 2;
            double middlePercentageFemale_36_To_40Level1 = (10 + 12) / 2;
            double middlePercentageFemale_36_To_40Level2 = (16 + 17) / 2;
            double middlePercentageFemale_36_To_40Level3 = (20 + 21) / 2;
            double middlePercentageFemale_36_To_40Level4 = (24 + 25) / 2;
            double middlePercentageFemale_41_To_45Level1 = (6 + 9) / 2;
            double middlePercentageFemale_41_To_45Level2 = (13 + 15) / 2;
            double middlePercentageFemale_41_To_45Level3 = (18 + 19) / 2;
            double middlePercentageFemale_41_To_45Level4 = (22 + 24) / 2;
            double middlePercentageFemale_46_To_50Level1 = (3 + 6) / 2;
            double middlePercentageFemale_46_To_50Level2 = (11 + 12) / 2;
            double middlePercentageFemale_46_To_50Level3 = (16 + 17) / 2;
            double middlePercentageFemale_46_To_50Level4 = (20 + 22) / 2;
            double middlePercentageFemale_51_To_55Level1 = (0 + 1) / 2;
            double middlePercentageFemale_51_To_55Level2 = (6 + 8) / 2;
            double middlePercentageFemale_51_To_55Level3 = (12 + 13) / 2;
            double middlePercentageFemale_51_To_55Level4 = (17 + 19) / 2;
            //double middlePercentageFemale_56_To_60Level1 = (0 + 0) / 2;
            double middlePercentageFemale_56_To_60Level2 = (2 + 3) / 2;
            double middlePercentageFemale_56_To_60Level3 = (9 + 11) / 2;
            double middlePercentageFemale_56_To_60Level4 = (15 + 16) / 2;
            //double middlePercentageFemale_61_To_65Level1 = (0 + 0) / 2;
            //double middlePercentageFemale_61_To_65Level2 = (0 + 0) / 2;
            double middlePercentageFemale_61_To_65Level3 = (4 + 6) / 2;
            double middlePercentageFemale_61_To_65Level4 = (11 + 13) / 2;

            if (gender == "男")
            {
                // 21 到 25 歲
                if (age >= 21 && age <= 25)
                {
                    if (kneeCrunchesFrequency <= 30)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 31 && kneeCrunchesFrequency <= 34)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 35 && kneeCrunchesFrequency <= 38)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 39 && kneeCrunchesFrequency <= 43)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 45)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 30 && kneeCrunchesFrequency < middlePercentageMale21_To_25Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale21_To_25Level1 && kneeCrunchesFrequency < 31)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 34 && kneeCrunchesFrequency < middlePercentageMale21_To_25Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale21_To_25Level2 && kneeCrunchesFrequency < 35)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 38 && kneeCrunchesFrequency < middlePercentageMale21_To_25Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale21_To_25Level3 && kneeCrunchesFrequency < 39)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 43 && kneeCrunchesFrequency < middlePercentageMale21_To_25Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale21_To_25Level4 && kneeCrunchesFrequency < 45)
                    {
                        return level5;
                    }
                }
                // 26 到 30 歲
                else if (age >= 26 && age <= 30)
                {
                    if (kneeCrunchesFrequency <= 26)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 27 && kneeCrunchesFrequency <= 31)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 32 && kneeCrunchesFrequency <= 35)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 36 && kneeCrunchesFrequency <= 40)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 41)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 26 && kneeCrunchesFrequency < middlePercentageMale26_To_30Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale26_To_30Level1 && kneeCrunchesFrequency < 27)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 31 && kneeCrunchesFrequency < middlePercentageMale26_To_30Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale26_To_30Level2 && kneeCrunchesFrequency < 32)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 35 && kneeCrunchesFrequency < middlePercentageMale26_To_30Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale26_To_30Level3 && kneeCrunchesFrequency < 36)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 40 && kneeCrunchesFrequency < middlePercentageMale26_To_30Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale26_To_30Level4 && kneeCrunchesFrequency < 41)
                    {
                        return level5;
                    }
                }
                // 31 到 35 歲
                else if (age >= 31 && age <= 35)
                {
                    if (kneeCrunchesFrequency <= 22)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 23 && kneeCrunchesFrequency <= 26)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 27 && kneeCrunchesFrequency <= 30)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 31 && kneeCrunchesFrequency <= 35)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 36)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 22 && kneeCrunchesFrequency < middlePercentageMale31_To_35Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale31_To_35Level1 && kneeCrunchesFrequency < 23)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 26 && kneeCrunchesFrequency < middlePercentageMale31_To_35Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale31_To_35Level2 && kneeCrunchesFrequency < 27)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 30 && kneeCrunchesFrequency < middlePercentageMale31_To_35Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale31_To_35Level3 && kneeCrunchesFrequency < 31)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 35 && kneeCrunchesFrequency < middlePercentageMale31_To_35Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale31_To_35Level4 && kneeCrunchesFrequency < 36)
                    {
                        return level5;
                    }
                }
                // 36 到 40 歲
                else if (age >= 36 && age <= 40)
                {
                    if (kneeCrunchesFrequency <= 19)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 20 && kneeCrunchesFrequency <= 23)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 24 && kneeCrunchesFrequency <= 27)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 28 && kneeCrunchesFrequency <= 31)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 33)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 19 && kneeCrunchesFrequency < middlePercentageMale36_To_40Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale36_To_40Level1 && kneeCrunchesFrequency < 20)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 23 && kneeCrunchesFrequency < middlePercentageMale36_To_40Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale36_To_40Level2 && kneeCrunchesFrequency < 24)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 27 && kneeCrunchesFrequency < middlePercentageMale36_To_40Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale36_To_40Level3 && kneeCrunchesFrequency < 28)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 31 && kneeCrunchesFrequency < middlePercentageMale36_To_40Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale36_To_40Level4 && kneeCrunchesFrequency < 33)
                    {
                        return level5;
                    }
                }
                // 41 到 45 歲
                else if (age >= 41 && age <= 45)
                {
                    if (kneeCrunchesFrequency <= 19)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 20 && kneeCrunchesFrequency <= 23)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 24 && kneeCrunchesFrequency <= 27)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 28 && kneeCrunchesFrequency <= 31)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 32)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 19 && kneeCrunchesFrequency < middlePercentageMale41_To_45Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale41_To_45Level1 && kneeCrunchesFrequency < 20)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 23 && kneeCrunchesFrequency < middlePercentageMale41_To_45Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale41_To_45Level2 && kneeCrunchesFrequency < 24)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 27 && kneeCrunchesFrequency < middlePercentageMale41_To_45Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale41_To_45Level3 && kneeCrunchesFrequency < 28)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 31 && kneeCrunchesFrequency < middlePercentageMale41_To_45Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale41_To_45Level4 && kneeCrunchesFrequency < 32)
                    {
                        return level5;
                    }
                }
                // 46 到 50 歲
                else if (age >= 46 && age <= 50)
                {
                    if (kneeCrunchesFrequency <= 16)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 17 && kneeCrunchesFrequency <= 20)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 21 && kneeCrunchesFrequency <= 24)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 25 && kneeCrunchesFrequency <= 28)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 30)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 16 && kneeCrunchesFrequency < middlePercentageMale46_To_50Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale46_To_50Level1 && kneeCrunchesFrequency < 17)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 20 && kneeCrunchesFrequency < middlePercentageMale46_To_50Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale46_To_50Level2 && kneeCrunchesFrequency < 21)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 24 && kneeCrunchesFrequency < middlePercentageMale46_To_50Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale46_To_50Level3 && kneeCrunchesFrequency < 25)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 28 && kneeCrunchesFrequency < middlePercentageMale46_To_50Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale46_To_50Level4 && kneeCrunchesFrequency < 30)
                    {
                        return level5;
                    }
                }
                // 51 到 55 歲
                else if (age >= 51 && age <= 55)
                {
                    if (kneeCrunchesFrequency <= 14)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 15 && kneeCrunchesFrequency <= 18)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 19 && kneeCrunchesFrequency <= 22)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 23 && kneeCrunchesFrequency <= 27)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 29)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 14 && kneeCrunchesFrequency < middlePercentageMale51_To_55Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale51_To_55Level1 && kneeCrunchesFrequency < 15)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 18 && kneeCrunchesFrequency < middlePercentageMale51_To_55Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale51_To_55Level2 && kneeCrunchesFrequency < 19)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 22 && kneeCrunchesFrequency < middlePercentageMale51_To_55Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale51_To_55Level3 && kneeCrunchesFrequency < 23)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 27 && kneeCrunchesFrequency < middlePercentageMale51_To_55Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale51_To_55Level4 && kneeCrunchesFrequency < 29)
                    {
                        return level5;
                    }
                }
                // 55 到 60 歲
                else if (age >= 56 && age <= 60)
                {
                    if (kneeCrunchesFrequency <= 10)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 12 && kneeCrunchesFrequency <= 15)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 16 && kneeCrunchesFrequency <= 19)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 20 && kneeCrunchesFrequency <= 24)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 26)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 10 && kneeCrunchesFrequency < middlePercentageMale56_To_60Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale56_To_60Level1 && kneeCrunchesFrequency < 12)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 15 && kneeCrunchesFrequency < middlePercentageMale56_To_60Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale56_To_60Level2 && kneeCrunchesFrequency < 16)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 19 && kneeCrunchesFrequency < middlePercentageMale56_To_60Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale56_To_60Level3 && kneeCrunchesFrequency < 20)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 24 && kneeCrunchesFrequency < middlePercentageMale56_To_60Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale56_To_60Level4 && kneeCrunchesFrequency < 26)
                    {
                        return level5;
                    }
                }
                // 61 到 65 歲
                else if (age >= 61 && age <= 65)
                {
                    if (kneeCrunchesFrequency <= 0)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 2 && kneeCrunchesFrequency <= 9)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 11 && kneeCrunchesFrequency <= 15)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 16 && kneeCrunchesFrequency <= 20)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 22)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 0 && kneeCrunchesFrequency < middlePercentageMale61_To_65Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale61_To_65Level1 && kneeCrunchesFrequency < 2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 9 && kneeCrunchesFrequency < middlePercentageMale61_To_65Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale61_To_65Level2 && kneeCrunchesFrequency < 11)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 15 && kneeCrunchesFrequency < middlePercentageMale61_To_65Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale61_To_65Level3 && kneeCrunchesFrequency < 16)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 20 && kneeCrunchesFrequency < middlePercentageMale61_To_65Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageMale61_To_65Level4 && kneeCrunchesFrequency < 22)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 21 到 25 歲
                if (age >= 21 && age <= 25)
                {
                    if (kneeCrunchesFrequency <= 18)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 19 && kneeCrunchesFrequency <= 23)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 24 && kneeCrunchesFrequency <= 27)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 28 && kneeCrunchesFrequency <= 32)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 34)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 18 && kneeCrunchesFrequency < middlePercentageFemale_21_To_25Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_21_To_25Level1 && kneeCrunchesFrequency < 19)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 23 && kneeCrunchesFrequency < middlePercentageFemale_21_To_25Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_21_To_25Level2 && kneeCrunchesFrequency < 24)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 27 && kneeCrunchesFrequency < middlePercentageFemale_21_To_25Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_21_To_25Level3 && kneeCrunchesFrequency < 28)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 32 && kneeCrunchesFrequency < middlePercentageFemale_21_To_25Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_21_To_25Level4 && kneeCrunchesFrequency < 34)
                    {
                        return level5;
                    }
                }
                // 26 到 30 歲
                else if (age >= 26 && age <= 30)
                {
                    if (kneeCrunchesFrequency <= 15)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 16 && kneeCrunchesFrequency <= 19)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 20 && kneeCrunchesFrequency <= 23)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 24 && kneeCrunchesFrequency <= 28)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 30)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 15 && kneeCrunchesFrequency < middlePercentageFemale_26_To_30Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_26_To_30Level1 && kneeCrunchesFrequency < 16)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 19 && kneeCrunchesFrequency < middlePercentageFemale_26_To_30Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_26_To_30Level2 && kneeCrunchesFrequency < 20)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 23 && kneeCrunchesFrequency < middlePercentageFemale_26_To_30Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_26_To_30Level3 && kneeCrunchesFrequency < 24)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 28 && kneeCrunchesFrequency < middlePercentageFemale_26_To_30Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_26_To_30Level4 && kneeCrunchesFrequency < 30)
                    {
                        return level5;
                    }
                }
                // 31 到 35 歲
                else if (age >= 31 && age <= 35)
                {
                    if (kneeCrunchesFrequency <= 12)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 14 && kneeCrunchesFrequency <= 17)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 18 && kneeCrunchesFrequency <= 21)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 22 && kneeCrunchesFrequency <= 25)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 27)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 12 && kneeCrunchesFrequency < middlePercentageFemale_31_To_35Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_31_To_35Level1 && kneeCrunchesFrequency < 14)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 17 && kneeCrunchesFrequency < middlePercentageFemale_31_To_35Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_31_To_35Level2 && kneeCrunchesFrequency < 18)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 21 && kneeCrunchesFrequency < middlePercentageFemale_31_To_35Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_31_To_35Level3 && kneeCrunchesFrequency < 22)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 25 && kneeCrunchesFrequency < middlePercentageFemale_31_To_35Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_31_To_35Level4 && kneeCrunchesFrequency < 27)
                    {
                        return level5;
                    }
                }
                // 36 到 40 歲
                else if (age >= 36 && age <= 40)
                {
                    if (kneeCrunchesFrequency <= 10)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 12 && kneeCrunchesFrequency <= 16)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 17 && kneeCrunchesFrequency <= 20)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 21 && kneeCrunchesFrequency <= 24)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 25)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 10 && kneeCrunchesFrequency < middlePercentageFemale_36_To_40Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_36_To_40Level1 && kneeCrunchesFrequency < 12)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 16 && kneeCrunchesFrequency < middlePercentageFemale_36_To_40Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_36_To_40Level2 && kneeCrunchesFrequency < 17)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 20 && kneeCrunchesFrequency < middlePercentageFemale_36_To_40Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_36_To_40Level3 && kneeCrunchesFrequency < 21)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 24 && kneeCrunchesFrequency < middlePercentageFemale_36_To_40Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_36_To_40Level4 && kneeCrunchesFrequency < 25)
                    {
                        return level5;
                    }
                }
                // 41 到 45 歲
                else if (age >= 41 && age <= 45)
                {
                    if (kneeCrunchesFrequency <= 3)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 9 && kneeCrunchesFrequency <= 13)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 15 && kneeCrunchesFrequency <= 18)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 19 && kneeCrunchesFrequency <= 22)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 24)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 6 && kneeCrunchesFrequency < middlePercentageFemale_41_To_45Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_41_To_45Level1 && kneeCrunchesFrequency < 9)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 13 && kneeCrunchesFrequency < middlePercentageFemale_41_To_45Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_41_To_45Level2 && kneeCrunchesFrequency < 15)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 18 && kneeCrunchesFrequency < middlePercentageFemale_41_To_45Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_41_To_45Level3 && kneeCrunchesFrequency < 19)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 22 && kneeCrunchesFrequency < middlePercentageFemale_41_To_45Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_41_To_45Level4 && kneeCrunchesFrequency < 24)
                    {
                        return level5;
                    }
                }
                // 46 到 50 歲
                else if (age >= 46 && age <= 50)
                {
                    if (kneeCrunchesFrequency <= 3)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency >= 6 && kneeCrunchesFrequency <= 11)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 12 && kneeCrunchesFrequency <= 16)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 17 && kneeCrunchesFrequency <= 20)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 22)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 3 && kneeCrunchesFrequency < middlePercentageFemale_46_To_50Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_46_To_50Level1 && kneeCrunchesFrequency < 6)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 11 && kneeCrunchesFrequency < middlePercentageFemale_46_To_50Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_46_To_50Level2 && kneeCrunchesFrequency < 12)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 16 && kneeCrunchesFrequency < middlePercentageFemale_46_To_50Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_46_To_50Level3 && kneeCrunchesFrequency < 17)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 20 && kneeCrunchesFrequency < middlePercentageFemale_46_To_50Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_46_To_50Level4 && kneeCrunchesFrequency < 22)
                    {
                        return level5;
                    }
                }
                // 51 到 55 歲
                else if (age >= 51 && age <= 55)
                {
                    if (kneeCrunchesFrequency <= 1)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 2 && kneeCrunchesFrequency <= 6)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 8 && kneeCrunchesFrequency <= 12)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 13 && kneeCrunchesFrequency <= 17)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 19)
                    {
                        return level5;
                    }


                    else if (kneeCrunchesFrequency > 0 && kneeCrunchesFrequency < middlePercentageFemale_51_To_55Level1)
                    {
                        return level1;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_51_To_55Level1 && kneeCrunchesFrequency < 1)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > 6 && kneeCrunchesFrequency < middlePercentageFemale_51_To_55Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_51_To_55Level2 && kneeCrunchesFrequency < 8)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 12 && kneeCrunchesFrequency < middlePercentageFemale_51_To_55Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_51_To_55Level3 && kneeCrunchesFrequency < 13)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 17 && kneeCrunchesFrequency < middlePercentageFemale_51_To_55Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_51_To_55Level4 && kneeCrunchesFrequency < 19)
                    {
                        return level5;
                    }
                }
                // 55 到 60 歲
                else if (age >= 56 && age <= 60)
                {
                    if (kneeCrunchesFrequency <= 2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency >= 3 && kneeCrunchesFrequency <= 9)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 11 && kneeCrunchesFrequency <= 15)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 16)
                    {
                        return level5;
                    }



                    else if (kneeCrunchesFrequency > 2 && kneeCrunchesFrequency < middlePercentageFemale_56_To_60Level2)
                    {
                        return level2;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_56_To_60Level2 && kneeCrunchesFrequency < 3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > 9 && kneeCrunchesFrequency < middlePercentageFemale_56_To_60Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_56_To_60Level3 && kneeCrunchesFrequency < 11)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 15 && kneeCrunchesFrequency < middlePercentageFemale_56_To_60Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_56_To_60Level4 && kneeCrunchesFrequency < 16)
                    {
                        return level5;
                    }
                }
                // 61 到 65 歲
                else if (age >= 61 && age <= 65)
                {
                    if (kneeCrunchesFrequency <= 2)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency >= 6 && kneeCrunchesFrequency <= 11)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency >= 13)
                    {
                        return level5;
                    }



                    else if (kneeCrunchesFrequency > 4 && kneeCrunchesFrequency < middlePercentageFemale_61_To_65Level3)
                    {
                        return level3;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_61_To_65Level3 && kneeCrunchesFrequency < 6)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > 11 && kneeCrunchesFrequency < middlePercentageFemale_61_To_65Level4)
                    {
                        return level4;
                    }
                    else if (kneeCrunchesFrequency > middlePercentageFemale_61_To_65Level4 && kneeCrunchesFrequency < 13)
                    {
                        return level5;
                    }
                }
            }

            return 100;
        }

        /// <summary>
        /// 體適能-肌力與肌耐力(手臂彎舉)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="armCurlUpperBodyFrequency">手臂彎舉次數</param>
        /// <returns></returns>
        public int PhysicalFitnessArmCurlUpperBodyLevel(string gender, int age, int armCurlUpperBodyFrequency)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;

            // 男性中間百分比
            double middlePercentageMale65_To_69Level1 = (16 + 17) / 2.0;
            double middlePercentageMale65_To_69Level2 = (19 + 20) / 2.0;
            double middlePercentageMale65_To_69Level3 = (22 + 23) / 2.0;
            double middlePercentageMale65_To_69Level4 = (26 + 27) / 2.0;
            double middlePercentageMale70_To_74Level1 = (15 + 16) / 2.0;
            double middlePercentageMale70_To_74Level2 = (18 + 19) / 2.0;
            double middlePercentageMale70_To_74Level3 = (21 + 22) / 2.0;
            double middlePercentageMale70_To_74Level4 = (25 + 26) / 2.0;
            double middlePercentageMale75_To_79Level1 = (14 + 15) / 2.0;
            double middlePercentageMale75_To_79Level2 = (16 + 17) / 2.0;
            double middlePercentageMale75_To_79Level3 = (19 + 20) / 2.0;
            double middlePercentageMale75_To_79Level4 = (23 + 25) / 2.0;
            double middlePercentageMale80_To_84Level1 = (13 + 14) / 2.0;
            double middlePercentageMale80_To_84Level2 = (15 + 16) / 2.0;
            double middlePercentageMale80_To_84Level3 = (18 + 19) / 2.0;
            double middlePercentageMale80_To_84Level4 = (21 + 23) / 2.0;
            double middlePercentageMale85_To_89Level1 = (11 + 12) / 2.0;
            double middlePercentageMale85_To_89Level2 = (14 + 15) / 2.0;
            double middlePercentageMale85_To_89Level3 = (17 + 18) / 2.0;
            double middlePercentageMale85_To_89Level4 = (20 + 21) / 2.0;
            double middlePercentageMale90OverLevel1 = (10 + 11) / 2.0;
            double middlePercentageMale90OverLevel2 = (13 + 14) / 2.0;
            double middlePercentageMale90OverLevel3 = (15 + 16) / 2.0;
            double middlePercentageMale90OverLevel4 = (18 + 20) / 2.0;

            // 女性中間百分比
            double middlePercentageFemale_65_To_69Level1 = (15 + 16) / 2.0;
            double middlePercentageFemale_65_To_69Level2 = (18 + 19) / 2.0;
            double middlePercentageFemale_65_To_69Level3 = (20 + 21) / 2.0;
            double middlePercentageFemale_65_To_69Level4 = (24 + 26) / 2.0;
            double middlePercentageFemale_70_To_74Level1 = (14 + 15) / 2.0;
            double middlePercentageFemale_70_To_74Level2 = (17 + 18) / 2.0;
            double middlePercentageFemale_70_To_74Level3 = (20 + 21) / 2.0;
            double middlePercentageFemale_70_To_74Level4 = (24 + 25) / 2.0;
            double middlePercentageFemale_75_To_79Level1 = (14 + 15) / 2.0;
            double middlePercentageFemale_75_To_79Level2 = (16 + 17) / 2.0;
            double middlePercentageFemale_75_To_79Level3 = (19 + 20) / 2.0;
            double middlePercentageFemale_75_To_79Level4 = (22 + 24) / 2.0;
            double middlePercentageFemale_80_To_84Level1 = (12 + 13) / 2.0;
            double middlePercentageFemale_80_To_84Level2 = (15 + 16) / 2.0;
            double middlePercentageFemale_80_To_84Level3 = (18 + 19) / 2.0;
            double middlePercentageFemale_80_To_84Level4 = (21 + 22) / 2.0;
            double middlePercentageFemale90OverLevel1 = (9 + 10) / 2.0;
            double middlePercentageFemale90OverLevel2 = (12 + 13) / 2.0;
            double middlePercentageFemale90OverLevel3 = (14 + 16) / 2.0;
            double middlePercentageFemale90OverLevel4 = (21 + 22) / 2.0;

            if (gender == "男")
            {
                // 65 以上到 69 歲
                if (age >= 65 && age <= 69)
                {
                    if (armCurlUpperBodyFrequency <= 16)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 17 && armCurlUpperBodyFrequency <= 19)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 20 && armCurlUpperBodyFrequency <= 22)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 23 && armCurlUpperBodyFrequency <= 26)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 27)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 16 && armCurlUpperBodyFrequency < middlePercentageMale65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale65_To_69Level1 && armCurlUpperBodyFrequency < 17)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 19 && armCurlUpperBodyFrequency < middlePercentageMale65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale65_To_69Level2 && armCurlUpperBodyFrequency < 20)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 22 && armCurlUpperBodyFrequency < middlePercentageMale65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale65_To_69Level3 && armCurlUpperBodyFrequency < 23)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 26 && armCurlUpperBodyFrequency < middlePercentageMale65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale65_To_69Level4 && armCurlUpperBodyFrequency < 27)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (armCurlUpperBodyFrequency <= 15)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 16 && armCurlUpperBodyFrequency <= 18)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 19 && armCurlUpperBodyFrequency <= 21)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 22 && armCurlUpperBodyFrequency <= 25)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 26)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 15 && armCurlUpperBodyFrequency < middlePercentageMale70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale70_To_74Level1 && armCurlUpperBodyFrequency < 16)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 18 && armCurlUpperBodyFrequency < middlePercentageMale70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale70_To_74Level2 && armCurlUpperBodyFrequency < 19)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 21 && armCurlUpperBodyFrequency < middlePercentageMale70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale70_To_74Level3 && armCurlUpperBodyFrequency < 22)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 25 && armCurlUpperBodyFrequency < middlePercentageMale70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale70_To_74Level4 && armCurlUpperBodyFrequency < 26)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (armCurlUpperBodyFrequency <= 14)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 15 && armCurlUpperBodyFrequency <= 17)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 17 && armCurlUpperBodyFrequency <= 20)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 20 && armCurlUpperBodyFrequency <= 23)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 25)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 14 && armCurlUpperBodyFrequency < middlePercentageMale75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale75_To_79Level1 && armCurlUpperBodyFrequency < 15)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 16 && armCurlUpperBodyFrequency < middlePercentageMale75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale75_To_79Level2 && armCurlUpperBodyFrequency < 17)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 19 && armCurlUpperBodyFrequency < middlePercentageMale75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale75_To_79Level3 && armCurlUpperBodyFrequency < 20)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 23 && armCurlUpperBodyFrequency < middlePercentageMale75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale75_To_79Level4 && armCurlUpperBodyFrequency < 25)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (armCurlUpperBodyFrequency <= 13)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 14 && armCurlUpperBodyFrequency <= 15)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 16 && armCurlUpperBodyFrequency <= 18)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 19 && armCurlUpperBodyFrequency <= 21)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 23)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 13 && armCurlUpperBodyFrequency < middlePercentageMale80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale80_To_84Level1 && armCurlUpperBodyFrequency < 14)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 15 && armCurlUpperBodyFrequency < middlePercentageMale80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale80_To_84Level2 && armCurlUpperBodyFrequency < 16)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 18 && armCurlUpperBodyFrequency < middlePercentageMale80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale80_To_84Level3 && armCurlUpperBodyFrequency < 19)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 21 && armCurlUpperBodyFrequency < middlePercentageMale80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale80_To_84Level4 && armCurlUpperBodyFrequency < 23)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (armCurlUpperBodyFrequency <= 12)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 12 && armCurlUpperBodyFrequency <= 15)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 15 && armCurlUpperBodyFrequency <= 17)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 18 && armCurlUpperBodyFrequency <= 20)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 21)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 11 && armCurlUpperBodyFrequency < middlePercentageMale85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale85_To_89Level1 && armCurlUpperBodyFrequency < 12)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 14 && armCurlUpperBodyFrequency < middlePercentageMale85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale85_To_89Level2 && armCurlUpperBodyFrequency < 15)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 17 && armCurlUpperBodyFrequency < middlePercentageMale85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale85_To_89Level3 && armCurlUpperBodyFrequency < 18)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 20 && armCurlUpperBodyFrequency < middlePercentageMale85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale85_To_89Level4 && armCurlUpperBodyFrequency < 21)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else
                {
                    if (armCurlUpperBodyFrequency <= 12)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 12 && armCurlUpperBodyFrequency <= 15)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 15 && armCurlUpperBodyFrequency <= 17)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 18 && armCurlUpperBodyFrequency <= 20)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 21)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 11 && armCurlUpperBodyFrequency < middlePercentageMale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale90OverLevel1 && armCurlUpperBodyFrequency < 12)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 14 && armCurlUpperBodyFrequency < middlePercentageMale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale90OverLevel2 && armCurlUpperBodyFrequency < 15)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 17 && armCurlUpperBodyFrequency < middlePercentageMale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale90OverLevel3 && armCurlUpperBodyFrequency < 18)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 20 && armCurlUpperBodyFrequency < middlePercentageMale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageMale90OverLevel4 && armCurlUpperBodyFrequency < 21)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (armCurlUpperBodyFrequency <= 15)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 16 && armCurlUpperBodyFrequency <= 18)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 19 && armCurlUpperBodyFrequency <= 21)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 21 && armCurlUpperBodyFrequency <= 24)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 26)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 15 && armCurlUpperBodyFrequency < middlePercentageFemale_65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_65_To_69Level1 && armCurlUpperBodyFrequency < 16)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 18 && armCurlUpperBodyFrequency < middlePercentageFemale_65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_65_To_69Level2 && armCurlUpperBodyFrequency < 19)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 20 && armCurlUpperBodyFrequency < middlePercentageFemale_65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_65_To_69Level3 && armCurlUpperBodyFrequency < 21)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 24 && armCurlUpperBodyFrequency < middlePercentageFemale_65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_65_To_69Level4 && armCurlUpperBodyFrequency < 26)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (armCurlUpperBodyFrequency <= 15)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 15 && armCurlUpperBodyFrequency <= 17)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 18 && armCurlUpperBodyFrequency <= 20)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 21 && armCurlUpperBodyFrequency <= 24)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 25)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 14 && armCurlUpperBodyFrequency < middlePercentageFemale_70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_70_To_74Level1 && armCurlUpperBodyFrequency < 15)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 17 && armCurlUpperBodyFrequency < middlePercentageFemale_70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_70_To_74Level2 && armCurlUpperBodyFrequency < 18)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 20 && armCurlUpperBodyFrequency < middlePercentageFemale_70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_70_To_74Level3 && armCurlUpperBodyFrequency < 21)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 24 && armCurlUpperBodyFrequency < middlePercentageFemale_70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_70_To_74Level4 && armCurlUpperBodyFrequency < 25)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (armCurlUpperBodyFrequency <= 14)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 15 && armCurlUpperBodyFrequency <= 16)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 17 && armCurlUpperBodyFrequency <= 19)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 20 && armCurlUpperBodyFrequency <= 22)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 24)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 14 && armCurlUpperBodyFrequency < middlePercentageFemale_75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_75_To_79Level1 && armCurlUpperBodyFrequency < 15)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 16 && armCurlUpperBodyFrequency < middlePercentageFemale_75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_75_To_79Level2 && armCurlUpperBodyFrequency < 17)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 19 && armCurlUpperBodyFrequency < middlePercentageFemale_75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_75_To_79Level3 && armCurlUpperBodyFrequency < 20)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 22 && armCurlUpperBodyFrequency < middlePercentageFemale_75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_75_To_79Level4 && armCurlUpperBodyFrequency < 24)
                    {
                        return level5;
                    }
                }
                // 80 到 89 歲
                else if (age >= 80 && age <= 89)
                {
                    if (armCurlUpperBodyFrequency <= 12)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 13 && armCurlUpperBodyFrequency <= 15)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 16 && armCurlUpperBodyFrequency <= 18)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 19 && armCurlUpperBodyFrequency <= 21)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 22)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 12 && armCurlUpperBodyFrequency < middlePercentageFemale_80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_80_To_84Level1 && armCurlUpperBodyFrequency < 13)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 15 && armCurlUpperBodyFrequency < middlePercentageFemale_80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_80_To_84Level2 && armCurlUpperBodyFrequency < 16)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 18 && armCurlUpperBodyFrequency < middlePercentageFemale_80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_80_To_84Level3 && armCurlUpperBodyFrequency < 19)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 21 && armCurlUpperBodyFrequency < middlePercentageFemale_80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale_80_To_84Level4 && armCurlUpperBodyFrequency < 22)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else
                {
                    if (armCurlUpperBodyFrequency <= 9)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency >= 10 && armCurlUpperBodyFrequency <= 12)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency >= 13 && armCurlUpperBodyFrequency <= 16)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency >= 16 && armCurlUpperBodyFrequency <= 21)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency >= 22)
                    {
                        return level5;
                    }


                    else if (armCurlUpperBodyFrequency > 9 && armCurlUpperBodyFrequency < middlePercentageFemale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale90OverLevel1 && armCurlUpperBodyFrequency < 10)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > 12 && armCurlUpperBodyFrequency < middlePercentageFemale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale90OverLevel2 && armCurlUpperBodyFrequency < 13)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > 14 && armCurlUpperBodyFrequency < middlePercentageFemale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale90OverLevel3 && armCurlUpperBodyFrequency < 16)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > 21 && armCurlUpperBodyFrequency < middlePercentageFemale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (armCurlUpperBodyFrequency > middlePercentageFemale90OverLevel4 && armCurlUpperBodyFrequency < 22)
                    {
                        return level5;
                    }
                }
            }

            return 100;
        }

        /// <summary>
        /// 體適能-肌力與肌耐力(起立坐下)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="armCurlLowerBodyFrequency">起立坐下次數</param>
        /// <returns></returns>
        public int PhysicalFitnessArmCurlLowerBodyFrequencyLevel(string gender, int age, int armCurlLowerBodyFrequency)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;

            // 男性中間百分比
            double middlePercentageMale65_To_69Level1 = (12 + 13) / 2.0;
            double middlePercentageMale65_To_69Level2 = (15 + 16) / 2.0;
            double middlePercentageMale65_To_69Level3 = (18 + 19) / 2.0;
            double middlePercentageMale65_To_69Level4 = (22 + 23) / 2.0;
            double middlePercentageMale70_To_74Level1 = (12 + 13) / 2.0;
            double middlePercentageMale70_To_74Level2 = (14 + 15) / 2.0;
            double middlePercentageMale70_To_74Level3 = (17 + 18) / 2.0;
            double middlePercentageMale70_To_74Level4 = (20 + 22) / 2.0;
            double middlePercentageMale75_To_79Level1 = (10 + 11) / 2.0;
            double middlePercentageMale75_To_79Level2 = (13 + 14) / 2.0;
            double middlePercentageMale75_To_79Level3 = (15 + 16) / 2.0;
            double middlePercentageMale75_To_79Level4 = (19 + 20) / 2.0;
            double middlePercentageMale80_To_84Level1 = (10 + 11) / 2.0;
            double middlePercentageMale80_To_84Level2 = (13 + 14) / 2.0;
            double middlePercentageMale80_To_84Level3 = (15 + 16) / 2.0;
            double middlePercentageMale80_To_84Level4 = (19 + 20) / 2.0;
            double middlePercentageMale85_To_89Level1 = (9 + 10) / 2.0;
            double middlePercentageMale85_To_89Level2 = (10 + 11) / 2.0;
            double middlePercentageMale85_To_89Level3 = (12 + 13) / 2.0;
            double middlePercentageMale85_To_89Level4 = (15 + 16) / 2.0;
            double middlePercentageMale90OverLevel1 = (8 + 9) / 2.0;
            double middlePercentageMale90OverLevel2 = (9 + 10) / 2.0;
            double middlePercentageMale90OverLevel3 = (11 + 12) / 2.0;
            double middlePercentageMale90OverLevel4 = (14 + 15) / 2.0;

            // 女性中間百分比
            double middlePercentageFemale_65_To_69Level1 = (12 + 13) / 2.0;
            double middlePercentageFemale_65_To_69Level2 = (15 + 16) / 2.0;
            double middlePercentageFemale_65_To_69Level3 = (17 + 18) / 2.0;
            double middlePercentageFemale_65_To_69Level4 = (20 + 21) / 2.0;
            double middlePercentageFemale_70_To_74Level1 = (11 + 12) / 2.0;
            double middlePercentageFemale_70_To_74Level2 = (13 + 14) / 2.0;
            double middlePercentageFemale_70_To_74Level3 = (15 + 16) / 2.0;
            double middlePercentageFemale_70_To_74Level4 = (19 + 20) / 2.0;
            double middlePercentageFemale_75_To_79Level1 = (10 + 11) / 2.0;
            double middlePercentageFemale_75_To_79Level2 = (12 + 13) / 2.0;
            double middlePercentageFemale_75_To_79Level3 = (15 + 16) / 2.0;
            double middlePercentageFemale_75_To_79Level4 = (17 + 18) / 2.0;
            double middlePercentageFemale_80_To_84Level1 = (9 + 10) / 2.0;
            double middlePercentageFemale_80_To_84Level2 = (11 + 12) / 2.0;
            double middlePercentageFemale_80_To_84Level3 = (13 + 14) / 2.0;
            double middlePercentageFemale_80_To_84Level4 = (16 + 18) / 2.0;
            double middlePercentageFemale_85_To_89Level1 = (8 + 9) / 2.0;
            double middlePercentageFemale_85_To_89Level2 = (10 + 11) / 2.0;
            double middlePercentageFemale_85_To_89Level3 = (12 + 13) / 2.0;
            double middlePercentageFemale_85_To_89Level4 = (15 + 16) / 2.0;
            double middlePercentageFemale90OverLevel1 = (7 + 8) / 2.0;
            double middlePercentageFemale90OverLevel2 = (9 + 10) / 2.0;
            double middlePercentageFemale90OverLevel3 = (11 + 12) / 2.0;
            double middlePercentageFemale90OverLevel4 = (14 + 15) / 2.0;

            if (gender == "男")
            {
                // 65 歲以上 69 歲
                if (age > 65 && age <= 69)
                {
                    if (armCurlLowerBodyFrequency <= 13)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 13 && armCurlLowerBodyFrequency <= 15)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 16 && armCurlLowerBodyFrequency <= 18)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 19 && armCurlLowerBodyFrequency <= 22)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 23)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 12 && armCurlLowerBodyFrequency < middlePercentageMale65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale65_To_69Level1 && armCurlLowerBodyFrequency < 13)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 15 && armCurlLowerBodyFrequency < middlePercentageMale65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale65_To_69Level2 && armCurlLowerBodyFrequency < 16)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 18 && armCurlLowerBodyFrequency < middlePercentageMale65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale65_To_69Level3 && armCurlLowerBodyFrequency < 19)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 22 && armCurlLowerBodyFrequency < middlePercentageMale65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale65_To_69Level4 && armCurlLowerBodyFrequency < 23)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (armCurlLowerBodyFrequency <= 12)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 13 && armCurlLowerBodyFrequency <= 14)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 15 && armCurlLowerBodyFrequency <= 17)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 18 && armCurlLowerBodyFrequency <= 20)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 22)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 12 && armCurlLowerBodyFrequency < middlePercentageMale70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale70_To_74Level1 && armCurlLowerBodyFrequency < 13)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 14 && armCurlLowerBodyFrequency < middlePercentageMale70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale70_To_74Level2 && armCurlLowerBodyFrequency < 15)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 17 && armCurlLowerBodyFrequency < middlePercentageMale70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale70_To_74Level3 && armCurlLowerBodyFrequency < 18)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 20 && armCurlLowerBodyFrequency < middlePercentageMale70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale70_To_74Level4 && armCurlLowerBodyFrequency < 22)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (armCurlLowerBodyFrequency <= 11)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 11 && armCurlLowerBodyFrequency <= 13)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 14 && armCurlLowerBodyFrequency <= 15)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 16 && armCurlLowerBodyFrequency <= 19)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 20)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 10 && armCurlLowerBodyFrequency < middlePercentageMale75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale75_To_79Level1 && armCurlLowerBodyFrequency < 11)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 13 && armCurlLowerBodyFrequency < middlePercentageMale75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale75_To_79Level2 && armCurlLowerBodyFrequency < 14)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 15 && armCurlLowerBodyFrequency < middlePercentageMale75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale75_To_79Level3 && armCurlLowerBodyFrequency < 16)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 19 && armCurlLowerBodyFrequency < middlePercentageMale75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale75_To_79Level4 && armCurlLowerBodyFrequency < 20)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (armCurlLowerBodyFrequency <= 10)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 10 && armCurlLowerBodyFrequency <= 12)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 12 && armCurlLowerBodyFrequency <= 14)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 15 && armCurlLowerBodyFrequency <= 17)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 18)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 9 && armCurlLowerBodyFrequency < middlePercentageMale80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale80_To_84Level1 && armCurlLowerBodyFrequency < 10)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 11 && armCurlLowerBodyFrequency < middlePercentageMale80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale80_To_84Level2 && armCurlLowerBodyFrequency < 12)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 14 && armCurlLowerBodyFrequency < middlePercentageMale80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale80_To_84Level3 && armCurlLowerBodyFrequency < 15)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 17 && armCurlLowerBodyFrequency < middlePercentageMale80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale80_To_84Level4 && armCurlLowerBodyFrequency < 18)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (armCurlLowerBodyFrequency <= 9)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 10 && armCurlLowerBodyFrequency <= 10)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 11 && armCurlLowerBodyFrequency <= 12)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 13 && armCurlLowerBodyFrequency <= 15)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 16)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 9 && armCurlLowerBodyFrequency < middlePercentageMale85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale85_To_89Level1 && armCurlLowerBodyFrequency < 10)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 11 && armCurlLowerBodyFrequency < middlePercentageMale85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale85_To_89Level2 && armCurlLowerBodyFrequency < 12)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 14 && armCurlLowerBodyFrequency < middlePercentageMale85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale85_To_89Level3 && armCurlLowerBodyFrequency < 15)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 17 && armCurlLowerBodyFrequency < middlePercentageMale85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale85_To_89Level4 && armCurlLowerBodyFrequency < 18)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (armCurlLowerBodyFrequency <= 8)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 9 && armCurlLowerBodyFrequency <= 9)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 10 && armCurlLowerBodyFrequency <= 11)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 12 && armCurlLowerBodyFrequency <= 14)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 15)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 8 && armCurlLowerBodyFrequency < middlePercentageMale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale90OverLevel1 && armCurlLowerBodyFrequency < 9)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 9 && armCurlLowerBodyFrequency < middlePercentageMale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale90OverLevel2 && armCurlLowerBodyFrequency < 10)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 11 && armCurlLowerBodyFrequency < middlePercentageMale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale90OverLevel3 && armCurlLowerBodyFrequency < 12)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 14 && armCurlLowerBodyFrequency < middlePercentageMale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageMale90OverLevel4 && armCurlLowerBodyFrequency < 15)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (armCurlLowerBodyFrequency <= 12)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 13 && armCurlLowerBodyFrequency <= 15)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 16 && armCurlLowerBodyFrequency <= 17)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 18 && armCurlLowerBodyFrequency <= 20)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 21)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 12 && armCurlLowerBodyFrequency < middlePercentageFemale_65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_65_To_69Level1 && armCurlLowerBodyFrequency < 13)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 15 && armCurlLowerBodyFrequency < middlePercentageFemale_65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_65_To_69Level2 && armCurlLowerBodyFrequency < 16)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 17 && armCurlLowerBodyFrequency < middlePercentageFemale_65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_65_To_69Level3 && armCurlLowerBodyFrequency < 18)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 20 && armCurlLowerBodyFrequency < middlePercentageFemale_65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_65_To_69Level4 && armCurlLowerBodyFrequency < 21)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (armCurlLowerBodyFrequency <= 11)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 12 && armCurlLowerBodyFrequency <= 13)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 14 && armCurlLowerBodyFrequency <= 15)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 16 && armCurlLowerBodyFrequency <= 19)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 20)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 11 && armCurlLowerBodyFrequency < middlePercentageFemale_70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_70_To_74Level1 && armCurlLowerBodyFrequency < 12)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 13 && armCurlLowerBodyFrequency < middlePercentageFemale_70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_70_To_74Level2 && armCurlLowerBodyFrequency < 14)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 15 && armCurlLowerBodyFrequency < middlePercentageFemale_70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_70_To_74Level3 && armCurlLowerBodyFrequency < 16)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 19 && armCurlLowerBodyFrequency < middlePercentageFemale_70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_70_To_74Level4 && armCurlLowerBodyFrequency < 20)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (armCurlLowerBodyFrequency <= 10)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 11 && armCurlLowerBodyFrequency <= 12)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 13 && armCurlLowerBodyFrequency <= 15)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 16 && armCurlLowerBodyFrequency <= 17)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 18)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 10 && armCurlLowerBodyFrequency < middlePercentageFemale_75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_75_To_79Level1 && armCurlLowerBodyFrequency < 11)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 12 && armCurlLowerBodyFrequency < middlePercentageFemale_75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_75_To_79Level2 && armCurlLowerBodyFrequency < 13)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 15 && armCurlLowerBodyFrequency < middlePercentageFemale_75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_75_To_79Level3 && armCurlLowerBodyFrequency < 16)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 17 && armCurlLowerBodyFrequency < middlePercentageFemale_75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_75_To_79Level4 && armCurlLowerBodyFrequency < 18)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (armCurlLowerBodyFrequency <= 9)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 10 && armCurlLowerBodyFrequency <= 11)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 12 && armCurlLowerBodyFrequency <= 13)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 14 && armCurlLowerBodyFrequency <= 16)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 18)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 9 && armCurlLowerBodyFrequency < middlePercentageFemale_80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_80_To_84Level1 && armCurlLowerBodyFrequency < 10)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 11 && armCurlLowerBodyFrequency < middlePercentageFemale_80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_80_To_84Level2 && armCurlLowerBodyFrequency < 12)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 13 && armCurlLowerBodyFrequency < middlePercentageFemale_80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_80_To_84Level3 && armCurlLowerBodyFrequency < 14)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 16 && armCurlLowerBodyFrequency < middlePercentageFemale_80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_80_To_84Level4 && armCurlLowerBodyFrequency < 18)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (armCurlLowerBodyFrequency <= 8)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 9 && armCurlLowerBodyFrequency <= 10)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 11 && armCurlLowerBodyFrequency <= 12)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 13 && armCurlLowerBodyFrequency <= 15)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 16)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 8 && armCurlLowerBodyFrequency < middlePercentageFemale_85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_85_To_89Level1 && armCurlLowerBodyFrequency < 9)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 10 && armCurlLowerBodyFrequency < middlePercentageFemale_85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_85_To_89Level2 && armCurlLowerBodyFrequency < 11)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 12 && armCurlLowerBodyFrequency < middlePercentageFemale_85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_85_To_89Level3 && armCurlLowerBodyFrequency < 13)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 15 && armCurlLowerBodyFrequency < middlePercentageFemale_85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale_85_To_89Level4 && armCurlLowerBodyFrequency < 16)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (armCurlLowerBodyFrequency <= 7)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency >= 8 && armCurlLowerBodyFrequency <= 9)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency >= 10 && armCurlLowerBodyFrequency <= 11)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency >= 12 && armCurlLowerBodyFrequency <= 14)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency >= 15)
                    {
                        return level5;
                    }


                    else if (armCurlLowerBodyFrequency > 7 && armCurlLowerBodyFrequency < middlePercentageFemale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale90OverLevel1 && armCurlLowerBodyFrequency < 8)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > 9 && armCurlLowerBodyFrequency < middlePercentageFemale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale90OverLevel2 && armCurlLowerBodyFrequency < 10)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > 11 && armCurlLowerBodyFrequency < middlePercentageFemale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale90OverLevel3 && armCurlLowerBodyFrequency < 12)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > 14 && armCurlLowerBodyFrequency < middlePercentageFemale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (armCurlLowerBodyFrequency > middlePercentageFemale90OverLevel4 && armCurlLowerBodyFrequency < 15)
                    {
                        return level5;
                    }
                }
            }

            return 100;
        }

        /// <summary>
        /// 體適能-柔軟度(壯年坐姿體前彎)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="seatedForwardBendOne">第一次坐姿體前彎距離</param>
        /// <param name="seatedForwardBendTwo">第二次坐姿體前彎距離</param>
        /// <param name="seatedForwardBendThree">第三次坐姿體前彎距離</param>
        /// <returns></returns>
        public int PhysicalFitnessSeatedForwardBend(string gender, int age, double seatedForwardBendOne, double seatedForwardBendTwo, double seatedForwardBendThree)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;

            // 坐姿體前彎，取最大距離
            double[] seatedForwardBendArray = new double[3] { seatedForwardBendOne, seatedForwardBendTwo, seatedForwardBendThree };
            Array.Sort(seatedForwardBendArray);
            double seatedForwardBend = seatedForwardBendArray.LastOrDefault();

            if (gender == "男")
            {
                // 21 到 25 歲
                if (age >= 21 && age <= 25)
                {
                    if (seatedForwardBend <= 18)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 19 && seatedForwardBend <= 26)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 27 && seatedForwardBend <= 31)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 32 && seatedForwardBend <= 37)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 38)
                    {
                        return level5;
                    }
                }
                // 26 到 30 歲
                else if (age >= 26 && age <= 30)
                {
                    if (seatedForwardBend <= 14)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 15 && seatedForwardBend <= 23)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 24 && seatedForwardBend <= 29)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 30 && seatedForwardBend <= 35)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 36)
                    {
                        return level5;
                    }
                }
                // 31 到 35 歲
                else if (age >= 31 && age <= 35)
                {
                    if (seatedForwardBend <= 14)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 15 && seatedForwardBend <= 22)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 23 && seatedForwardBend <= 27)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 28 && seatedForwardBend <= 35)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 36)
                    {
                        return level5;
                    }
                }
                // 36 到 40 歲
                else if (age >= 36 && age <= 40)
                {
                    if (seatedForwardBend <= 14)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 15 && seatedForwardBend <= 21)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 22 && seatedForwardBend <= 27)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 28 && seatedForwardBend <= 34)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 35)
                    {
                        return level5;
                    }
                }
                // 41 到 45 歲
                else if (age >= 41 && age <= 45)
                {
                    if (seatedForwardBend <= 13)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 14 && seatedForwardBend <= 21)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 22 && seatedForwardBend <= 27)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 28 && seatedForwardBend <= 33)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 34)
                    {
                        return level5;
                    }
                }
                // 46 到 50 歲
                else if (age >= 46 && age <= 50)
                {
                    if (seatedForwardBend <= 12)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 13 && seatedForwardBend <= 20)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 21 && seatedForwardBend <= 25)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 26 && seatedForwardBend <= 32)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 33)
                    {
                        return level5;
                    }
                }
                // 51 到 55 歲
                else if (age >= 51 && age <= 55)
                {
                    if (seatedForwardBend <= 12)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 13 && seatedForwardBend <= 18)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 19 && seatedForwardBend <= 25)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 26 && seatedForwardBend <= 31)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 32)
                    {
                        return level5;
                    }
                }
                // 56 到 60 歲
                else if (age >= 56 && age <= 60)
                {
                    if (seatedForwardBend <= 9)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 10 && seatedForwardBend <= 15)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 16 && seatedForwardBend <= 23)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 24 && seatedForwardBend <= 31)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 32)
                    {
                        return level5;
                    }
                }
                // 61 到 65 歲
                else if (age >= 61 && age <= 65)
                {
                    if (seatedForwardBend <= 5)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 6 && seatedForwardBend <= 14)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 15 && seatedForwardBend <= 23)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 24 && seatedForwardBend <= 30)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 31)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 21 到 25 歲
                if (age >= 21 && age <= 25)
                {
                    if (seatedForwardBend <= 22)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 23 && seatedForwardBend <= 28)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 29 && seatedForwardBend <= 34)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 35 && seatedForwardBend <= 40)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 41)
                    {
                        return level5;
                    }
                }
                // 26 到 30 歲
                else if (age >= 26 && age <= 30)
                {
                    if (seatedForwardBend <= 20)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 21 && seatedForwardBend <= 27)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 28 && seatedForwardBend <= 33)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 34 && seatedForwardBend <= 39)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 40)
                    {
                        return level5;
                    }
                }
                // 31 到 35 歲
                else if (age >= 31 && age <= 35)
                {
                    if (seatedForwardBend <= 19)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 20 && seatedForwardBend <= 26)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 27 && seatedForwardBend <= 31)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 32 && seatedForwardBend <= 38)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 39)
                    {
                        return level5;
                    }
                }
                // 36 到 40 歲
                else if (age >= 36 && age <= 40)
                {
                    if (seatedForwardBend <= 18)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 19 && seatedForwardBend <= 25)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 26 && seatedForwardBend <= 30)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 31 && seatedForwardBend <= 38)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 39)
                    {
                        return level5;
                    }
                }
                // 41 到 45 歲
                else if (age >= 41 && age <= 45)
                {
                    if (seatedForwardBend <= 18)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 19 && seatedForwardBend <= 25)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 26 && seatedForwardBend <= 31)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 32 && seatedForwardBend <= 38)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 39)
                    {
                        return level5;
                    }
                }
                // 46 到 50 歲
                else if (age >= 46 && age <= 50)
                {
                    if (seatedForwardBend <= 18)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 19 && seatedForwardBend <= 25)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 26 && seatedForwardBend <= 30)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 31 && seatedForwardBend <= 38)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 39)
                    {
                        return level5;
                    }
                }
                // 51 到 55 歲
                else if (age >= 51 && age <= 55)
                {
                    if (seatedForwardBend <= 17)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 18 && seatedForwardBend <= 25)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 26 && seatedForwardBend <= 28)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 29 && seatedForwardBend <= 38)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 39)
                    {
                        return level5;
                    }
                }
                // 56 到 60 歲
                else if (age >= 56 && age <= 60)
                {
                    if (seatedForwardBend <= 15)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 16 && seatedForwardBend <= 25)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 26 && seatedForwardBend <= 28)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 29 && seatedForwardBend <= 38)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 39)
                    {
                        return level5;
                    }
                }
                // 61 到 65 歲
                else if (age >= 61 && age <= 65)
                {
                    if (seatedForwardBend <= 12)
                    {
                        return level1;
                    }
                    else if (seatedForwardBend >= 13 && seatedForwardBend <= 24)
                    {
                        return level2;
                    }
                    else if (seatedForwardBend >= 25 && seatedForwardBend <= 27)
                    {
                        return level3;
                    }
                    else if (seatedForwardBend >= 28 && seatedForwardBend <= 36)
                    {
                        return level4;
                    }
                    else if (seatedForwardBend >= 37)
                    {
                        return level5;
                    }
                }
            }

            return level5;
        }

        /// <summary>
        /// 體適能-柔軟度(老年椅子坐姿體前彎)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="chairSeatedForwardBendOne">第一次椅子坐姿體前彎距離</param>
        /// <param name="chairSeatedForwardBendTwo">第二次椅子坐姿體前彎距離</param>
        /// <param name="chairSeatedForwardBendThree">第三次椅子坐姿體前彎距離</param>
        /// <returns></returns>
        public int PhysicalFitnessChairSeatedForwardBend(string gender, int age, double chairSeatedForwardBendOne, double chairSeatedForwardBendTwo, double chairSeatedForwardBendThree)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;
            // 男性百分比
            double middlePercentageMale65_To_69Level1 = (-6.0 + -3.0) / 2.0;
            double middlePercentageMale65_To_69Level2 = (0.0 + 1.0) / 2.0;
            double middlePercentageMale65_To_69Level3 = (4.0 + 5.0) / 2.0;
            double middlePercentageMale65_To_69Level4 = (10.0 + 13.0) / 2.0;
            double middlePercentageMale70_To_74Level1 = (-7.0 + -4.0) / 2.0;
            double middlePercentageMale70_To_74Level2 = (0.0 + 1.0) / 2.0;
            double middlePercentageMale70_To_74Level3 = (4.0 + 5.0) / 2.0;
            double middlePercentageMale70_To_74Level4 = (10.8 + 13.0) / 2.0;
            double middlePercentageMale75_To_79Level1 = (-8.0 + -5.0) / 2.0;
            double middlePercentageMale75_To_79Level2 = (0.0 + 1.0) / 2.0;
            double middlePercentageMale75_To_79Level3 = (3.0 + 4.0) / 2.0;
            double middlePercentageMale75_To_79Level4 = (10.0 + 12.0) / 2.0;
            double middlePercentageMale80_To_84Level1 = (-11.0 + -8.5) / 2.0;
            double middlePercentageMale80_To_84Level2 = (-2.0 + 0.0) / 2.0;
            double middlePercentageMale80_To_84Level3 = (1.0 + 2.0) / 2.0;
            double middlePercentageMale80_To_84Level4 = (7.0 + 10.0) / 2.0;
            double middlePercentageMale85_To_89Level1 = (-12.0 + -10.0) / 2.0;
            double middlePercentageMale85_To_89Level2 = (-5.0 + -3.0) / 2.0;
            double middlePercentageMale85_To_89Level3 = (0.0 + 1.0) / 2.0;
            double middlePercentageMale85_To_89Level4 = (5.0 + 8.0) / 2.0;
            double middlePercentageMale90OverLevel1 = (-14.0 + -12.0) / 2.0;
            double middlePercentageMale90OverLevel2 = (-4.4 + -1.0) / 2.0;
            double middlePercentageMale90OverLevel3 = (0.0 + 1.0) / 2.0;
            double middlePercentageMale90OverLevel4 = (5.0 + 7.0) / 2.0;

            // 女性百分比
            double middlePercentageFemale_65_To_69Level1 = (0.0 + 1.0) / 2.0;
            double middlePercentageFemale_65_To_69Level2 = (3.0 + 4.0) / 2.0;
            double middlePercentageFemale_65_To_69Level3 = (8.0 + 10.0) / 2.0;
            double middlePercentageFemale_65_To_69Level4 = (15.0 + 17.0) / 2.0;
            double middlePercentageFemale_70_To_74Level1 = (0.0 + 1.0) / 2.0;
            double middlePercentageFemale_70_To_74Level2 = (2.0 + 3.0) / 2.0;
            double middlePercentageFemale_70_To_74Level3 = (7.0 + 8.9) / 2.0;
            double middlePercentageFemale_70_To_74Level4 = (14.0 + 16.0) / 2.0;
            double middlePercentageFemale_75_To_79Level1 = (0.0 + 1.0) / 2.0;
            double middlePercentageFemale_75_To_79Level2 = (2.0 + 3.0) / 2.0;
            double middlePercentageFemale_75_To_79Level3 = (6.0 + 7.0) / 2.0;
            double middlePercentageFemale_75_To_79Level4 = (12.0 + 14.0) / 2.0;
            double middlePercentageFemale_80_To_84Level1 = (-2.0 + 0.0) / 2.0;
            double middlePercentageFemale_80_To_84Level2 = (1.0 + 2.0) / 2.0;
            double middlePercentageFemale_80_To_84Level3 = (4.0 + 5.0) / 2.0;
            double middlePercentageFemale_80_To_84Level4 = (10.0 + 12.0) / 2.0;
            double middlePercentageFemale_85_To_89Level1 = (-3.8 + -1.8) / 2.0;
            double middlePercentageFemale_85_To_89Level2 = (0.0 + 1.0) / 2.0;
            double middlePercentageFemale_85_To_89Level3 = (2.0 + 3.0) / 2.0;
            double middlePercentageFemale_85_To_89Level4 = (8.0 + 10.0) / 2.0;
            double middlePercentageFemale90OverLevel1 = (-6.8 + -1.5) / 2.0;
            double middlePercentageFemale90OverLevel2 = (0.0 + 1.0) / 2.0;
            double middlePercentageFemale90OverLevel3 = (2.0 + 2.6) / 2.0;
            double middlePercentageFemale90OverLevel4 = (5.4 + 7.0) / 2.0;

            // 椅子坐姿體前彎，取最大距離
            double[] chairSeatedForwardBendArray = new double[] { chairSeatedForwardBendOne, chairSeatedForwardBendTwo, chairSeatedForwardBendThree };
            Array.Sort(chairSeatedForwardBendArray);
            double chairSeatedForwardBend = chairSeatedForwardBendArray.LastOrDefault();

            if (gender == "男")
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (chairSeatedForwardBend <= -6.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= -3.0 && chairSeatedForwardBend <= 0.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 4.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 5.0 && chairSeatedForwardBend <= 10.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 13.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > 12 && chairSeatedForwardBend < middlePercentageMale65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale65_To_69Level1 && chairSeatedForwardBend < 13)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > 15 && chairSeatedForwardBend < middlePercentageMale65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale65_To_69Level2 && chairSeatedForwardBend < 16)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 17 && chairSeatedForwardBend < middlePercentageMale65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale65_To_69Level3 && chairSeatedForwardBend < 18)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 20 && chairSeatedForwardBend < middlePercentageMale65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale65_To_69Level4 && chairSeatedForwardBend < 21)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (chairSeatedForwardBend <= -7.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= -4.0 && chairSeatedForwardBend <= 0.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 4.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 5.0 && chairSeatedForwardBend <= 10.8)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 13.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > -7.0 && chairSeatedForwardBend < middlePercentageMale70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale70_To_74Level1 && chairSeatedForwardBend < -4.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > 0.0 && chairSeatedForwardBend < middlePercentageMale70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale70_To_74Level2 && chairSeatedForwardBend < 1.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 4.0 && chairSeatedForwardBend < middlePercentageMale70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale70_To_74Level3 && chairSeatedForwardBend < 5.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 10.8 && chairSeatedForwardBend < middlePercentageMale70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale70_To_74Level4 && chairSeatedForwardBend < 13.0)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (chairSeatedForwardBend <= -8.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= -5.0 && chairSeatedForwardBend <= 0.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 3.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 4.0 && chairSeatedForwardBend <= 10.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 12.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > -8.0 && chairSeatedForwardBend < middlePercentageMale75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale75_To_79Level1 && chairSeatedForwardBend < -5.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > 0.0 && chairSeatedForwardBend < middlePercentageMale75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale75_To_79Level2 && chairSeatedForwardBend < 1.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 3.0 && chairSeatedForwardBend < middlePercentageMale75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale75_To_79Level3 && chairSeatedForwardBend < 4.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 10.0 && chairSeatedForwardBend < middlePercentageMale75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale75_To_79Level4 && chairSeatedForwardBend < 12.0)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (chairSeatedForwardBend <= -11.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= -8.5 && chairSeatedForwardBend <= -2.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 0.0 && chairSeatedForwardBend <= 1.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 2.0 && chairSeatedForwardBend <= 7.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 10.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > -11.0 && chairSeatedForwardBend < middlePercentageMale80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale80_To_84Level1 && chairSeatedForwardBend < -8.5)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > -2.0 && chairSeatedForwardBend < middlePercentageMale80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale80_To_84Level2 && chairSeatedForwardBend < 0.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 1.0 && chairSeatedForwardBend < middlePercentageMale80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale80_To_84Level3 && chairSeatedForwardBend < 2.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 7.0 && chairSeatedForwardBend < middlePercentageMale80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale80_To_84Level4 && chairSeatedForwardBend < 10.0)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (chairSeatedForwardBend <= -12.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= -10.0 && chairSeatedForwardBend <= -5.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= -3.0 && chairSeatedForwardBend <= 0.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 5.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 8.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > -12.0 && chairSeatedForwardBend < middlePercentageMale85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale85_To_89Level1 && chairSeatedForwardBend < -10.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > -5.0 && chairSeatedForwardBend < middlePercentageMale85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale85_To_89Level2 && chairSeatedForwardBend < -3.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 0.0 && chairSeatedForwardBend < middlePercentageMale85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale85_To_89Level3 && chairSeatedForwardBend < 1.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 5.0 && chairSeatedForwardBend < middlePercentageMale85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale85_To_89Level4 && chairSeatedForwardBend < 8.0)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (chairSeatedForwardBend <= -14.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= -12.0 && chairSeatedForwardBend <= -4.4)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= -1.0 && chairSeatedForwardBend <= 0.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 5.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 7.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > -14.0 && chairSeatedForwardBend < middlePercentageMale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale90OverLevel1 && chairSeatedForwardBend < -12.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > -4.4 && chairSeatedForwardBend < middlePercentageMale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale90OverLevel2 && chairSeatedForwardBend < -1.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 0.0 && chairSeatedForwardBend < middlePercentageMale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale90OverLevel3 && chairSeatedForwardBend < 1.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 5.0 && chairSeatedForwardBend < middlePercentageMale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageMale90OverLevel4 && chairSeatedForwardBend < 7.0)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (chairSeatedForwardBend <= 0.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 3.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 4.0 && chairSeatedForwardBend <= 8.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 10.0 && chairSeatedForwardBend <= 15.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 17.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > 0.0 && chairSeatedForwardBend < middlePercentageFemale_65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_65_To_69Level1 && chairSeatedForwardBend < 1.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > 3.0 && chairSeatedForwardBend < middlePercentageFemale_65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_65_To_69Level2 && chairSeatedForwardBend < 4.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 8.0 && chairSeatedForwardBend < middlePercentageFemale_65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_65_To_69Level3 && chairSeatedForwardBend < 10.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 15.0 && chairSeatedForwardBend < middlePercentageFemale_65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_65_To_69Level4 && chairSeatedForwardBend < 17.0)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (chairSeatedForwardBend <= 0.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 2.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 3.0 && chairSeatedForwardBend <= 7.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 8.9 && chairSeatedForwardBend <= 14.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 16.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > 0.0 && chairSeatedForwardBend < middlePercentageFemale_70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_70_To_74Level1 && chairSeatedForwardBend < 1.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > 2.0 && chairSeatedForwardBend < middlePercentageFemale_70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_70_To_74Level2 && chairSeatedForwardBend < 3.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 7.0 && chairSeatedForwardBend < middlePercentageFemale_70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_70_To_74Level3 && chairSeatedForwardBend < 8.9)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 14.0 && chairSeatedForwardBend < middlePercentageFemale_70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_70_To_74Level4 && chairSeatedForwardBend < 16.0)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (chairSeatedForwardBend <= 0.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 2.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 3.0 && chairSeatedForwardBend <= 6.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 7.0 && chairSeatedForwardBend <= 12.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 14.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > 0.0 && chairSeatedForwardBend < middlePercentageFemale_75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_75_To_79Level1 && chairSeatedForwardBend < 1.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > 2.0 && chairSeatedForwardBend < middlePercentageFemale_75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_75_To_79Level2 && chairSeatedForwardBend < 3.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 6.0 && chairSeatedForwardBend < middlePercentageFemale_75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_75_To_79Level3 && chairSeatedForwardBend < 7.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 12.0 && chairSeatedForwardBend < middlePercentageFemale_75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_75_To_79Level4 && chairSeatedForwardBend < 14.0)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (chairSeatedForwardBend <= -2.0)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= 0.0 && chairSeatedForwardBend <= 1.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 2.0 && chairSeatedForwardBend <= 4.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 5.0 && chairSeatedForwardBend <= 10.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 12.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > -2.0 && chairSeatedForwardBend < middlePercentageFemale_80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_80_To_84Level1 && chairSeatedForwardBend < 0.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > 1.0 && chairSeatedForwardBend < middlePercentageFemale_80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_80_To_84Level2 && chairSeatedForwardBend < 2.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 4.0 && chairSeatedForwardBend < middlePercentageFemale_80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_80_To_84Level3 && chairSeatedForwardBend < 5.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 10.0 && chairSeatedForwardBend < middlePercentageFemale_80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_80_To_84Level4 && chairSeatedForwardBend < 12.0)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (chairSeatedForwardBend <= -3.8)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= -1.8 && chairSeatedForwardBend <= 0.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 2.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 3.0 && chairSeatedForwardBend <= 8.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 10.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > -3.8 && chairSeatedForwardBend < middlePercentageFemale_85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_85_To_89Level1 && chairSeatedForwardBend < -1.8)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > 0.0 && chairSeatedForwardBend < middlePercentageFemale_85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_85_To_89Level2 && chairSeatedForwardBend < 1.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 2.0 && chairSeatedForwardBend < middlePercentageFemale_85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_85_To_89Level3 && chairSeatedForwardBend < 3.0)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 8.0 && chairSeatedForwardBend < middlePercentageFemale_85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale_85_To_89Level4 && chairSeatedForwardBend < 10.0)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (chairSeatedForwardBend <= -6.8)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend >= -1.5 && chairSeatedForwardBend <= 0.0)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend >= 1.0 && chairSeatedForwardBend <= 2.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend >= 2.6 && chairSeatedForwardBend <= 5.4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend >= 7.0)
                    {
                        return level5;
                    }


                    else if (chairSeatedForwardBend > -6.8 && chairSeatedForwardBend < middlePercentageFemale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale90OverLevel1 && chairSeatedForwardBend < -1.5)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > 0.0 && chairSeatedForwardBend < middlePercentageFemale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale90OverLevel2 && chairSeatedForwardBend < 1.0)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > 2.0 && chairSeatedForwardBend < middlePercentageFemale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale90OverLevel3 && chairSeatedForwardBend < 2.6)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > 5.4 && chairSeatedForwardBend < middlePercentageFemale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (chairSeatedForwardBend > middlePercentageFemale90OverLevel4 && chairSeatedForwardBend < 7.0)
                    {
                        return level5;
                    }
                }
            }

            return 100;
        }

        /// <summary>
        /// 體適能-柔軟度(拉背測驗)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="pullBackTestOne">第一次拉背測驗</param>
        /// <param name="pullBackTestTwo">第二次拉背測驗</param>
        /// <returns></returns>
        public int PhysicalFitnessPullBackTest(string gender, int age, double pullBackTestOne, double pullBackTestTwo)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;

            // 男性百分比
            double middlePercentageMale65_To_69Level1 = (-20.0 + -18.0) / 2.0;
            double middlePercentageMale65_To_69Level2 = (-11.0 + -9.0) / 2.0;
            double middlePercentageMale65_To_69Level3 = (-3.0 + 0.0) / 2.0;
            double middlePercentageMale65_To_69Level4 = (2.0 + 3.0) / 2.0;
            double middlePercentageMale70_To_74Level1 = (-23.0 + -20.0) / 2.0;
            double middlePercentageMale70_To_74Level2 = (-15.0 + -13.0) / 2.0;
            double middlePercentageMale70_To_74Level3 = (-6.1 + -4.0) / 2.0;
            double middlePercentageMale70_To_74Level4 = (1.0 + 2.0) / 2.0;
            double middlePercentageMale75_To_79Level1 = (-25.0 + -23.0) / 2.0;
            double middlePercentageMale75_To_79Level2 = (-17.0 + -15.0) / 2.0;
            double middlePercentageMale75_To_79Level3 = (-9.0 + -6.0) / 2.0;
            double middlePercentageMale75_To_79Level4 = (0.0 + 2.0) / 2.0;
            double middlePercentageMale80_To_84Level1 = (-28.0 + -25.0) / 2.0;
            double middlePercentageMale80_To_84Level2 = (-19.0 + -16.0) / 2.0;
            double middlePercentageMale80_To_84Level3 = (-10.0 + -9.0) / 2.0;
            double middlePercentageMale80_To_84Level4 = (0.0 + 1.0) / 2.0;
            double middlePercentageMale85_To_89Level1 = (-29.0 + -26.0) / 2.0;
            double middlePercentageMale85_To_89Level2 = (-20.0 + -19.0) / 2.0;
            double middlePercentageMale85_To_89Level3 = (-12.0 + -10.0) / 2.0;
            double middlePercentageMale85_To_89Level4 = (-1.0 + 0.0) / 2.0;
            double middlePercentageMale90OverLevel1 = (-30.0 + -26.5) / 2.0;
            double middlePercentageMale90OverLevel2 = (-21.8 + -20.0) / 2.0;
            double middlePercentageMale90OverLevel3 = (-15.0 + -14.0) / 2.0;
            double middlePercentageMale90OverLevel4 = (-1.6 + 0.9) / 2.0;

            // 女性百分比
            double middlePercentageFemale_65_To_69Level1 = (-10.0 + -8.0) / 2.0;
            double middlePercentageFemale_65_To_69Level2 = (-2.0 + 0.0) / 2.0;
            double middlePercentageFemale_65_To_69Level3 = (1.0 + 2.0) / 2.0;
            double middlePercentageFemale_65_To_69Level4 = (4.0 + 5.0) / 2.0;
            double middlePercentageFemale_70_To_74Level1 = (-14.0 + -11.0) / 2.0;
            double middlePercentageFemale_70_To_74Level2 = (-5.0 + -3.0) / 2.0;
            double middlePercentageFemale_70_To_74Level3 = (0.0 + 1.0) / 2.0;
            double middlePercentageFemale_70_To_74Level4 = (3.9 + 4.4) / 2.0;
            double middlePercentageFemale_75_To_79Level1 = (-17.0 + -15.0) / 2.0;
            double middlePercentageFemale_75_To_79Level2 = (-8.0 + -6.0) / 2.0;
            double middlePercentageFemale_75_To_79Level3 = (0.0 + 1.0) / 2.0;
            double middlePercentageFemale_75_To_79Level4 = (3.0 + 4.0) / 2.0;
            double middlePercentageFemale_80_To_84Level1 = (-20.0 + -17.0) / 2.0;
            double middlePercentageFemale_80_To_84Level2 = (-11.0 + -10.0) / 2.0;
            double middlePercentageFemale_80_To_84Level3 = (-3.0 + 0.0) / 2.0;
            double middlePercentageFemale_80_To_84Level4 = (2.0 + 3.0) / 2.0;
            double middlePercentageFemale_85_To_89Level1 = (-23.0 + -20.0) / 2.0;
            double middlePercentageFemale_85_To_89Level2 = (-14.0 + -12.0) / 2.0;
            double middlePercentageFemale_85_To_89Level3 = (-6.0 + -3.0) / 2.0;
            double middlePercentageFemale_85_To_89Level4 = (1.0 + 2.0) / 2.0;
            double middlePercentageFemale90OverLevel1 = (-38.2 + -22.4) / 2.0;
            double middlePercentageFemale90OverLevel2 = (-20.0 + -16.0) / 2.0;
            double middlePercentageFemale90OverLevel3 = (-1.1 + 2.0) / 2.0;
            double middlePercentageFemale90OverLevel4 = (2.1 + 4.0) / 2.0;

            // 椅子坐姿體前彎，取最大距離
            double[] pullBackTestArray = new double[] { pullBackTestOne, pullBackTestTwo };
            Array.Sort(pullBackTestArray);
            double pullBackTest = pullBackTestArray.LastOrDefault();

            if (gender == "男")
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (pullBackTest <= -20.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -18.0 && pullBackTest <= -11.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -9.0 && pullBackTest <= -3.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= 0.0 && pullBackTest <= 2.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 3.0)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -20.0 && pullBackTest < middlePercentageMale65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageMale65_To_69Level1 && pullBackTest < -18.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -11.0 && pullBackTest < middlePercentageMale65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageMale65_To_69Level2 && pullBackTest < -9.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > -3.0 && pullBackTest < middlePercentageMale65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageMale65_To_69Level3 && pullBackTest < 0.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 2.0 && pullBackTest < middlePercentageMale65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageMale65_To_69Level4 && pullBackTest < 3.0)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (pullBackTest <= -23.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -20.0 && pullBackTest <= -15.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -13.0 && pullBackTest <= -6.1)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= -4.0 && pullBackTest <= 1.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 2.0)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -23.0 && pullBackTest < middlePercentageMale70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageMale70_To_74Level1 && pullBackTest < -20.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -15.0 && pullBackTest < middlePercentageMale70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageMale70_To_74Level2 && pullBackTest < -13.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > -6.1 && pullBackTest < middlePercentageMale70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageMale70_To_74Level3 && pullBackTest < -4.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 1.0 && pullBackTest < middlePercentageMale70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageMale70_To_74Level4 && pullBackTest < 2.0)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (pullBackTest <= -25.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -23.0 && pullBackTest <= -17.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -15.0 && pullBackTest <= -9.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= -6.0 && pullBackTest <= 0.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 2.0)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -25.0 && pullBackTest < middlePercentageMale75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageMale75_To_79Level1 && pullBackTest < -23.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -17.0 && pullBackTest < middlePercentageMale75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageMale75_To_79Level2 && pullBackTest < -15.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > -9.0 && pullBackTest < middlePercentageMale75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageMale75_To_79Level3 && pullBackTest < -6.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 0.0 && pullBackTest < middlePercentageMale75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageMale75_To_79Level4 && pullBackTest < 2.0)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (pullBackTest <= -28.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -25.0 && pullBackTest <= -19.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -16.0 && pullBackTest <= -10.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= -9.0 && pullBackTest <= 0.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 1.0)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -28.0 && pullBackTest < middlePercentageMale80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageMale80_To_84Level1 && pullBackTest < -25.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -19.0 && pullBackTest < middlePercentageMale80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageMale80_To_84Level2 && pullBackTest < -16.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > -10.0 && pullBackTest < middlePercentageMale80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageMale80_To_84Level3 && pullBackTest < -9.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 0.0 && pullBackTest < middlePercentageMale80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageMale80_To_84Level4 && pullBackTest < 1.0)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (pullBackTest <= -29.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -26.0 && pullBackTest <= -20.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -19.0 && pullBackTest <= -12.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= -10.0 && pullBackTest <= -1.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 0.0)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -29.0 && pullBackTest < middlePercentageMale85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageMale85_To_89Level1 && pullBackTest < -26.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -20.0 && pullBackTest < middlePercentageMale85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageMale85_To_89Level2 && pullBackTest < -19.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > -12.0 && pullBackTest < middlePercentageMale85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageMale85_To_89Level3 && pullBackTest < -10.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > -1.0 && pullBackTest < middlePercentageMale85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageMale85_To_89Level4 && pullBackTest < 0.0)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (pullBackTest <= -30.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -26.5 && pullBackTest <= -21.8)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -20.0 && pullBackTest <= -15.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= -14.0 && pullBackTest <= -1.6)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 0.9)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -30.0 && pullBackTest < middlePercentageMale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageMale90OverLevel1 && pullBackTest < -26.5)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -21.8 && pullBackTest < middlePercentageMale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageMale90OverLevel2 && pullBackTest < -20.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > -15.0 && pullBackTest < middlePercentageMale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageMale90OverLevel3 && pullBackTest < -14.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > -1.6 && pullBackTest < middlePercentageMale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageMale90OverLevel4 && pullBackTest < 0.9)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (pullBackTest <= -10.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -8.0 && pullBackTest <= -2.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= 0.0 && pullBackTest <= 1.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= 2.0 && pullBackTest <= 4.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 5.0)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -10.0 && pullBackTest < middlePercentageFemale_65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageFemale_65_To_69Level1 && pullBackTest < -8.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -2.0 && pullBackTest < middlePercentageFemale_65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageFemale_65_To_69Level2 && pullBackTest < 0.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > 1.0 && pullBackTest < middlePercentageFemale_65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageFemale_65_To_69Level3 && pullBackTest < 2.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 4.0 && pullBackTest < middlePercentageFemale_65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageFemale_65_To_69Level4 && pullBackTest < 5.0)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (pullBackTest <= -14.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -11.0 && pullBackTest <= -5.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -3.0 && pullBackTest <= 0.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= 1.0 && pullBackTest <= 3.9)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 4.4)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -14.0 && pullBackTest < middlePercentageFemale_70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageFemale_70_To_74Level1 && pullBackTest < -11.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -5.0 && pullBackTest < middlePercentageFemale_70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageFemale_70_To_74Level2 && pullBackTest < -3.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > 0.0 && pullBackTest < middlePercentageFemale_70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageFemale_70_To_74Level3 && pullBackTest < 1.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 3.9 && pullBackTest < middlePercentageFemale_70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageFemale_70_To_74Level4 && pullBackTest < 4.4)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (pullBackTest <= -17.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -15.0 && pullBackTest <= -8.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -6.0 && pullBackTest <= 0.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= 1.0 && pullBackTest <= 3.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 4.0)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -17.0 && pullBackTest < middlePercentageFemale_75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageFemale_75_To_79Level1 && pullBackTest < -15.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -8.0 && pullBackTest < middlePercentageFemale_75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageFemale_75_To_79Level2 && pullBackTest < -6.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > 0.0 && pullBackTest < middlePercentageFemale_75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageFemale_75_To_79Level3 && pullBackTest < 1.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 3.0 && pullBackTest < middlePercentageFemale_75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageFemale_75_To_79Level4 && pullBackTest < 4.0)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (pullBackTest <= -20.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -17.0 && pullBackTest <= -11.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -10.0 && pullBackTest <= -3.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= 0.0 && pullBackTest <= 2.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 3.0)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -20.0 && pullBackTest < middlePercentageFemale_80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageFemale_80_To_84Level1 && pullBackTest < -17.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -11.0 && pullBackTest < middlePercentageFemale_80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageFemale_80_To_84Level2 && pullBackTest < -10.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > -3.0 && pullBackTest < middlePercentageFemale_80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageFemale_80_To_84Level3 && pullBackTest < 0.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 2.0 && pullBackTest < middlePercentageFemale_80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageFemale_80_To_84Level4 && pullBackTest < 3.0)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (pullBackTest <= -23.0)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -20.0 && pullBackTest <= -14.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -12.0 && pullBackTest <= -6.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= -3.0 && pullBackTest <= 1.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 2.0)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -23.0 && pullBackTest < middlePercentageFemale_85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageFemale_85_To_89Level1 && pullBackTest < -20.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -14.0 && pullBackTest < middlePercentageFemale_85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageFemale_85_To_89Level2 && pullBackTest < -12.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > -6.0 && pullBackTest < middlePercentageFemale_85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageFemale_85_To_89Level3 && pullBackTest < -3.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 1.0 && pullBackTest < middlePercentageFemale_85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageFemale_85_To_89Level4 && pullBackTest < 2.0)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (pullBackTest <= -22.4)
                    {
                        return level1;
                    }
                    else if (pullBackTest >= -20.0 && pullBackTest <= -16.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest >= -11.0 && pullBackTest <= -3.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest >= -1.1 && pullBackTest <= 2.0)
                    {
                        return level4;
                    }
                    else if (pullBackTest >= 2.1)
                    {
                        return level5;
                    }


                    else if (pullBackTest > -22.4 && pullBackTest < middlePercentageFemale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (pullBackTest > middlePercentageFemale90OverLevel1 && pullBackTest < -20.0)
                    {
                        return level2;
                    }
                    else if (pullBackTest > -16.0 && pullBackTest < middlePercentageFemale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (pullBackTest > middlePercentageFemale90OverLevel2 && pullBackTest < -11.0)
                    {
                        return level3;
                    }
                    else if (pullBackTest > -3.0 && pullBackTest < middlePercentageFemale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (pullBackTest > middlePercentageFemale90OverLevel3 && pullBackTest < -1.1)
                    {
                        return level4;
                    }
                    else if (pullBackTest > 2.0 && pullBackTest < middlePercentageFemale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (pullBackTest > middlePercentageFemale90OverLevel4 && pullBackTest < 2.1)
                    {
                        return level5;
                    }
                }
            }

            return 100;
        }

        /// <summary>
        /// 體適能-平衡(壯年開眼單足立)
        /// </summary>
        /// <param name="age"></param>
        /// <param name="adultEyeOpeningMonopodSecond"></param>
        /// <returns></returns>
        public int PhysicalFitnessAdultEyeOpeningMonopodSecond(int age, double adultEyeOpeningMonopodSecond)
        {
            int level1 = 33;
            int level2 = 66;
            int level3 = 100;

            // 21 歲以上 39 歲
            if (age >= 21 && age <= 39)
            {
                if (adultEyeOpeningMonopodSecond <= 38.1)
                {
                    return level1;
                }
                else if (adultEyeOpeningMonopodSecond >= 38.2 && adultEyeOpeningMonopodSecond <= 48.3)
                {
                    return level2;
                }
                else if (adultEyeOpeningMonopodSecond >= 48.4)
                {
                    return level3;
                }
            }
            // 40 到 49 歲
            else if (age >= 40 && age <= 49)
            {
                if (adultEyeOpeningMonopodSecond <= 35.0)
                {
                    return level1;
                }
                else if (adultEyeOpeningMonopodSecond >= 35.1 && adultEyeOpeningMonopodSecond <= 45.1)
                {
                    return level2;
                }
                else if (adultEyeOpeningMonopodSecond >= 45.2)
                {
                    return level3;
                }
            }
            // 50 到 59 歲
            else if (age >= 50 && age <= 59)
            {
                if (adultEyeOpeningMonopodSecond <= 33.0)
                {
                    return level1;
                }
                else if (adultEyeOpeningMonopodSecond >= 35.1 && adultEyeOpeningMonopodSecond <= 45.1)
                {
                    return level2;
                }
                else if (adultEyeOpeningMonopodSecond >= 45.2)
                {
                    return level3;
                }
            }
            // 60 到 65 歲
            else if (age >= 60 && age <= 65)
                {
                    if (adultEyeOpeningMonopodSecond <= 23.6)
                    {
                        return level1;
                    }
                    else if (adultEyeOpeningMonopodSecond >= 23.7 && adultEyeOpeningMonopodSecond <= 33.7)
                    {
                        return level2;
                    }
                    else if (adultEyeOpeningMonopodSecond >= 33.8)
                    {
                        return level3;
                    }
                }

            return 100;
        }

        /// <summary>
        /// 體適能-平衡(老年開眼單足立)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="elderlyEyeOpeningMonopodSecond">老年開眼單足立秒數</param>
        /// <returns></returns>
        public int PhysicalFitnessElderlyEyeOpeningMonopodSecond(string gender, int age, double elderlyEyeOpeningMonopodSecond)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;

            // 男性百分比
            double middlePercentageMale65_To_69Level1 = (8.2 + 10.3) / 2.0;
            double middlePercentageMale65_To_69Level2 = (20.0 + 23.3) / 2.0;
            //double middlePercentageMale65_To_69Level3 = (-3.0 + 0.0) / 2.0;
            //double middlePercentageMale65_To_69Level4 = (2.0 + 3.0) / 2.0;
            double middlePercentageMale70_To_74Level1 = (5.1 + 6.4) / 2.0;
            double middlePercentageMale70_To_74Level2 = (11.7 + 14.0) / 2.0;
            double middlePercentageMale70_To_74Level3 = (23.6 + 28.0) / 2.0;
            //double middlePercentageMale70_To_74Level4 = (1.0 + 2.0) / 2.0;
            double middlePercentageMale75_To_79Level1 = (3.7 + 4.2) / 2.0;
            double middlePercentageMale75_To_79Level2 = (7.0 + 8.3) / 2.0;
            double middlePercentageMale75_To_79Level3 = (14.0 + 17.0) / 2.0;
            //double middlePercentageMale75_To_79Level4 = (0.0 + 2.0) / 2.0;
            double middlePercentageMale80_To_84Level1 = (2.8 + 3.0) / 2.0;
            double middlePercentageMale80_To_84Level2 = (4.9 + 5.4) / 2.0;
            double middlePercentageMale80_To_84Level3 = (8.3 + 10.2) / 2.0;
            double middlePercentageMale80_To_84Level4 = (18.6 + 22.9) / 2.0;
            double middlePercentageMale85_To_89Level1 = (2.0 + 2.1) / 2.0;
            double middlePercentageMale85_To_89Level2 = (3.1 + 3.7) / 2.0;
            double middlePercentageMale85_To_89Level3 = (5.7 + 7.0) / 2.0;
            double middlePercentageMale85_To_89Level4 = (11.0 + 13.5) / 2.0;
            double middlePercentageMale90OverLevel1 = (-30.0 + -26.5) / 2.0;
            double middlePercentageMale90OverLevel2 = (-21.8 + -20.0) / 2.0;
            double middlePercentageMale90OverLevel3 = (-15.0 + -14.0) / 2.0;
            double middlePercentageMale90OverLevel4 = (-1.6 + 0.9) / 2.0;

            // 女性百分比
            double middlePercentageFemale_65_To_69Level1 = (6.8 + 8.1) / 2.0;
            double middlePercentageFemale_65_To_69Level2 = (14.5 + 17.0) / 2.0;
            double middlePercentageFemale_65_To_69Level3 = (27.0 + 30.0) / 2.0;
            //double middlePercentageFemale_65_To_69Level4 = (4.0 + 5.0) / 2.0;
            double middlePercentageFemale_70_To_74Level1 = (4.3 + 5.0) / 2.0;
            double middlePercentageFemale_70_To_74Level2 = (8.3 + 10.0) / 2.0;
            double middlePercentageFemale_70_To_74Level3 = (16.2 + 19.8) / 2.0;
            //double middlePercentageFemale_70_To_74Level4 = (3.9 + 4.4) / 2.0;
            double middlePercentageFemale_75_To_79Level1 = (2.9 + 3.3) / 2.0;
            double middlePercentageFemale_75_To_79Level2 = (5.1 + 6.0) / 2.0;
            double middlePercentageFemale_75_To_79Level3 = (9.1 + 10.5) / 2.0;
            double middlePercentageFemale_75_To_79Level4 = (19.1 + 25.0) / 2.0;
            double middlePercentageFemale_80_To_84Level1 = (2.0 + 2.3) / 2.0;
            double middlePercentageFemale_80_To_84Level2 = (3.7 + 4.2) / 2.0;
            double middlePercentageFemale_80_To_84Level3 = (6.3 + 7.3) / 2.0;
            double middlePercentageFemale_80_To_84Level4 = (12.0 + 15.9) / 2.0;
            double middlePercentageFemale_85_To_89Level1 = (1.4 + 1.9) / 2.0;
            double middlePercentageFemale_85_To_89Level2 = (2.9 + 3.1) / 2.0;
            double middlePercentageFemale_85_To_89Level3 = (5.0 + 5.5) / 2.0;
            double middlePercentageFemale_85_To_89Level4 = (10.0 + 12.0) / 2.0;
            double middlePercentageFemale90OverLevel1 = (0.9 + 1.0) / 2.0;
            double middlePercentageFemale90OverLevel2 = (1.6 + 2.0) / 2.0;
            double middlePercentageFemale90OverLevel3 = (2.7 + 3.2) / 2.0;
            double middlePercentageFemale90OverLevel4 = (5.3 + 6.0) / 2.0;

            if (gender == "男")
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 8.2)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 10.3 && elderlyEyeOpeningMonopodSecond <= 20.0)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 23.3)
                    {
                        return level3;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 8.2 && elderlyEyeOpeningMonopodSecond < middlePercentageMale65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale65_To_69Level1 && elderlyEyeOpeningMonopodSecond < 10.3)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 20.0 && elderlyEyeOpeningMonopodSecond < middlePercentageMale65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale65_To_69Level2 && elderlyEyeOpeningMonopodSecond < 23.3)
                    {
                        return level3;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 5.1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 6.4 && elderlyEyeOpeningMonopodSecond <= 11.7)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 14.0 && elderlyEyeOpeningMonopodSecond <= 23.6)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 28.0)
                    {
                        return level4;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 5.1 && elderlyEyeOpeningMonopodSecond < middlePercentageMale70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale70_To_74Level1 && elderlyEyeOpeningMonopodSecond < 6.4)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 11.7 && elderlyEyeOpeningMonopodSecond < middlePercentageMale70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale70_To_74Level2 && elderlyEyeOpeningMonopodSecond < 14.0)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 23.6 && elderlyEyeOpeningMonopodSecond < middlePercentageMale70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale70_To_74Level3 && elderlyEyeOpeningMonopodSecond < 28.0)
                    {
                        return level4;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 3.7)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 4.2 && elderlyEyeOpeningMonopodSecond <= 7.0)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 8.3 && elderlyEyeOpeningMonopodSecond <= 14.0)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 17.0)
                    {
                        return level4;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 3.7 && elderlyEyeOpeningMonopodSecond < middlePercentageMale75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale75_To_79Level1 && elderlyEyeOpeningMonopodSecond < 4.2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 7.0 && elderlyEyeOpeningMonopodSecond < middlePercentageMale75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale75_To_79Level2 && elderlyEyeOpeningMonopodSecond < 8.3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 14.0 && elderlyEyeOpeningMonopodSecond < middlePercentageMale75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale75_To_79Level3 && elderlyEyeOpeningMonopodSecond < 17.0)
                    {
                        return level4;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 2.8)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 3.0 && elderlyEyeOpeningMonopodSecond <= 4.9)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 5.4 && elderlyEyeOpeningMonopodSecond <= 8.3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 10.2 && elderlyEyeOpeningMonopodSecond <= 18.6)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 22.9)
                    {
                        return level5;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 2.8 && elderlyEyeOpeningMonopodSecond < middlePercentageMale80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale80_To_84Level1 && elderlyEyeOpeningMonopodSecond < 3.0)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 4.9 && elderlyEyeOpeningMonopodSecond < middlePercentageMale80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale80_To_84Level2 && elderlyEyeOpeningMonopodSecond < 5.4)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 8.3 && elderlyEyeOpeningMonopodSecond < middlePercentageMale80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale80_To_84Level3 && elderlyEyeOpeningMonopodSecond < 10.2)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 18.6 && elderlyEyeOpeningMonopodSecond < middlePercentageMale80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale80_To_84Level4 && elderlyEyeOpeningMonopodSecond < 22.9)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 2.0)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 2.1 && elderlyEyeOpeningMonopodSecond <= 3.1)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 3.7 && elderlyEyeOpeningMonopodSecond <= 5.7)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 7.0 && elderlyEyeOpeningMonopodSecond <= 11.0)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 13.5)
                    {
                        return level5;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 2.0 && elderlyEyeOpeningMonopodSecond < middlePercentageMale85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale85_To_89Level1 && elderlyEyeOpeningMonopodSecond < 2.1)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 3.1 && elderlyEyeOpeningMonopodSecond < middlePercentageMale85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale85_To_89Level2 && elderlyEyeOpeningMonopodSecond < 3.7)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 5.7 && elderlyEyeOpeningMonopodSecond < middlePercentageMale85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale85_To_89Level3 && elderlyEyeOpeningMonopodSecond < 7.0)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 11.0 && elderlyEyeOpeningMonopodSecond < middlePercentageMale85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale85_To_89Level4 && elderlyEyeOpeningMonopodSecond < 13.5)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 1.0)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 1.3 && elderlyEyeOpeningMonopodSecond <= 2.1)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 2.9 && elderlyEyeOpeningMonopodSecond <= 3.8)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 4.8 && elderlyEyeOpeningMonopodSecond <= 9.2)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 11.5)
                    {
                        return level5;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 1.0 && elderlyEyeOpeningMonopodSecond < middlePercentageMale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale90OverLevel1 && elderlyEyeOpeningMonopodSecond < 1.3)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 2.1 && elderlyEyeOpeningMonopodSecond < middlePercentageMale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale90OverLevel2 && elderlyEyeOpeningMonopodSecond < 2.9)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 3.8 && elderlyEyeOpeningMonopodSecond < middlePercentageMale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale90OverLevel3 && elderlyEyeOpeningMonopodSecond < 4.8)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 9.2 && elderlyEyeOpeningMonopodSecond < middlePercentageMale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageMale90OverLevel4 && elderlyEyeOpeningMonopodSecond < 11.5)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 6.8)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 8.1 && elderlyEyeOpeningMonopodSecond <= 14.5)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 17.0 && elderlyEyeOpeningMonopodSecond <= 27.0)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 30.0)
                    {
                        return level4;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 6.8 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_65_To_69Level1 && elderlyEyeOpeningMonopodSecond < 8.1)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 14.5 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_65_To_69Level2 && elderlyEyeOpeningMonopodSecond < 17.0)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 27.0 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_65_To_69Level3 && elderlyEyeOpeningMonopodSecond < 30.0)
                    {
                        return level4;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 4.3)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 5.0 && elderlyEyeOpeningMonopodSecond <= 8.3)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 10.0 && elderlyEyeOpeningMonopodSecond <= 16.2)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 19.8)
                    {
                        return level4;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 4.3 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_70_To_74Level1 && elderlyEyeOpeningMonopodSecond < 5.0)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 8.3 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_70_To_74Level2 && elderlyEyeOpeningMonopodSecond < 10.0)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 16.2 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_70_To_74Level3 && elderlyEyeOpeningMonopodSecond < 19.8)
                    {
                        return level4;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 2.9)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 3.3 && elderlyEyeOpeningMonopodSecond <= 5.1)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 6.0 && elderlyEyeOpeningMonopodSecond <= 9.1)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 10.5 && elderlyEyeOpeningMonopodSecond <= 19.1)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 25.0)
                    {
                        return level5;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 2.9 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_75_To_79Level1 && elderlyEyeOpeningMonopodSecond < 3.3)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 5.1 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_75_To_79Level2 && elderlyEyeOpeningMonopodSecond < 6.0)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 9.1 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_75_To_79Level3 && elderlyEyeOpeningMonopodSecond < 10.5)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 19.1 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_75_To_79Level4 && elderlyEyeOpeningMonopodSecond < 25.0)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 2.0)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 2.3 && elderlyEyeOpeningMonopodSecond <= 3.7)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 4.2 && elderlyEyeOpeningMonopodSecond <= 6.3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 7.3 && elderlyEyeOpeningMonopodSecond <= 12.0)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 15.9)
                    {
                        return level5;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 2.0 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_80_To_84Level1 && elderlyEyeOpeningMonopodSecond < 2.3)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 3.7 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_80_To_84Level2 && elderlyEyeOpeningMonopodSecond < 4.2)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 6.3 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_80_To_84Level3 && elderlyEyeOpeningMonopodSecond < 7.3)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 12.0 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_80_To_84Level4 && elderlyEyeOpeningMonopodSecond < 15.9)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 1.4)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 1.9 && elderlyEyeOpeningMonopodSecond <= 2.9)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 3.1 && elderlyEyeOpeningMonopodSecond <= 5.0)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 5.5 && elderlyEyeOpeningMonopodSecond <= 10.0)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 12.0)
                    {
                        return level5;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 1.4 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_85_To_89Level1 && elderlyEyeOpeningMonopodSecond < 1.9)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 2.9 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_85_To_89Level2 && elderlyEyeOpeningMonopodSecond < 3.1)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 5.0 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_85_To_89Level3 && elderlyEyeOpeningMonopodSecond < 5.5)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 10.0 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale_85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale_85_To_89Level4 && elderlyEyeOpeningMonopodSecond < 12.0)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (elderlyEyeOpeningMonopodSecond <= 0.9)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 1.0 && elderlyEyeOpeningMonopodSecond <= 1.6)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 2.0 && elderlyEyeOpeningMonopodSecond <= 2.7)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 3.2 && elderlyEyeOpeningMonopodSecond <= 5.3)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond >= 6.0)
                    {
                        return level5;
                    }


                    else if (elderlyEyeOpeningMonopodSecond > 0.9 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale90OverLevel1 && elderlyEyeOpeningMonopodSecond < 1.0)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 1.6 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale90OverLevel2 && elderlyEyeOpeningMonopodSecond < 2.0)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 2.7 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale90OverLevel3 && elderlyEyeOpeningMonopodSecond < 3.2)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > 5.3 && elderlyEyeOpeningMonopodSecond < middlePercentageFemale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (elderlyEyeOpeningMonopodSecond > middlePercentageFemale90OverLevel4 && elderlyEyeOpeningMonopodSecond < 6.0)
                    {
                        return level5;
                    }
                }
            }

            return 100;
        }

        /// <summary>
        /// 體適能-平衡(坐立繞物)
        /// </summary>
        /// <param name="gender">性別</param>
        /// <param name="age">年齡</param>
        /// <param name="sittingAroundOneSecond">第一次坐立繞物</param>
        /// <param name="sittingAroundTwoSecond">第二次坐立繞物</param>
        /// <returns></returns>
        public int PhysicalFitnessSittingAroundSecond(string gender, int age, double sittingAroundOneSecond, double sittingAroundTwoSecond)
        {
            int level1 = 20;
            int level2 = 40;
            int level3 = 60;
            int level4 = 80;
            int level5 = 100;

            // 男性百分比
            double middlePercentageMale65_To_69Level1 = (7.6 + 7.2) / 2.0;
            double middlePercentageMale65_To_69Level2 = (6.5 + 6.2) / 2.0;
            double middlePercentageMale65_To_69Level3 = (5.8 + 5.6) / 2.0;
            double middlePercentageMale65_To_69Level4 = (5.0 + 4.9) / 2.0;
            double middlePercentageMale70_To_74Level1 = (8.5 + 8.0) / 2.0;
            double middlePercentageMale70_To_74Level2 = (7.1 + 7.0) / 2.0;
            double middlePercentageMale70_To_74Level3 = (6.3 + 6.1) / 2.0;
            double middlePercentageMale70_To_74Level4 = (5.5 + 5.2) / 2.0;
            double middlePercentageMale75_To_79Level1 = (9.6 + 9.0) / 2.0;
            double middlePercentageMale75_To_79Level2 = (8.0 + 7.7) / 2.0;
            double middlePercentageMale75_To_79Level3 = (7.0 + 6.8) / 2.0;
            double middlePercentageMale75_To_79Level4 = (6.0 + 5.8) / 2.0;
            double middlePercentageMale80_To_84Level1 = (11.0 + 10.4) / 2.0;
            double middlePercentageMale80_To_84Level2 = (9.1 + 8.8) / 2.0;
            double middlePercentageMale80_To_84Level3 = (7.7 + 7.4) / 2.0;
            double middlePercentageMale80_To_84Level4 = (6.9 + 6.2) / 2.0;
            double middlePercentageMale85_To_89Level1 = (12.1 + 11.6) / 2.0;
            double middlePercentageMale85_To_89Level2 = (10.0 + 9.6) / 2.0;
            double middlePercentageMale85_To_89Level3 = (8.6 + 8.2) / 2.0;
            double middlePercentageMale85_To_89Level4 = (7.2 + 6.9) / 2.0;
            double middlePercentageMale90OverLevel1 = (14.8 + 14.0) / 2.0;
            double middlePercentageMale90OverLevel2 = (12.2 + 10.9) / 2.0;
            double middlePercentageMale90OverLevel3 = (9.6 + 9.2) / 2.0;
            double middlePercentageMale90OverLevel4 = (8.5 + 8.2) / 2.0;

            // 女性百分比
            double middlePercentageFemale_65_To_69Level1 = (8.0 + 7.7) / 2.0;
            double middlePercentageFemale_65_To_69Level2 = (7.0 + 6.7) / 2.0;
            double middlePercentageFemale_65_To_69Level3 = (6.1 + 6.0) / 2.0;
            double middlePercentageFemale_65_To_69Level4 = (5.4 + 5.2) / 2.0;
            double middlePercentageFemale_70_To_74Level1 = (9.1 + 8.7) / 2.0;
            double middlePercentageFemale_70_To_74Level2 = (7.7 + 7.4) / 2.0;
            double middlePercentageFemale_70_To_74Level3 = (6.7 + 6.5) / 2.0;
            double middlePercentageFemale_70_To_74Level4 = (5.9 + 5.6) / 2.0;
            double middlePercentageFemale_75_To_79Level1 = (10.3 + 10.0) / 2.0;
            double middlePercentageFemale_75_To_79Level2 = (8.7 + 8.3) / 2.0;
            double middlePercentageFemale_75_To_79Level3 = (7.6 + 7.2) / 2.0;
            double middlePercentageFemale_75_To_79Level4 = (6.5 + 6.1) / 2.0;
            double middlePercentageFemale_80_To_84Level1 = (11.7 + 11.1) / 2.0;
            double middlePercentageFemale_80_To_84Level2 = (9.9 + 9.4) / 2.0;
            double middlePercentageFemale_80_To_84Level3 = (8.5 + 8.1) / 2.0;
            double middlePercentageFemale_80_To_84Level4 = (7.2 + 7.0) / 2.0;
            double middlePercentageFemale_85_To_89Level1 = (13.6 + 12.8) / 2.0;
            double middlePercentageFemale_85_To_89Level2 = (11.0 + 10.5) / 2.0;
            double middlePercentageFemale_85_To_89Level3 = (9.2 + 9.0) / 2.0;
            double middlePercentageFemale_85_To_89Level4 = (8.0 + 7.5) / 2.0;
            double middlePercentageFemale90OverLevel1 = (15.9 + 14.6) / 2.0;
            double middlePercentageFemale90OverLevel2 = (12.9 + 12.0) / 2.0;
            double middlePercentageFemale90OverLevel3 = (10.3 + 9.9) / 2.0;
            double middlePercentageFemale90OverLevel4 = (8.4 + 8.0) / 2.0;

            // 椅子坐姿體前彎，取最大距離
            double[] sittingAroundArray = new double[] { sittingAroundOneSecond, sittingAroundTwoSecond };
            Array.Sort(sittingAroundArray);
            double sittingAround = sittingAroundArray.FirstOrDefault();

            if (gender == "男")
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (sittingAround <= 7.6)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 7.2 && sittingAround <= 6.5)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 6.2 && sittingAround <= 5.8)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 5.6 && sittingAround <= 5.0)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 4.9)
                    {
                        return level5;
                    }


                    else if (sittingAround > 7.6 && sittingAround < middlePercentageMale65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageMale65_To_69Level1 && sittingAround < 7.2)
                    {
                        return level2;
                    }
                    else if (sittingAround > 6.5 && sittingAround < middlePercentageMale65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageMale65_To_69Level2 && sittingAround < 6.2)
                    {
                        return level3;
                    }
                    else if (sittingAround > 5.8 && sittingAround < middlePercentageMale65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageMale65_To_69Level3 && sittingAround < 5.6)
                    {
                        return level4;
                    }
                    else if (sittingAround > 5.0 && sittingAround < middlePercentageMale65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageMale65_To_69Level4 && sittingAround < 4.9)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (sittingAround <= 8.5)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 8.0 && sittingAround <= 7.1)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 7.0 && sittingAround <= 6.3)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 6.1 && sittingAround <= 5.5)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 5.2)
                    {
                        return level5;
                    }


                    else if (sittingAround > 8.5 && sittingAround < middlePercentageMale70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageMale70_To_74Level1 && sittingAround < 8.0)
                    {
                        return level2;
                    }
                    else if (sittingAround > 7.1 && sittingAround < middlePercentageMale70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageMale70_To_74Level2 && sittingAround < 7.0)
                    {
                        return level3;
                    }
                    else if (sittingAround > 6.3 && sittingAround < middlePercentageMale70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageMale70_To_74Level3 && sittingAround < 6.1)
                    {
                        return level4;
                    }
                    else if (sittingAround > 5.5 && sittingAround < middlePercentageMale70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageMale70_To_74Level4 && sittingAround < 5.2)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (sittingAround <= 9.6)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 9.0 && sittingAround <= 8.0)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 7.7 && sittingAround <= 7.0)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 6.8 && sittingAround <= 6.0)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 5.8)
                    {
                        return level5;
                    }


                    else if (sittingAround > 9.6 && sittingAround < middlePercentageMale75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageMale75_To_79Level1 && sittingAround < 9.0)
                    {
                        return level2;
                    }
                    else if (sittingAround > 8.0 && sittingAround < middlePercentageMale75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageMale75_To_79Level2 && sittingAround < 7.7)
                    {
                        return level3;
                    }
                    else if (sittingAround > 7.0 && sittingAround < middlePercentageMale75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageMale75_To_79Level3 && sittingAround < 6.8)
                    {
                        return level4;
                    }
                    else if (sittingAround > 6.0 && sittingAround < middlePercentageMale75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageMale75_To_79Level4 && sittingAround < 5.8)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (sittingAround <= 11.0)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 10.4 && sittingAround <= 9.1)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 8.8 && sittingAround <= 7.7)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 7.4 && sittingAround <= 6.9)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 6.2)
                    {
                        return level5;
                    }


                    else if (sittingAround > 11.0 && sittingAround < middlePercentageMale80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageMale80_To_84Level1 && sittingAround < 10.4)
                    {
                        return level2;
                    }
                    else if (sittingAround > 9.1 && sittingAround < middlePercentageMale80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageMale80_To_84Level2 && sittingAround < 8.8)
                    {
                        return level3;
                    }
                    else if (sittingAround > 7.7 && sittingAround < middlePercentageMale80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageMale80_To_84Level3 && sittingAround < 7.4)
                    {
                        return level4;
                    }
                    else if (sittingAround > 6.9 && sittingAround < middlePercentageMale80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageMale80_To_84Level4 && sittingAround < 6.2)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (sittingAround <= 12.1)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 11.6 && sittingAround <= 10.0)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 9.6 && sittingAround <= 8.6)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 8.2 && sittingAround <= 7.2)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 6.9)
                    {
                        return level5;
                    }


                    else if (sittingAround > 12.1 && sittingAround < middlePercentageMale85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageMale85_To_89Level1 && sittingAround < 11.6)
                    {
                        return level2;
                    }
                    else if (sittingAround > 10.0 && sittingAround < middlePercentageMale85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageMale85_To_89Level2 && sittingAround < 9.6)
                    {
                        return level3;
                    }
                    else if (sittingAround > 8.6 && sittingAround < middlePercentageMale85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageMale85_To_89Level3 && sittingAround < 8.2)
                    {
                        return level4;
                    }
                    else if (sittingAround > 7.2 && sittingAround < middlePercentageMale85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageMale85_To_89Level4 && sittingAround < 6.9)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (sittingAround <= 14.8)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 14.0 && sittingAround <= 12.2)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 10.9 && sittingAround <= 9.6)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 9.2 && sittingAround <= 8.5)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 8.2)
                    {
                        return level5;
                    }


                    else if (sittingAround > 14.8 && sittingAround < middlePercentageMale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageMale90OverLevel1 && sittingAround < 14.0)
                    {
                        return level2;
                    }
                    else if (sittingAround > 12.2 && sittingAround < middlePercentageMale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageMale90OverLevel2 && sittingAround < 10.9)
                    {
                        return level3;
                    }
                    else if (sittingAround > 9.6 && sittingAround < middlePercentageMale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageMale90OverLevel3 && sittingAround < 9.2)
                    {
                        return level4;
                    }
                    else if (sittingAround > 8.5 && sittingAround < middlePercentageMale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageMale90OverLevel4 && sittingAround < 8.2)
                    {
                        return level5;
                    }
                }
            }
            else
            {
                // 65 歲以上到 69 歲
                if (age > 65 && age <= 69)
                {
                    if (sittingAround <= 8.0)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 7.7 && sittingAround <= 7.0)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 6.7 && sittingAround <= 6.1)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 6.0 && sittingAround <= 5.4)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 5.2)
                    {
                        return level5;
                    }


                    else if (sittingAround > 8.0 && sittingAround < middlePercentageFemale_65_To_69Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageFemale_65_To_69Level1 && sittingAround < 7.7)
                    {
                        return level2;
                    }
                    else if (sittingAround > 7.0 && sittingAround < middlePercentageFemale_65_To_69Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageFemale_65_To_69Level2 && sittingAround < 6.7)
                    {
                        return level3;
                    }
                    else if (sittingAround > 6.1 && sittingAround < middlePercentageFemale_65_To_69Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageFemale_65_To_69Level3 && sittingAround < 6.0)
                    {
                        return level4;
                    }
                    else if (sittingAround > 5.4 && sittingAround < middlePercentageFemale_65_To_69Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageFemale_65_To_69Level4 && sittingAround < 5.2)
                    {
                        return level5;
                    }
                }
                // 70 到 74 歲
                else if (age >= 70 && age <= 74)
                {
                    if (sittingAround <= 9.1)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 8.7 && sittingAround <= 7.7)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 7.4 && sittingAround <= 6.7)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 6.5 && sittingAround <= 5.9)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 5.6)
                    {
                        return level5;
                    }


                    else if (sittingAround > 9.1 && sittingAround < middlePercentageFemale_70_To_74Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageFemale_70_To_74Level1 && sittingAround < 8.7)
                    {
                        return level2;
                    }
                    else if (sittingAround > 7.7 && sittingAround < middlePercentageFemale_70_To_74Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageFemale_70_To_74Level2 && sittingAround < 7.4)
                    {
                        return level3;
                    }
                    else if (sittingAround > 6.7 && sittingAround < middlePercentageFemale_70_To_74Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageFemale_70_To_74Level3 && sittingAround < 6.5)
                    {
                        return level4;
                    }
                    else if (sittingAround > 5.9 && sittingAround < middlePercentageFemale_70_To_74Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageFemale_70_To_74Level4 && sittingAround < 5.6)
                    {
                        return level5;
                    }
                }
                // 75 到 79 歲
                else if (age >= 75 && age <= 79)
                {
                    if (sittingAround <= 10.3)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 10.0 && sittingAround <= 8.7)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 8.3 && sittingAround <= 7.6)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 7.2 && sittingAround <= 6.5)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 6.1)
                    {
                        return level5;
                    }


                    else if (sittingAround > 10.3 && sittingAround < middlePercentageFemale_75_To_79Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageFemale_75_To_79Level1 && sittingAround < 10.0)
                    {
                        return level2;
                    }
                    else if (sittingAround > 8.7 && sittingAround < middlePercentageFemale_75_To_79Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageFemale_75_To_79Level2 && sittingAround < 8.3)
                    {
                        return level3;
                    }
                    else if (sittingAround > 7.6 && sittingAround < middlePercentageFemale_75_To_79Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageFemale_75_To_79Level3 && sittingAround < 7.2)
                    {
                        return level4;
                    }
                    else if (sittingAround > 6.5 && sittingAround < middlePercentageFemale_75_To_79Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageFemale_75_To_79Level4 && sittingAround < 6.1)
                    {
                        return level5;
                    }
                }
                // 80 到 84 歲
                else if (age >= 80 && age <= 84)
                {
                    if (sittingAround <= 11.7)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 11.1 && sittingAround <= 9.9)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 9.4 && sittingAround <= 8.5)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 8.1 && sittingAround <= 7.2)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 7.0)
                    {
                        return level5;
                    }


                    else if (sittingAround > 11.7 && sittingAround < middlePercentageFemale_80_To_84Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageFemale_80_To_84Level1 && sittingAround < 11.1)
                    {
                        return level2;
                    }
                    else if (sittingAround > 9.9 && sittingAround < middlePercentageFemale_80_To_84Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageFemale_80_To_84Level2 && sittingAround < 9.4)
                    {
                        return level3;
                    }
                    else if (sittingAround > 8.5 && sittingAround < middlePercentageFemale_80_To_84Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageFemale_80_To_84Level3 && sittingAround < 8.1)
                    {
                        return level4;
                    }
                    else if (sittingAround > 7.2 && sittingAround < middlePercentageFemale_80_To_84Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageFemale_80_To_84Level4 && sittingAround < 7.0)
                    {
                        return level5;
                    }
                }
                // 85 到 89 歲
                else if (age >= 85 && age <= 89)
                {
                    if (sittingAround <= 13.6)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 12.8 && sittingAround <= 11.0)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 10.5 && sittingAround <= 9.2)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 9.0 && sittingAround <= 8.0)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 7.5)
                    {
                        return level5;
                    }


                    else if (sittingAround > 13.6 && sittingAround < middlePercentageFemale_85_To_89Level1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageFemale_85_To_89Level1 && sittingAround < 12.8)
                    {
                        return level2;
                    }
                    else if (sittingAround > 11.0 && sittingAround < middlePercentageFemale_85_To_89Level2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageFemale_85_To_89Level2 && sittingAround < 10.5)
                    {
                        return level3;
                    }
                    else if (sittingAround > 9.2 && sittingAround < middlePercentageFemale_85_To_89Level3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageFemale_85_To_89Level3 && sittingAround < 9.0)
                    {
                        return level4;
                    }
                    else if (sittingAround > 8.0 && sittingAround < middlePercentageFemale_85_To_89Level4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageFemale_85_To_89Level4 && sittingAround < 7.5)
                    {
                        return level5;
                    }
                }
                // 90 歲以上
                else if (age >= 90)
                {
                    if (sittingAround <= 15.9)
                    {
                        return level1;
                    }
                    else if (sittingAround >= 14.6 && sittingAround <= 12.9)
                    {
                        return level2;
                    }
                    else if (sittingAround >= 12.0 && sittingAround <= 10.3)
                    {
                        return level3;
                    }
                    else if (sittingAround >= 9.9 && sittingAround <= 8.4)
                    {
                        return level4;
                    }
                    else if (sittingAround >= 8.0)
                    {
                        return level5;
                    }


                    else if (sittingAround > 15.9 && sittingAround < middlePercentageFemale90OverLevel1)
                    {
                        return level1;
                    }
                    else if (sittingAround > middlePercentageFemale90OverLevel1 && sittingAround < 14.6)
                    {
                        return level2;
                    }
                    else if (sittingAround > 12.9 && sittingAround < middlePercentageFemale90OverLevel2)
                    {
                        return level2;
                    }
                    else if (sittingAround > middlePercentageFemale90OverLevel2 && sittingAround < 12.0)
                    {
                        return level3;
                    }
                    else if (sittingAround > 10.3 && sittingAround < middlePercentageFemale90OverLevel3)
                    {
                        return level3;
                    }
                    else if (sittingAround > middlePercentageFemale90OverLevel3 && sittingAround < 9.9)
                    {
                        return level4;
                    }
                    else if (sittingAround > 8.4 && sittingAround < middlePercentageFemale90OverLevel4)
                    {
                        return level4;
                    }
                    else if (sittingAround > middlePercentageFemale90OverLevel4 && sittingAround < 8.0)
                    {
                        return level5;
                    }
                }
            }

            return 100;
        }
        
        /// <summary>
        /// 體適能分數轉換評語
        /// </summary>
        /// <param name="fraction"></param>
        /// <returns></returns>
        public string FractionConvertComments(int fraction, string name)
        {
            if (name == "YoungManCardiorespiratoryFitness" || name == "YoungManMuscleStrengthAndMuscleEndurance" || name == "YoungManSoftness" ||
                name == "ElderlyCardiorespiratoryFitness" || name == "ElderlyMuscleStrengthAndMuscleEndurance" || name == "ElderlySoftness" || 
                name == "ElderlyBalance")
            {
                if (fraction == 20)
                {
                    return "異常";
                }
                else if (fraction == 40)
                {
                    return "稍差";
                }
                else if (fraction == 60)
                {
                    return "標準";
                }
                else if (fraction == 80)
                {
                    return "尚好";
                }
                else
                {
                    return "極佳";
                }
            }
            else if (name == "YoungManBalance")
            {
                if (fraction == 33)
                {
                    return "稍差";
                }
                else if (fraction == 66)
                {
                    return "普通";
                }
                else
                {
                    return "尚好";
                }
            }

            return "未測試";
        }
    }

    [Flags]
    public enum NormEnum : int
    {
        /// <summary>
        /// 壯年心肺適能
        /// </summary>
        YoungManCardiorespiratoryFitness = 5,

        /// <summary>
        /// 壯年肌力與肌耐力
        /// </summary>
        YoungManMuscleStrengthAndMuscleEndurance = 5,

        /// <summary>
        /// 壯年柔軟度
        /// </summary>
        YoungManSoftness = 5,

        /// <summary>
        /// 壯年平衡
        /// </summary>
        YoungManBalance = 3,

        /// <summary>
        /// 老年心肺適能
        /// </summary>
        ElderlyCardiorespiratoryFitness = 5,

        /// <summary>
        /// 老年柔軟度
        /// </summary>
        ElderlyMuscleStrengthAndMuscleEndurance = 5,

        /// <summary>
        /// 老年柔軟度
        /// </summary>
        ElderlySoftness = 5,

        /// <summary>
        /// 老年平衡
        /// </summary>
        ElderlyBalance = 5
    }
}
