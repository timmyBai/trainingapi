using Dapper;
using MySql.Data.MySqlClient;
using System.Security.Cryptography.X509Certificates;

namespace TrainingApi.Models.PhysicalFitness
{
    public class PhysicalFitnessModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public PhysicalFitnessModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 取得最新 體適能-心肺適能 資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userAuthorizedId">使用者 id</param>
        /// <returns></returns>
        public T GetFirstPhysicalFitnessCardiorespiratoryFitness<T>(int userAuthorizedId)
        {
            sqlStr = @"SELECT physical_fitness_cardiorespiratory_fitness.userAuthorizedId,
                              (180 * 100) / ((physical_fitness_cardiorespiratory_fitness.pulseRateOne + physical_fitness_cardiorespiratory_fitness.pulseRateTwo + physical_fitness_cardiorespiratory_fitness.pulseRateThree) * 2) AS setUpTest,
                              physical_fitness_cardiorespiratory_fitness.kneeLiftFrequency
                         FROM user_authorized
                         LEFT JOIN (physical_fitness_cardiorespiratory_fitness)
                           ON (user_authorized.userAuthorizedId = physical_fitness_cardiorespiratory_fitness.userAuthorizedId)
                        WHERE     user_authorized.userAuthorizedId                            = @userAuthorizedId
                              AND physical_fitness_cardiorespiratory_fitness.userAuthorizedId = @userAuthorizedId
                        ORDER BY user_authorized.userAuthorizedId ASC,
                                 physical_fitness_cardiorespiratory_fitness.updateRecordDate DESC,
                                 physical_fitness_cardiorespiratory_fitness.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = userAuthorizedId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得最新 體適能-肌力與肌耐力 資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userAuthorizedId"></param>
        /// <returns></returns>
        public T GetFirstPhysicalFitnessMuscleStrengthAndMuscleEndurance<T>(int userAuthorizedId)
        {
            sqlStr = @"SELECT physical_fitness_muscle_strength_and_muscle_endurance.userAuthorizedId,
                              physical_fitness_muscle_strength_and_muscle_endurance.kneeCrunchesFrequency,
                              physical_fitness_muscle_strength_and_muscle_endurance.armCurlUpperBodyFrequency,
                              physical_fitness_muscle_strength_and_muscle_endurance.armCurlLowerBodyFrequency
                         FROM user_authorized
                         LEFT JOIN (physical_fitness_muscle_strength_and_muscle_endurance)
                           ON (user_authorized.userAuthorizedId = physical_fitness_muscle_strength_and_muscle_endurance.userAuthorizedId)
                        WHERE     user_authorized.userAuthorizedId                                       = @userAuthorizedId
                              AND physical_fitness_muscle_strength_and_muscle_endurance.userAuthorizedId = @userAuthorizedId
                        ORDER BY user_authorized.userAuthorizedId ASC,
                                 physical_fitness_muscle_strength_and_muscle_endurance.updateRecordDate DESC,
                                 physical_fitness_muscle_strength_and_muscle_endurance.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = userAuthorizedId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得最新 體適能-柔軟度 資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userAuthorizedId"></param>
        /// <returns></returns>
        public T GetFirstPhysicalFitnessSoftness<T>(int userAuthorizedId)
        {
            sqlStr = @"SELECT physical_fitness_softness.userAuthorizedId,
                              physical_fitness_softness.seatedForwardBendOne,
                              physical_fitness_softness.seatedForwardBendTwo,
                              physical_fitness_softness.seatedForwardBendThree,
                              physical_fitness_softness.chairSeatedForwardBendOne,
                              physical_fitness_softness.chairSeatedForwardBendTwo,
                              physical_fitness_softness.chairSeatedForwardBendThree,
                              physical_fitness_softness.pullBackTestOne,
                              physical_fitness_softness.pullBackTestTwo
                         FROM user_authorized
                         LEFT JOIN (physical_fitness_softness)
                           ON (user_authorized.userAuthorizedId = physical_fitness_softness.userAuthorizedId)
                        WHERE     user_authorized.userAuthorizedId           = @userAuthorizedId
                              AND physical_fitness_softness.userAuthorizedId = @userAuthorizedId
                        ORDER BY user_authorized.userAuthorizedId ASC,
                                 physical_fitness_softness.updateRecordDate DESC,
                                 physical_fitness_softness.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = userAuthorizedId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得最新 體適能-平衡 資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userAuthorizedId"></param>
        /// <returns></returns>
        public T GetFirstPhysicalFitnessBalance<T>(int userAuthorizedId)
        {
            sqlStr = @"SELECT physical_fitness_balance.userAuthorizedId,
                              physical_fitness_balance.adultEyeOpeningMonopodSecond,
                              physical_fitness_balance.elderlyEyeOpeningMonopodSecond,
                              physical_fitness_balance.sittingAroundOneSecond,
                              physical_fitness_balance.sittingAroundTwoSecond
                         FROM user_authorized
                         LEFT JOIN (physical_fitness_balance)
                           ON (user_authorized.userAuthorizedId = physical_fitness_balance.userAuthorizedId)
                        WHERE     user_authorized.userAuthorizedId          = @userAuthorizedId
                              AND physical_fitness_balance.userAuthorizedId = @userAuthorizedId
                        ORDER BY user_authorized.userAuthorizedId ASC,
                                 physical_fitness_balance.updateRecordDate DESC,
                                 physical_fitness_balance.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = userAuthorizedId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }
    }
}
