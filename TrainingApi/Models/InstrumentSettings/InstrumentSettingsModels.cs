using Dapper;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using TrainingApi.Utils;
using static Mysqlx.Notice.Warning.Types;

namespace TrainingApi.Models.InstrumentSettings
{
    public class InstrumentSettingsModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public InstrumentSettingsModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查學員是否有曾經使用過此機器
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <param name="machinaryCode"></param>
        /// <returns></returns>
        public T InspectInstrumentSettingsExistMachinary<T>(int studentId, int machinaryCode)
        {
            sqlStr = $@"SELECT machinaryCode,
                               level,
                               maxRange,
                               speed
                          FROM instrument_settings
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND machinaryCode    = @machinaryCode
                               AND consciousEffort IS NULL
                         ORDER BY trainingDate DESC,
                                  trainingTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = machinaryCode
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得目標次數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public T GetExercisePrescriptionMuscleStrengthAndMuscleEnduranceTargetNumber<T>(int studentId)
        {
            sqlStr = @"SELECT trainingUnitNumber
                         FROM exercise_prescription_muscle_strength_and_muscle_endurance
                        WHERE userAuthorizedId = @userAuthorizedId
                        ORDER BY updateRecordDate DESC,
                                 updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 查詢今天是否有最大運動表現
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId">學員 id</param>
        /// <param name="today">今天日期</param>
        /// <returns></returns>
        public T GetTodayInstrumentSettingsSportsPerformance<T>(int studentId, string today, RequestInsertInstrumentSettingsSportsPerformanceModels obj)
        {
            sqlStr = $@"SELECT instrument_settings.userAuthorizedId,
                               instrument_settings.machinaryCode,
                               instrument_settings.level,
                               instrument_settings.finish,
                               instrument_settings.mistake,
                               instrument_settings.maxRange,
                               instrument_settings.power,
                               instrument_settings.speed,
                               instrument_settings.trainingDate,
                               instrument_settings.trainingTime
                          FROM user_authorized
                          LEFT JOIN (instrument_settings) ON (user_authorized.userAuthorizedId = instrument_settings.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId     = @userAuthorizedId
                               AND instrument_settings.userAuthorizedId = @userAuthorizedId
                               AND instrument_settings.trainingDate     = @trainingDate
                               AND instrument_settings.machinaryCode    = @machinaryCode
                               AND instrument_settings.consciousEffort IS NULL
            ";

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = obj.machinaryCode,
                trainingDate = today
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增儀器設定最大運動表現
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int InsertInstrumentSettingsSportsPerformance(int studentId, RequestInsertInstrumentSettingsSportsPerformanceModels obj)
        {
            sqlStr = $@"INSERT INTO instrument_settings
                        ( userAuthorizedId,  machinaryCode,  level,  finish,  mistake,  maxRange,  power,  speed,  trainingDate,  trainingTime) VALUES
                        (@userAuthorizedId, @machinaryCode, @level, @finish, @mistake, @maxRange, @power, @speed, @trainingDate, @trainingTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = obj.machinaryCode,
                level = obj.level,
                finish = obj.finish,
                mistake = obj.mistake,
                maxRange = obj.maxRange,
                power = obj.power,
                speed = JsonConvert.SerializeObject(obj.speed),
                trainingDate = nowDate,
                trainingTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }

        /// <summary>
        /// 更新儀器設定最大運動表現
        /// </summary>
        /// <returns></returns>
        public int UpdateInstrumentSettingsSportsPerformance(int studentId, string today, RequestInsertInstrumentSettingsSportsPerformanceModels obj)
        {
            sqlStr = $@"UPDATE instrument_settings
                           SET machinaryCode    = @machinaryCode,
                               level            = @level,
                               finish           = @finish,
                               mistake          = @mistake,
                               maxRange         = @maxRange,
                               power            = @power,
                               speed            = @speed,
                               trainingTime     = @trainingTime
                         WHERE     userAuthorizedId = @userAuthorizedId
                               AND machinaryCode    = @machinaryCode
                               AND maxSpeed IS NULL
                               AND consciousEffort IS NULL
                               AND trainingDate     = @trainingDate
            ";

            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                level = obj.level,
                finish = obj.finish,
                mistake = obj.mistake,
                maxRange = obj.maxRange,
                power = obj.power,
                speed = JsonConvert.SerializeObject(obj.speed),
                machinaryCode = obj.machinaryCode,
                trainingDate = today,
                trainingTime = nowTime
            };

            var updateRow = conn.Execute(sqlStr, param);

            return updateRow;
        }

        /// <summary>
        /// 新增儀器設定正式訓練
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int InsertInstrumentSettingsFormalTraining(int studentId, RequestInsertInstrumentSettingFormalTrainingModels obj)
        {
            sqlStr = $@"INSERT INTO instrument_settings
                        ( userAuthorizedId,  machinaryCode,  level,  finish,  mistake,  power,  speed,  maxSpeed,  consciousEffort,  trainingDate,  trainingTime) VALUES
                        (@userAuthorizedId, @machinaryCode, @level, @finish, @mistake, @power, @speed, @maxSpeed, @consciousEffort, @trainingDate, @trainingTime)
            ";

            string nowDate = new DayUtils().GetFullDate();
            string nowTime = new DayUtils().GetFullTime();

            var param = new
            {
                userAuthorizedId = studentId,
                machinaryCode = obj.machinaryCode,
                level = obj.level,
                finish = obj.finish,
                mistake = obj.mistake,
                power = obj.power,
                speed = JsonConvert.SerializeObject(obj.speed),
                maxSpeed = obj.maxSpeed,
                consciousEffort = obj.consciousEffort,
                trainingDate = nowDate,
                trainingTime = nowTime
            };

            var insertRow = conn.Execute(sqlStr, param);

            return insertRow;
        }
    }

    /// <summary>
    /// 接收 新增儀器設定 最大運動表現
    /// </summary>
    public class RequestInsertInstrumentSettingsSportsPerformanceModels
    {
        /// <summary>
        /// 機器編號
        /// </summary>
        [Required]
        [Range(1, 10)]
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 阻力等級
        /// </summary>
        [Required]
        [Range(1, 10)]
        public int level { get; set; } = 0;

        /// <summary>
        /// 完成次數
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]*$")]
        public int finish { get; set; } = 0;

        /// <summary>
        /// 失誤次數
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]*$")]
        public int mistake { get; set; } = 0;

        /// <summary>
        /// 活動範圍 (最大運動表現)
        /// </summary>
        [Range(0, 100)]
        public int maxRange { get; set; } = 0;

        /// <summary>
        /// 功率
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]*$")]
        public int power { get; set; } = 0;

        /// <summary>
        /// 速度陣列
        /// </summary>
        [Required]
        public List<int> speed { get; set; } = new List<int>();
    }

    /// <summary>
    /// 新增儀器設定 正式訓練
    /// </summary>
    public class RequestInsertInstrumentSettingFormalTrainingModels
    {
        /// <summary>
        /// 機器編號
        /// </summary>
        [Required]
        [Range(1, 10)]
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 阻力等級
        /// </summary>
        [Required]
        [Range(1, 10)]
        public int level { get; set; } = 0;

        /// <summary>
        /// 完成次數
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]*$")]
        public int finish { get; set; } = 0;

        /// <summary>
        /// 失誤次數
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]*$")]
        public int mistake { get; set; } = 0;

        /// <summary>
        /// 功率
        /// </summary>
        [Required]
        [RegularExpression("^[0-9]*$")]
        public int power { get; set; } = 0;

        /// <summary>
        /// 速度陣列
        /// </summary>
        [Required]
        public List<int> speed { get; set; } = new List<int>();

        /// <summary>
        /// 最大速度
        /// </summary>
        [Required]
        [RegularExpression("^([1-9]+)([0-9]+)*$")]
        public int maxSpeed { get; set; } = 0;

        /// <summary>
        /// 自覺努力量表 (正式訓練)
        /// </summary>
        [Range(1, 10)]
        public int consciousEffort { get; set; } = 0;
    }
}
