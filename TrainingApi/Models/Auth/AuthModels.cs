using MySql.Data.MySqlClient;
using Dapper;
using System.ComponentModel.DataAnnotations;

using TrainingApi.Utils;

namespace TrainingApi.Models.Auth
{
    public class AuthModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public AuthModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查使用者註冊帳號資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">接收 帳密登入模型</param>
        /// <returns></returns>
        public T InspactUserAuthSecret<T>(RequestAuthLoginModels obj)
        {
            sqlStr = @"SELECT user_authorized.userAuthorizedId,
                              user_authorized.account,
                              user_authorized.name,
                              user_authorized.phone,
                              user_authorized.email,
                              user_authorized.gender,
                              user_authorized.address,
                              user_authorized.birthday,
                              user_authorized.status,
                              user_authorized.identity,
                              user_authorized.fileextension,
                              YEAR(DATE_SUB(NOW(), INTERVAL 0 YEAR)) - SubString(birthday, 1, 4) AS age,
                              body_mass_record.height,
                              body_mass_record.weight
                         FROM user_authorized
                         LEFT JOIN body_mass_record
                           ON body_mass_record.userAuthorizedId = user_authorized.userAuthorizedId
                        WHERE     account  = @account
                              AND password = @password
                              AND identity = 'E'
                        ORDER BY body_mass_record.newRecordDate DESC,
                                 body_mass_record.newRecordTime DESC LIMIT 1
            ";

            string account = obj.account;
            string password = new EncryptionUtils().SHA256Encryption(obj.password);

            var param = new
            {
                account = account,
                password = password
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 檢查使用者註冊手機資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">接收 手機登入模型</param>
        /// <returns></returns>
        public T InspactUserAuthPhoneSecret<T>(RequestAuthPhoneLoginModels obj)
        {
            sqlStr = @"SELECT user_authorized.userAuthorizedId,
                              user_authorized.account,
                              user_authorized.name,
                              user_authorized.phone,
                              user_authorized.email,
                              user_authorized.gender,
                              user_authorized.address,
                              user_authorized.birthday,
                              user_authorized.status,
                              user_authorized.identity,
                              user_authorized.fileextension,
                              YEAR(DATE_SUB(NOW(), INTERVAL 0 YEAR)) - user_authorized.birthday AS age,
                              body_mass_record.height,
                              body_mass_record.weight
                         FROM user_authorized
                         LEFT JOIN body_mass_record
                           ON body_mass_record.userAuthorizedId = user_authorized.userAuthorizedId
                        WHERE     phone    = @phone
                              AND identity = 'E'
                        ORDER BY body_mass_record.newRecordDate DESC,
                                 body_mass_record.newRecordTime DESC LIMIT 1
            ";

            

            var param = new
            {
                phone = obj.phone
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }
    }

    /// <summary>
    /// 接收 會員登驗證模型
    /// </summary>
    public class RequestAuthLoginModels
    {
        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        public string account { get; set; } = "";

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        public string password { get; set; } = "";
    }

    /// <summary>
    /// 接收 會員登驗證模型
    /// </summary>
    public class RequestAuthPhoneLoginModels
    {
        [Required]
        [RegularExpression("[0-9]{8,10}")]
        public string phone { get; set; } = "";
    }
}
