using Dapper;
using MySql.Data.MySqlClient;

namespace TrainingApi.Models.ExercisePrescription
{
    public class ExercisePrescriptionModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        private string ageSql = "SET @age = (SELECT YEAR(DATE_SUB(NOW(), INTERVAL 0 YEAR)) - SubString(user_authorized.birthday, 1, 4) AS age FROM user_authorized WHERE userAuthorizedId = @userAuthorizedId);";
        private string lowheartRateSql = "SET @lowheartRate = round(206.9 - (0.67 * @age) * 0.55);";
        private string middleMinHeartRate = "SET @middleMinHeartRate = round(206.9 - (0.67 * @age) * 0.55);";
        private string middleMaxHeartRate = "SET @middleMaxHeartRate = round(206.9 - (0.67 * @age) * 0.65);";
        private string highMinHeartRate = "SET @highMinHeartRate   = round(206.9 - (0.67 * @age) * 0.65);";
        private string highMaxHeartRate = "SET @highMaxHeartRate   = round(206.9 - (0.67 * @age) * 0.9);";

        public ExercisePrescriptionModels(MySqlConnection _conn)
        {
            conn = _conn;
        }
        
        /// <summary>
        /// 取得最新運動處方-心肺適能
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionCardiorespiratoryFitness<T>(int studentId)
        {
            sqlStr += ageSql;
            sqlStr += lowheartRateSql;
            sqlStr += middleMinHeartRate;
            sqlStr += middleMaxHeartRate;
            sqlStr += highMinHeartRate;
            sqlStr += highMaxHeartRate;
            sqlStr += $@"SELECT user_authorized.userAuthorizedId,
                                CASE exercise_prescription_cardiorespiratory_fitness.exerciseIntensity
                                     WHEN '1' THEN '低強度運動'
	                                 WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exercise_prescription_cardiorespiratory_fitness.exerciseIntensity
                                     WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
	                                 WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
                                 END AS heartRate,
                                CONCAT('每周 ', exercise_prescription_cardiorespiratory_fitness.exerciseFrequency, ' 天，', exercise_prescription_cardiorespiratory_fitness.trainingUnitMinute, ' 分鐘/次') AS trainingFrequency,
                                CONCAT(SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordDate, 1, 4), '-', SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordDate, 5, 2), '-', SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordDate, 7, 2)) AS updateRecordDate,
                                CONCAT(SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordTime, 1, 2), ':', SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordTime, 3, 2), ':', SUBSTRING(exercise_prescription_cardiorespiratory_fitness.updateRecordTime, 5, 2)) AS updateRecordTime
                           FROM user_authorized
                           LEFT JOIN (exercise_prescription_cardiorespiratory_fitness)
                             ON (user_authorized.userAuthorizedId = exercise_prescription_cardiorespiratory_fitness.userAuthorizedId)
                          WHERE     user_authorized.userAuthorizedId                                 = @userAuthorizedId
                                AND exercise_prescription_cardiorespiratory_fitness.userAuthorizedId = @userAuthorizedId
                          ORDER BY exercise_prescription_cardiorespiratory_fitness.updateRecordDate DESC,
                                   exercise_prescription_cardiorespiratory_fitness.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得最新運動處方-肌力與肌耐力
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionMuscleStrengthAndMuscleEndurance<T>(int studentId)
        {
            sqlStr += ageSql;
            sqlStr += lowheartRateSql;
            sqlStr += middleMinHeartRate;
            sqlStr += middleMaxHeartRate;
            sqlStr += highMinHeartRate;
            sqlStr += highMaxHeartRate;
            sqlStr += $@"SELECT user_authorized.userAuthorizedId,
                                CASE exercise_prescription_muscle_strength_and_muscle_endurance.exerciseIntensity
                                     WHEN '1' THEN '低強度運動'
	                                 WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exercise_prescription_muscle_strength_and_muscle_endurance.exerciseIntensity
                                     WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
	                                 WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
                                 END AS heartRate,
                                CONCAT('每周 ', exercise_prescription_muscle_strength_and_muscle_endurance.exerciseFrequency, ' 天， 每天 ', exercise_prescription_muscle_strength_and_muscle_endurance.trainingUnitGroup, ' 組， ', exercise_prescription_muscle_strength_and_muscle_endurance.trainingUnitNumber, ' 次/組') AS trainingFrequency,
                                CONCAT(SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate, 1, 4), '-', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate, 5, 2), '-', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate, 7, 2)) AS updateRecordDate,
                                CONCAT(SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime, 1, 2), ':', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime, 3, 2), ':', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime, 5, 2)) AS updateRecordTime
                           FROM user_authorized
                           LEFT JOIN (exercise_prescription_muscle_strength_and_muscle_endurance)
                             ON (user_authorized.userAuthorizedId = exercise_prescription_muscle_strength_and_muscle_endurance.userAuthorizedId)
                          WHERE     user_authorized.userAuthorizedId                                            = @userAuthorizedId
                                AND exercise_prescription_muscle_strength_and_muscle_endurance.userAuthorizedId = @userAuthorizedId
                          ORDER BY exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate DESC,
                                   exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得最新運動處方-柔軟度
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionSoftness<T>(int studentId)
        {
            sqlStr += ageSql;
            sqlStr += lowheartRateSql;
            sqlStr += middleMinHeartRate;
            sqlStr += middleMaxHeartRate;
            sqlStr += highMinHeartRate;
            sqlStr += highMaxHeartRate;
            sqlStr += $@"SELECT user_authorized.userAuthorizedId,
                                CASE exercise_prescription_softness.exerciseIntensity
                                     WHEN '1' THEN '低強度運動'
	                                 WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exercise_prescription_softness.exerciseIntensity
                                     WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
	                                 WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
                                 END AS heartRate,
                                CONCAT('每周 ', exercise_prescription_softness.exerciseFrequency, ' 天， ', exercise_prescription_softness.trainingUnitMinute, ' 次/組') AS trainingFrequency,
                                CONCAT(SUBSTRING(exercise_prescription_softness.updateRecordDate, 1, 4), '-', SUBSTRING(exercise_prescription_softness.updateRecordDate, 5, 2), '-', SUBSTRING(exercise_prescription_softness.updateRecordDate, 7, 2)) AS updateRecordDate,
                                CONCAT(SUBSTRING(exercise_prescription_softness.updateRecordTime, 1, 2), ':', SUBSTRING(exercise_prescription_softness.updateRecordTime, 3, 2), ':', SUBSTRING(exercise_prescription_softness.updateRecordTime, 5, 2)) AS updateRecordTime
                           FROM user_authorized
                           LEFT JOIN (exercise_prescription_softness)
                             ON (user_authorized.userAuthorizedId = exercise_prescription_softness.userAuthorizedId)
                          WHERE     user_authorized.userAuthorizedId                = @userAuthorizedId
                                AND exercise_prescription_softness.userAuthorizedId = @userAuthorizedId
                          ORDER BY exercise_prescription_softness.updateRecordDate DESC,
                                   exercise_prescription_softness.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得最新運動處方-平衡
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionBalance<T>(int studentId)
        {
            sqlStr += ageSql;
            sqlStr += lowheartRateSql;
            sqlStr += middleMinHeartRate;
            sqlStr += middleMaxHeartRate;
            sqlStr += highMinHeartRate;
            sqlStr += highMaxHeartRate;
            sqlStr += $@"SELECT user_authorized.userAuthorizedId,
                                CASE exercise_prescription_balance.exerciseIntensity
                                     WHEN '1' THEN '低強度運動'
	                                 WHEN '2' THEN '中強度運動'
                                     WHEN '3' THEN '高強度運動'
                                 END AS exerciseIntensity,
                                CASE exercise_prescription_balance.exerciseIntensity
                                     WHEN '1' THEN CONCAT('建議心率區間 ', @lowheartRate)
	                                 WHEN '2' THEN CONCAT('建議心率區間 ', @middleMinHeartRate, '-', @middleMaxHeartRate)
	                                 WHEN '3' THEN CONCAT('建議心率區間 ', @highMinHeartRate, '-', @highMaxHeartRate)
                                 END AS heartRate,
                                CONCAT('每周 ', exercise_prescription_balance.exerciseFrequency, ' 天， ', exercise_prescription_balance.trainingUnitMinute, ' 次/組') AS trainingFrequency,
                                CONCAT(SUBSTRING(exercise_prescription_balance.updateRecordDate, 1, 4), '-', SUBSTRING(exercise_prescription_balance.updateRecordDate, 5, 2), '-', SUBSTRING(exercise_prescription_balance.updateRecordDate, 7, 2)) AS updateRecordDate,
                                CONCAT(SUBSTRING(exercise_prescription_balance.updateRecordTime, 1, 2), ':', SUBSTRING(exercise_prescription_balance.updateRecordTime, 3, 2), ':', SUBSTRING(exercise_prescription_balance.updateRecordTime, 5, 2)) AS updateRecordTime
                           FROM user_authorized
                           LEFT JOIN (exercise_prescription_balance)
                             ON (user_authorized.userAuthorizedId = exercise_prescription_balance.userAuthorizedId)
                          WHERE     user_authorized.userAuthorizedId               = @userAuthorizedId
                                AND exercise_prescription_balance.userAuthorizedId = @userAuthorizedId
                          ORDER BY exercise_prescription_balance.updateRecordDate DESC,
                                   exercise_prescription_balance.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public T GetLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea<T>(int studentId)
        {
            sqlStr = $@"SELECT user_authorized.userAuthorizedId,
                               exercise_prescription_muscle_strength_and_muscle_endurance.trainingArea,
                               CONCAT(SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate, 1, 4), '-', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate, 5, 2), '-', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate, 7, 2)) AS updateRecordDate,
                               CONCAT(SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime, 1, 2), ':', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime, 3, 2), ':', SUBSTRING(exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime, 5, 2)) AS updateRecordTime
                          FROM user_authorized
                          LEFT JOIN (exercise_prescription_muscle_strength_and_muscle_endurance)
                            ON (user_authorized.userAuthorizedId = exercise_prescription_muscle_strength_and_muscle_endurance.userAuthorizedId)
                         WHERE     user_authorized.userAuthorizedId                                            = @userAuthorizedId
                               AND exercise_prescription_muscle_strength_and_muscle_endurance.userAuthorizedId = @userAuthorizedId
                         ORDER BY exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordDate DESC,
                                  exercise_prescription_muscle_strength_and_muscle_endurance.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }
    }
}
