using Dapper;
using MySql.Data.MySqlClient;
using System.ComponentModel.DataAnnotations;

namespace TrainingApi.Models.BodyMass
{
    public class BodyCompositionModels
    {
        private MySqlConnection conn;
        private string sqlStr = "";

        public BodyCompositionModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 取得使用者最新 bmi 紀錄
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userAuthorizedId">使用者 id</param>
        /// <returns></returns>
        public T GetLatestBodyCompositionBmi<T>(int userAuthorizedId)
        {
            sqlStr = @"SELECT user_authorized.userAuthorizedId,
                              body_composition_bmi.bmi
                         FROM user_authorized
                         LEFT JOIN (body_composition_bmi)
                           ON (user_authorized.userAuthorizedId = body_composition_bmi.userAuthorizedId)
                        WHERE     user_authorized.userAuthorizedId      = @userAuthorizedId
                              AND body_composition_bmi.userAuthorizedId = @userAuthorizedId
                        ORDER BY user_authorized.userAuthorizedId ASC,
                                 body_composition_bmi.updateRecordDate DESC,
                                 body_composition_bmi.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = userAuthorizedId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得使用者最新 體脂肪 紀錄
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userAuthorizedId">使用者 id</param>
        /// <returns></returns>
        public T GetLatestBodyCompositionBodyFat<T>(int studentId)
        {
            sqlStr = @"SELECT user_authorized.userAuthorizedId,
                              body_composition_bodyFat.bodyFat
                         FROM user_authorized
                         LEFT JOIN (body_composition_bodyFat)
                           ON (user_authorized.userAuthorizedId = body_composition_bodyFat.userAuthorizedId)
                        WHERE     user_authorized.userAuthorizedId          = @userAuthorizedId
                              AND body_composition_bodyFat.userAuthorizedId = @userAuthorizedId
                        ORDER BY user_authorized.userAuthorizedId ASC,
                                 body_composition_bodyFat.updateRecordDate DESC,
                                 body_composition_bodyFat.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得使用者最新 內臟脂肪 紀錄
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userAuthorizedId">使用者 id</param>
        /// <returns></returns>
        public T GetLatestBodyCompositionVisceralFat<T>(int studentId)
        {
            sqlStr = @"SELECT user_authorized.userAuthorizedId,
                              body_composition_visceralFat.visceralFat
                         FROM user_authorized
                         LEFT JOIN (body_composition_visceralFat)
                           ON (user_authorized.userAuthorizedId = body_composition_visceralFat.userAuthorizedId)
                        WHERE     user_authorized.userAuthorizedId              = @userAuthorizedId
                              AND body_composition_visceralFat.userAuthorizedId = @userAuthorizedId
                        ORDER BY user_authorized.userAuthorizedId ASC,
                                 body_composition_visceralFat.updateRecordDate DESC,
                                 body_composition_visceralFat.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 取得使用者最新 骨骼肌率 紀錄
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="userAuthorizedId">使用者 id</param>
        /// <returns></returns>
        public T GetLatestBodyCompositionSkeletalMuscleRate<T>(int studentId)
        {
            sqlStr = @"SELECT user_authorized.userAuthorizedId,
                              body_composition_skeletalMuscleRate.skeletalMuscleRate
                         FROM user_authorized
                         LEFT JOIN (body_composition_skeletalMuscleRate)
                           ON (user_authorized.userAuthorizedId = body_composition_skeletalMuscleRate.userAuthorizedId)
                        WHERE     user_authorized.userAuthorizedId                     = @userAuthorizedId
                              AND body_composition_skeletalMuscleRate.userAuthorizedId = @userAuthorizedId
                        ORDER BY user_authorized.userAuthorizedId ASC,
                                 body_composition_skeletalMuscleRate.updateRecordDate DESC,
                                 body_composition_skeletalMuscleRate.updateRecordTime DESC LIMIT 1
            ";

            var param = new
            {
                userAuthorizedId = studentId
            };

            var result = conn.Query<T>(sqlStr, param).SingleOrDefault();

            return result;
        }
    }
}

/// <summary>
/// 接收 身體質量 bmi 模型
/// </summary>
public class BodyMassBmiModels
{
    /// <summary>
    /// bmi 指數
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double bmi { get; set; } = 0.0;
}

/// <summary>
/// 接收 身體質量 體脂肪 模型
/// </summary>
public class BodyMassBodyFatModels
{
    /// <summary>
    /// 體脂肪
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double bodyFat { get; set; } = 0.0;
}

/// <summary>
/// 接收 身體質量 內臟脂肪 模型
/// </summary>
public class BodyMassVisceralFatModels
{
    /// <summary>
    /// 內臟脂肪
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double visceralFat { get; set; } = 0.0;
}

/// <summary>
/// 接收 身體質量 骨骼心肌率 模型
/// </summary>
public class BodyMassSkeletalMuscleRateModels
{
    /// <summary>
    /// 骨骼心肌率
    /// </summary>
    [Required]
    [RegularExpression("^([0-9]+.[0-9]+|[0-9]+)")]
    public double skeletalMuscleRate { get; set; } = 0.0;
}
