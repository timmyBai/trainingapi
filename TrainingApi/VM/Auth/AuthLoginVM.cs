namespace TrainingApi.VM.Auth
{
    /// <summary>
    /// 檢查是否有此使用者模型
    /// </summary>
    public class InspactAuthUserVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 帳號
        /// </summary>
        public string account { get; set; } = "";

        /// <summary>
        /// 會員名子
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 電話號碼
        /// </summary>
        public string phone { get; set; } = "";

        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; } = "";

        /// <summary>
        /// 生日
        /// </summary>
        public int birthday { get; set; } = 0;

        /// <summary>
        /// 信箱
        /// </summary>
        public string email { get; set; } = "";

        /// <summary>
        /// 地址
        /// </summary>
        public string address { get; set; } = "";

        /// <summary>
        /// 年齡
        /// </summary>
        public int age { get; set; } = 0;

        /// <summary>
        /// 身高
        /// </summary>
        public int height { get; set; } = 0;

        /// <summary>
        /// 體重
        /// </summary>
        public int weight { get; set; } = 0;

        /// <summary>
        /// 角色身分
        /// </summary>
        public string identity { get; set; } = "";

        /// <summary>
        /// 帳號狀態
        /// </summary>
        public int status { get; set; } = 0;

        /// <summary>
        /// 使用者圖片附檔名
        /// </summary>
        public string fileextension { get; set; } = "";
    }

    /// <summary>
    /// 產出令牌
    /// </summary>
    public class AuthLoginTokenVM
    {
        /// <summary>
        /// 訪問令牌
        /// </summary>
        public string accessToken { get; set; } = "";

        /// <summary>
        /// 令牌過期時間
        /// </summary>
        public long expires_in { get; set; } = 0;
    }
}
