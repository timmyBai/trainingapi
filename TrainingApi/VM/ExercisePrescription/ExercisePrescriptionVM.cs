﻿using System.Collections.Specialized;

namespace TrainingApi.VM.ExercisePrescription
{
    /// <summary>
    /// 最新運動處方-心肺適能 回傳模型
    /// </summary>
    public class LatestExercisePrescriptionCardiorespiratoryFitnessVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訊練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新運動處方-肌力與肌耐力 回傳模型
    /// </summary>
    public class LatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訊練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練部位
        /// </summary>
        public string trainingArea { get; set; } = "";

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 轉換最新運動處方-肌力與肌耐力 回傳模型
    /// </summary>
    public class ConvertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 訓練部位
        /// </summary>
        public List<string> trainingArea { get; set; } = new List<string>();

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新運動處方-柔軟度 回傳模型
    /// </summary>
    public class LatestExercisePrescriptionSoftnessVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新運動處方-平衡 回傳模型
    /// </summary>
    public class LatestExercisePrescriptionBalanceVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訊練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新運動處方 回傳模型
    /// </summary>
    public class LatestExercisePrescriptionVM
    {
        /// <summary>
        /// 運動處方名子
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 運動強度
        /// </summary>
        public string exerciseIntensity { get; set; } = "";

        /// <summary>
        /// 心率
        /// </summary>
        public string heartRate { get; set; } = "";

        /// <summary>
        /// 訓練頻率
        /// </summary>
        public string trainingFrequency { get; set; } = "";

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 最新運動處方-肌力與肌耐力 回傳模型
    /// </summary>
    public class LatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingAreaVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 訓練部位
        /// </summary>
        public string trainingArea { get; set; } = "";

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }

    /// <summary>
    /// 轉換最新運動處方-肌力與肌耐力 回傳模型
    /// </summary>
    public class ConvertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingAreaVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 訓練部位清單
        /// </summary>
        public List<string> trainingArea { get; set; } = new List<string>();

        /// <summary>
        /// 更新紀錄日期
        /// </summary>
        public string updateRecordDate { get; set; } = "";

        /// <summary>
        /// 更新紀錄時間
        /// </summary>
        public string updateRecordTime { get; set; } = "";
    }
}
