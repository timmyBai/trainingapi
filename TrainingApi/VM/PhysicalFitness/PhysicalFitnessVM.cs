namespace TrainingApi.VM.PhysicalFitness
{
    /// <summary>
    /// 體適能-心肺適能
    /// </summary>
    public class PhysicalFitnessCardiorespiratoryFitnessVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 登階測驗
        /// </summary>
        public double setUpTest { get; set; } = 0.0;

        /// <summary>
        /// 原地站立抬膝
        /// </summary>
        public int kneeLiftFrequency { get; set; } = 0;
    }

    /// <summary>
    /// 體適能-肌力與肌耐力
    /// </summary>
    public class PhysicalFitnessMuscleStrengthAndMuscleEnduranceVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 屈膝仰臥起坐
        /// </summary>
        public int kneeCrunchesFrequency { get; set; } = 0;

        /// <summary>
        /// 手臂彎舉(上肢)
        /// </summary>
        public int armCurlUpperBodyFrequency { get; set; } = 0;

        /// <summary>
        /// 手臂彎舉(下肢)
        /// </summary>
        public int armCurlLowerBodyFrequency { get; set; } = 0;
    }

    /// <summary>
    /// 體適能-柔軟度
    /// </summary>
    public class PhysicalFitnessSoftnessVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 坐姿體前彎第一次距離
        /// </summary>
        public double seatedForwardBendOne { get; set; } = 0.0;

        /// <summary>
        /// 坐姿體前彎第二次距離
        /// </summary>
        public double seatedForwardBendTwo { get; set; } = 0.0;

        /// <summary>
        /// 坐姿體前彎第三次距離
        /// </summary>
        public double seatedForwardBendThree { get; set; } = 0.0;

        /// <summary>
        /// 椅子坐姿體前彎第一次距離
        /// </summary>
        public double chairSeatedForwardBendOne { get; set; } = 0.0;

        /// <summary>
        /// 椅子坐姿體前彎第二次距離
        /// </summary>
        public double chairSeatedForwardBendTwo { get; set; } = 0.0;

        /// <summary>
        /// 椅子坐姿體前彎第三次距離
        /// </summary>
        public double chairSeatedForwardBendThree { get; set; } = 0.0;

        /// <summary>
        /// 拉背測驗第一次距離
        /// </summary>
        public double pullBackTestOne { get; set; } = 0.0;

        /// <summary>
        /// 拉背測驗第二次距離
        /// </summary>
        public double pullBackTestTwo { get; set; } = 0.0;
    }

    /// <summary>
    /// 體適能-平衡
    /// </summary>
    public class PhysicalFitnessBalanceVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 壯年開眼足立
        /// </summary>
        public double adultEyeOpeningMonopodSecond { get; set; }

        /// <summary>
        /// 老年開眼足立
        /// </summary>
        public double elderlyEyeOpeningMonopodSecond { get; set; }

        /// <summary>
        /// 第一次坐立繞物
        /// </summary>
        public double sittingAroundOneSecond { get; set; }

        /// <summary>
        /// 第二次坐立繞物
        /// </summary>
        public double sittingAroundTwoSecond { get; set; }
    }

    /// <summary>
    /// 體適能各項等級
    /// </summary>
    public class LatestPhysicalFitnessVM
    {
        /// <summary>
        /// 心肺適能百分等級
        /// </summary>
        public int cardiorespiratoryFitnessLevel { get; set; } = 0;

        /// <summary>
        /// 心肺適能分數評語
        /// </summary>
        public string cardiorespiratoryFitnessComments { get; set; } = "";

        /// <summary>
        /// 肌力與肌耐力百分等級
        /// </summary>
        public int muscleStrengthAndMuscleEnduranceLevel { get; set; } = 0;

        /// <summary>
        /// 肌力與肌耐力分數評語
        /// </summary>
        public string muscleStrengthAndMuscleEnduranceComments { get; set; } = "";

        /// <summary>
        /// 柔軟度百分等級
        /// </summary>
        public int softnessLevel { get; set; } = 0;

        /// <summary>
        /// 柔軟度分數評語
        /// </summary>
        public string softnessComments { get; set; } = "";

        /// <summary>
        /// 平衡百分等級
        /// </summary>
        public int balanceLevel { get; set; } = 0;

        /// <summary>
        /// 平衡分數評語
        /// </summary>
        public string balanceComments { get; set; } = "";
    }
}
