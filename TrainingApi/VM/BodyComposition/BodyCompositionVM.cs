namespace TrainingApi.VM.BodyMass
{
    /// <summary>
    /// 身體組成 bmi 回傳模型
    /// </summary>
    public class BodyCompositionBmiVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; }

        /// <summary>
        /// bmi 指數
        /// </summary>
        public double bmi { get; set; } = 0.0;

        /// <summary>
        /// bmi 評語
        /// </summary>
        public string bmiComment { get; set; } = "";
    }

    /// <summary>
    /// 身體組成 體脂肪 回傳模型
    /// </summary>
    public class BodyCompositionBodyFatVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; }

        /// <summary>
        /// 體脂肪 指數
        /// </summary>
        public double bodyFat { get; set; } = 0.0;

        /// <summary>
        /// 體脂肪 評語
        /// </summary>
        public string bodyFatComment { get; set; } = "";
    }

    /// <summary>
    /// 身體組成 內臟脂肪 回傳模型
    /// </summary>
    public class BodyCompositionVisceralFatVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; }

        /// <summary>
        /// 內臟脂肪 指數
        /// </summary>
        public double visceralFat { get; set; } = 0.0;

        /// <summary>
        /// 內臟脂肪 評語
        /// </summary>
        public string visceralFatComment { get; set; } = "";
    }

    public class BodyCompositionSkeletalMuscleRateVM
    {
        /// <summary>
        /// 使用者 id
        /// </summary>
        public int userAuthorizedId { get; set; }

        /// <summary>
        /// 骨骼肌率 指數
        /// </summary>
        public double skeletalMuscleRate { get; set; } = 0.0;

        /// <summary>
        /// 骨骼肌率 評語
        /// </summary>
        public string skeletalMuscleRateComment { get; set; } = "";
    }

    /// <summary>
    /// 最新身體質量回傳模型
    /// </summary>
    public class LatestBodyCompositionVM
    {
        /// <summary>
        /// bmi 指數
        /// </summary>
        public double bmi { get; set; } = 0.0;

        /// <summary>
        /// bmi 百分等級
        /// </summary>
        public string bmiComment { get; set; } = "";

        /// <summary>
        /// 體指率
        /// </summary>
        public double bodyFat { get; set;} = 0;

        /// <summary>
        /// 體指率百分等級
        /// </summary>
        public string bodyFatComment { get; set; } = "";
        
        /// <summary>
        /// 內臟脂肪
        /// </summary>
        public double visceralFat { get; set; } = 0.0;

        /// <summary>
        /// 內臟脂肪百分等級
        /// </summary>
        public string visceralFatComment { get; set; } = "";

        /// <summary>
        /// 骨骼肌率
        /// </summary>
        public double skeletalMuscleRate { get; set; } = 0.0;

        /// <summary>
        /// 骨骼肌率百分等級
        /// </summary>
        public string skeletalMuscleRateLevel { get; set; } = "";
    }
}
