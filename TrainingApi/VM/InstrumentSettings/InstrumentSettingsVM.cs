﻿namespace TrainingApi.VM.InstrumentSettings
{
    /// <summary>
    /// 取得最近運動表現
    /// </summary>
    public class FirstInstrumentSettingsSportsPerformanceVM
    {
        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;

        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;
        
        /// <summary>
        /// 活動範圍
        /// </summary>
        public int maxRange { get; set; } = 0;

        /// <summary>
        /// 速度陣列
        /// </summary>
        public string speed { get; set; } = "";
    }
    
    /// <summary>
    /// 取得目標次數
    /// </summary>
    public class ExercisePrescriptionMuscleStrengthAndMuscleEnduranceTargetNumber
    {
        /// <summary>
        /// 目標次數
        /// </summary>
        public int trainingUnitNumber { get; set; } = 0;
    }

    /// <summary>
    /// 取得最新運動表現
    /// </summary>
    public class LatestInstrumentSettingsSportsPerformanceVM
    {
        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;

        /// <summary>
        /// 活動範圍
        /// </summary>
        public int maxRange { get; set; } = 0;

        /// <summary>
        /// 最大運動表現速度
        /// </summary>
        public int maxSpeed { get; set; } = 0;

        /// <summary>
        /// 目標次數
        /// </summary>
        public int trainingUnitNumber { get; set; } = 0;
    }

    /// <summary>
    /// 今日是否有最大運動表現
    /// </summary>
    public class TodayInstrumentSettingsSportsPerformanceVM
    {
        /// <summary>
        /// 學員 id
        /// </summary>
        public int userAuthorizedId { get; set; } = 0;

        /// <summary>
        /// 機器編號
        /// </summary>
        public int machinaryCode { get; set; } = 0;
        
        /// <summary>
        /// 阻力等級
        /// </summary>
        public int level { get; set; } = 0;
        
        /// <summary>
        /// 完成次數
        /// </summary>
        public int finish { get; set; } = 0;
        
        /// <summary>
        /// 失誤次數
        /// </summary>
        public int mistake { get; set; } = 0;
        
        /// <summary>
        /// 活動範圍
        /// </summary>
        public int maxRange { get; set; } = 0;
        
        /// <summary>
        /// 功率
        /// </summary>
        public int power { get; set; } = 0;
        
        /// <summary>
        /// 速度陣列
        /// </summary>
        public string speed { get; set; } = "";

        /// <summary>
        /// 紀錄修改人
        /// </summary>
        public int newRecordId { get; set; } = 0;

        /// <summary>
        /// 紀錄日期
        /// </summary>
        public string newRecordDate { get; set; } = "";

        /// <summary>
        /// 紀錄時間
        /// </summary>
        public string newRecordTime { get; set; } = "";
    }
}
