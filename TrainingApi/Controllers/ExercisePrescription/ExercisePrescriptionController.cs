using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Net;
using TrainingApi.Models;
using TrainingApi.Models.ExercisePrescription;
using TrainingApi.Settings;
using TrainingApi.Utils;
using TrainingApi.VM.ExercisePrescription;

namespace TrainingApi.Controllers.ExercisePrescription
{
    [Route("Training")]
    [ApiController]
    public class ExercisePrescriptionController : ControllerBase
    {
        private ILogger<ExercisePrescriptionController> logger;
        private MySqlConnection? conn;

        public ExercisePrescriptionController(ILogger<ExercisePrescriptionController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得最新運動處方資訊
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpGet]
        [Authorize]
        [Route("api/exercisePrescription/latest")]
        public Task<IActionResult> LatestExercisePrescription()
        {
            ResultModels result = new ResultModels();
            
            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var memberInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int studentId = Convert.ToInt32(memberInfo["id"]);

                MysqlSetting setting = new MysqlSetting();

                using (conn = new MySqlConnection(setting.GetConnectionString()))
                {
                    conn.Open();

                    List<LatestExercisePrescriptionVM> latestExercisePrescription = new List<LatestExercisePrescriptionVM>();

                    // 取得最新運動處方-心肺適能資料
                    var latestExercisePrescriptionCardiorespiratoryFitness = new ExercisePrescriptionModels(conn).GetLatestExercisePrescriptionCardiorespiratoryFitness<LatestExercisePrescriptionCardiorespiratoryFitnessVM>(studentId);

                    if (latestExercisePrescriptionCardiorespiratoryFitness != null)
                    {
                        latestExercisePrescription.Add(new LatestExercisePrescriptionVM()
                        {
                            name = "心肺適能",
                            userAuthorizedId = latestExercisePrescriptionCardiorespiratoryFitness.userAuthorizedId,
                            exerciseIntensity = latestExercisePrescriptionCardiorespiratoryFitness.exerciseIntensity,
                            heartRate = latestExercisePrescriptionCardiorespiratoryFitness.heartRate,
                            trainingFrequency = latestExercisePrescriptionCardiorespiratoryFitness.trainingFrequency,
                            updateRecordDate = latestExercisePrescriptionCardiorespiratoryFitness.updateRecordDate,
                            updateRecordTime = latestExercisePrescriptionCardiorespiratoryFitness.updateRecordTime
                        });
                    }
                    else
                    {
                        latestExercisePrescription.Add(new LatestExercisePrescriptionVM()
                        {
                            name = "心肺適能",
                            userAuthorizedId = 0,
                            exerciseIntensity = "-",
                            heartRate = "-",
                            trainingFrequency = "-",
                            updateRecordDate = "-",
                            updateRecordTime = "-"
                        });
                    }

                    // 取得最新運動處方-肌力與肌耐力資料
                    var latestExercisePrescriptionMuscleStrengthAndMuscleEndurance = new ExercisePrescriptionModels(conn).GetLatestExercisePrescriptionMuscleStrengthAndMuscleEndurance<LatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceVM>(studentId);
                    
                    if (latestExercisePrescriptionMuscleStrengthAndMuscleEndurance != null)
                    {
                        latestExercisePrescription.Add(new LatestExercisePrescriptionVM()
                        {
                            name = "肌力與肌耐力",
                            userAuthorizedId = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.userAuthorizedId,
                            exerciseIntensity = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.exerciseIntensity,
                            heartRate = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.heartRate,
                            trainingFrequency = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.trainingFrequency,
                            updateRecordDate = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.updateRecordDate,
                            updateRecordTime = latestExercisePrescriptionMuscleStrengthAndMuscleEndurance.updateRecordTime
                        });
                    }
                    else
                    {
                        latestExercisePrescription.Add(new LatestExercisePrescriptionVM()
                        {
                            name = "肌力與肌耐力",
                            userAuthorizedId = 0,
                            exerciseIntensity = "-",
                            heartRate = "-",
                            trainingFrequency = "-",
                            updateRecordDate = "-",
                            updateRecordTime = "-"
                        });
                    }

                    // 取得最新運動處方-柔軟度資料
                    var latestExercisePrescriptionSoftness = new ExercisePrescriptionModels(conn).GetLatestExercisePrescriptionSoftness<LatestExercisePrescriptionSoftnessVM>(studentId);

                    if (latestExercisePrescriptionSoftness != null)
                    {
                        latestExercisePrescription.Add(new LatestExercisePrescriptionVM()
                        {
                            name = "柔軟度",
                            userAuthorizedId = latestExercisePrescriptionSoftness.userAuthorizedId,
                            exerciseIntensity = latestExercisePrescriptionSoftness.exerciseIntensity,
                            heartRate = latestExercisePrescriptionSoftness.heartRate,
                            trainingFrequency = latestExercisePrescriptionSoftness.trainingFrequency,
                            updateRecordDate = latestExercisePrescriptionSoftness.updateRecordDate,
                            updateRecordTime = latestExercisePrescriptionSoftness.updateRecordTime 
                        });
                    }
                    else
                    {
                        latestExercisePrescription.Add(new LatestExercisePrescriptionVM()
                        {
                            name = "柔軟度",
                            userAuthorizedId = 0,
                            exerciseIntensity = "-",
                            heartRate = "-",
                            trainingFrequency = "-",
                            updateRecordDate = "-",
                            updateRecordTime = "-"
                        });
                    }

                    // 取得最新運動處方-平衡資料
                    var latsetExercisePrescriptionBalance = new ExercisePrescriptionModels(conn).GetLatestExercisePrescriptionBalance<LatestExercisePrescriptionBalanceVM>(studentId);

                    if (latsetExercisePrescriptionBalance != null)
                    {
                        latestExercisePrescription.Add(new LatestExercisePrescriptionVM()
                        {
                            name = "平衡",
                            userAuthorizedId = latsetExercisePrescriptionBalance.userAuthorizedId,
                            exerciseIntensity = latsetExercisePrescriptionBalance.exerciseIntensity,
                            heartRate = latsetExercisePrescriptionBalance.heartRate,
                            trainingFrequency = latsetExercisePrescriptionBalance.trainingFrequency,
                            updateRecordDate = latsetExercisePrescriptionBalance.updateRecordDate,
                            updateRecordTime = latsetExercisePrescriptionBalance.updateRecordTime
                        });
                    }
                    else
                    {
                        latestExercisePrescription.Add(new LatestExercisePrescriptionVM()
                        {
                            name = "平衡",
                            userAuthorizedId = 0,
                            exerciseIntensity = "-",
                            heartRate = "-",
                            trainingFrequency = "-",
                            updateRecordDate = "-",
                            updateRecordTime = "-"
                        });
                    }

                    result.isSuccess = true;
                    result.data = latestExercisePrescription;
                    logger.LogInformation("GetLatestExercisePrescriptionSuccess");
                    return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.OK, result));
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, result));
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 取得最新運動處方-肌力與肌耐力的訓練部位資訊
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpGet]
        [Authorize]
        [Route("api/exercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea/latest")]
        public Task<IActionResult> LatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea()
        {
            ResultModels result = new ResultModels();

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var memberInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int studentId = Convert.ToInt32(memberInfo["id"]);

                MysqlSetting setting = new MysqlSetting();

                using (conn = new MySqlConnection(setting.GetConnectionString()))
                {
                    conn.Open();

                    ConvertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingAreaVM convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea = new ConvertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingAreaVM();

                    // 取得最新運動處方-肌力與肌耐力訓練部位資料
                    var latestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea = new ExercisePrescriptionModels(conn).GetLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea<LatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingAreaVM>(studentId);

                    if (latestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea != null)
                    {
                        convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.userAuthorizedId = latestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.userAuthorizedId;
                        convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.trainingArea = JsonConvert.DeserializeObject<List<string>>(latestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.trainingArea);
                        convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.updateRecordDate = latestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.updateRecordDate;
                        convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.updateRecordTime = latestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.updateRecordTime;
                    }
                    else
                    {
                        convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.userAuthorizedId = 0;
                        convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.trainingArea = new List<string>();
                        convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.updateRecordDate = "-";
                        convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea.updateRecordTime = "-";
                    }

                    result.isSuccess = true;
                    result.data = convertLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea;
                    logger.LogInformation("GetLatestExercisePrescriptionMuscleStrengthAndMuscleEnduranceTrainingArea");
                    return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.OK, result));
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, result));
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
