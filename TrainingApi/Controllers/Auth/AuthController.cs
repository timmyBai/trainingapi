using System.Net;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Microsoft.AspNetCore.Cors;

using TrainingApi.Models;
using TrainingApi.Models.Auth;
using TrainingApi.Services;
using TrainingApi.Utils;
using TrainingApi.Settings;
using TrainingApi.VM.Auth;

namespace TrainingApi.Controllers.Auth
{
    [Route("Training")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private MySqlConnection? conn;
        private readonly ILogger<AuthController> logger;
        private readonly HostService hostService;

        public AuthController(HostService _hostService, ILogger<AuthController> _logger)
        {
            hostService = _hostService;
            logger = _logger;
        }

        /// <summary>
        /// 會員登入
        /// </summary>
        /// <param name="body">接收會員模型</param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [AllowAnonymous]
        [HttpPost]
        [Route("api/motherboard/auth/login")]
        public Task<IActionResult> AuthLogin(RequestAuthLoginModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.BadRequest, result));
            }

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查帳號密碼
                    var userInfo = new AuthModels(conn).InspactUserAuthSecret<InspactAuthUserVM>(body);

                    // 如果帳號不存在
                    if (userInfo == null)
                    {
                        result.isSuccess = false;
                        result.message = "NoSuchUser";
                        logger.LogError("NoSuchUser");
                        return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.Forbidden, result));
                    }

                    // 如果帳號停用
                    if (userInfo.status == 0)
                    {
                        result.isSuccess = false;
                        result.message = "AccountDisabled";
                        logger.LogError("AccountDisabled");
                        return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.Gone, result));
                    }

                    // 使用者 id
                    string userId = userInfo.userAuthorizedId.ToString();

                    // 圖片 url
                    string fileImageUrl = string.Empty;

                    // 檢查檔案是否存在
                    var userImageInfo = new ImageUtils().InspactImageFilePathDoesNotExist(userId);

                    if (userImageInfo != null)
                    {
                        for (int i = 0; i < userImageInfo.Count(); i++)
                        {
                            // 圖片名稱
                            string fileName = Path.GetFileNameWithoutExtension(userImageInfo[0].ToString());

                            // 圖片副檔名
                            string fileExtension = Path.GetExtension(userImageInfo[0].ToString());

                            if (fileName == userInfo.name)
                            {
                                fileImageUrl = $@"{hostService.Domain}{hostService.ImagePath}/{userId}/{fileName}{fileExtension}";
                                break;
                            }
                        }
                    }
                    else
                    {
                        fileImageUrl = "";
                    }

                    // 加密敏感資料
                    Claim[] claim = new[]
                    {
                        new Claim("id", userInfo.userAuthorizedId.ToString()),
                        new Claim("account", userInfo.account),
                        new Claim("name", userInfo.name),
                        new Claim("phone", userInfo.phone),
                        new Claim("imageUrl", fileImageUrl),
                        new Claim("_gender", userInfo.gender),
                        new Claim("birthday", userInfo.birthday.ToString()),
                        new Claim("age", userInfo.age.ToString()),
                        new Claim("_email", userInfo.email),
                        new Claim("address", userInfo.address),
                        new Claim("height", userInfo.height.ToString()),
                        new Claim("weight", userInfo.weight.ToString()),
                        new Claim("identity", userInfo.identity),
                        new Claim(JwtRegisteredClaimNames.Nbf, $"{new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()}"),
                        new Claim(JwtRegisteredClaimNames.Exp, $"{new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds()}")
                    };

                    // 建立 token 模型
                    AuthLoginTokenVM tokenAuthVM = new AuthLoginTokenVM();
                    tokenAuthVM.accessToken = new JwtUtils().GenerateJwtToken(claim);
                    tokenAuthVM.expires_in = Convert.ToInt64(new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds().ToString().PadRight(13, '0'));

                    // 統一資料壓縮回傳
                    result.isSuccess = true;
                    result.data = tokenAuthVM;
                    logger.LogInformation("SendTokenSuccess");
                    return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.OK, result));
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, result));
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// </summary>
        /// <param name="body">接收會員模型</param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [AllowAnonymous]
        [HttpPost]
        [Route("api/motherboard/auth/phone/login")]
        public Task<IActionResult> AuthPhoneLogin(RequestAuthPhoneLoginModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.BadRequest, result));
            }

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    // 檢查帳號密碼
                    var userInfo = new AuthModels(conn).InspactUserAuthPhoneSecret<InspactAuthUserVM>(body);

                    // 如果帳號不存在
                    if (userInfo == null)
                    {
                        result.isSuccess = false;
                        result.message = "NoSuchUser";
                        logger.LogError("NoSuchUser");
                        return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.Forbidden, result));
                    }

                    // 如果帳號停用
                    if (userInfo.status == 0)
                    {
                        result.isSuccess = false;
                        result.message = "AccountDisabled";
                        logger.LogError("AccountDisabled");
                        return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.Gone, result));
                    }

                    // 使用者 id
                    string userId = userInfo.userAuthorizedId.ToString();

                    // 圖片 url
                    string fileImageUrl = string.Empty;

                    // 檢查檔案是否存在
                    var userImageInfo = new ImageUtils().InspactImageFilePathDoesNotExist(userId);

                    if (userImageInfo != null)
                    {
                        for (int i = 0; i < userImageInfo.Count(); i++)
                        {
                            // 圖片名稱
                            string fileName = Path.GetFileNameWithoutExtension(userImageInfo[0].ToString());

                            // 圖片副檔名
                            string fileExtension = Path.GetExtension(userImageInfo[0].ToString());

                            if (fileName == userInfo.name)
                            {
                                fileImageUrl = $@"{hostService.Domain}{hostService.ImagePath}/{userId}/{fileName}{fileExtension}";
                                break;
                            }
                        }
                    }
                    else
                    {
                        fileImageUrl = "";
                    }

                    // 加密敏感資料
                    Claim[] claim = new[]
                    {
                        new Claim("id", userInfo.userAuthorizedId.ToString()),
                        new Claim("account", userInfo.account),
                        new Claim("name", userInfo.name),
                        new Claim("phone", userInfo.phone),
                        new Claim("imageUrl", fileImageUrl),
                        new Claim("_gender", userInfo.gender),
                        new Claim("birthday", userInfo.birthday.ToString()),
                        new Claim("age", userInfo.age.ToString()),
                        new Claim("_email", userInfo.email),
                        new Claim("address", userInfo.address),
                        new Claim("height", userInfo.height.ToString()),
                        new Claim("weight", userInfo.weight.ToString()),
                        new Claim("identity", userInfo.identity),
                        new Claim(JwtRegisteredClaimNames.Nbf, $"{new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds()}"),
                        new Claim(JwtRegisteredClaimNames.Exp, $"{new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds()}")
                    };

                    // 建立 token 模型
                    AuthLoginTokenVM tokenAuthVM = new AuthLoginTokenVM();
                    tokenAuthVM.accessToken = new JwtUtils().GenerateJwtToken(claim);
                    tokenAuthVM.expires_in = Convert.ToInt64(new DateTimeOffset(DateTime.Now).AddMinutes(30).ToUnixTimeSeconds().ToString().PadRight(13, '0'));

                    // 統一資料壓縮回傳
                    result.isSuccess = true;
                    result.data = tokenAuthVM;
                    logger.LogInformation("SendTokenSuccess");
                    return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.OK, result));
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, result));
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
