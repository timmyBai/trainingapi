using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System.Net;

using TrainingApi.Models;
using TrainingApi.Models.PhysicalFitness;
using TrainingApi.Settings;
using TrainingApi.Utils;
using TrainingApi.VM.PhysicalFitness;

// 體適能 api
namespace TrainingApi.Controllers.PhysicalFitness
{
    [Route("Training")]
    [ApiController]
    public class PhysicalFitnessController : ControllerBase
    {
        private MySqlConnection? conn;
        private ILogger<PhysicalFitnessController> logger;

        public PhysicalFitnessController(ILogger<PhysicalFitnessController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得最新體適能等級
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/physicalFitness/latest")]
        public Task<IActionResult> LatestPhysicalFitness()
        {
            ResultModels result = new ResultModels();

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var userInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int userAuthorizedId = Convert.ToInt32(userInfo["id"]);
                int age = Convert.ToInt32(DateTime.Now.ToString("yyyy")) - Convert.ToInt32(userInfo["birthday"].Substring(0, 4));
                string gender = userInfo["_gender"].ToString();

                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    LatestPhysicalFitnessVM physicalFitnessLevelVM = new LatestPhysicalFitnessVM();

                    var firstCardiorespiratoryFitness = new PhysicalFitnessModels(conn).GetFirstPhysicalFitnessCardiorespiratoryFitness<PhysicalFitnessCardiorespiratoryFitnessVM>(userAuthorizedId);

                    var firstMuscleStrengthAndMuscleEndurance = new PhysicalFitnessModels(conn).GetFirstPhysicalFitnessMuscleStrengthAndMuscleEndurance<PhysicalFitnessMuscleStrengthAndMuscleEnduranceVM>(userAuthorizedId);

                    var firstPhysicalFitnessSoftness = new PhysicalFitnessModels(conn).GetFirstPhysicalFitnessSoftness<PhysicalFitnessSoftnessVM>(userAuthorizedId);

                    var firstPhysicalFitnessBalance = new PhysicalFitnessModels(conn).GetFirstPhysicalFitnessBalance<PhysicalFitnessBalanceVM>(userAuthorizedId);

                    if (age < 65)
                    {
                        // 如果體適能-心肺適能 有資料
                        if (firstCardiorespiratoryFitness != null)
                        {
                            physicalFitnessLevelVM.cardiorespiratoryFitnessLevel = new NormUtils().PhysicalFitnessSetUpTestLevel(gender, age, firstCardiorespiratoryFitness.setUpTest);
                        }

                        // 如果體適能-肌力與肌耐力 有資料
                        if (firstMuscleStrengthAndMuscleEndurance != null)
                        {
                            physicalFitnessLevelVM.muscleStrengthAndMuscleEnduranceLevel = new NormUtils().PhysicalFitnessKneeCrunchesFrequencyLevel(gender, age, firstMuscleStrengthAndMuscleEndurance.kneeCrunchesFrequency);
                        }

                        // 如果體適能-柔軟度 有資料
                        if (firstPhysicalFitnessSoftness != null)
                        {
                            physicalFitnessLevelVM.softnessLevel = new NormUtils().PhysicalFitnessSeatedForwardBend(gender, age, firstPhysicalFitnessSoftness.seatedForwardBendOne, firstPhysicalFitnessSoftness.seatedForwardBendTwo, firstPhysicalFitnessSoftness.seatedForwardBendThree);
                        }

                        // 如果體適能-平衡 有資料
                        if (firstPhysicalFitnessBalance != null)
                        {
                            physicalFitnessLevelVM.balanceLevel = new NormUtils().PhysicalFitnessAdultEyeOpeningMonopodSecond(age, firstPhysicalFitnessBalance.adultEyeOpeningMonopodSecond);
                        }

                        physicalFitnessLevelVM.cardiorespiratoryFitnessComments = physicalFitnessLevelVM.cardiorespiratoryFitnessLevel == 0 ? "未測試" : new NormUtils().FractionConvertComments(physicalFitnessLevelVM.cardiorespiratoryFitnessLevel, Enum.GetName(NormEnum.YoungManCardiorespiratoryFitness));
                        physicalFitnessLevelVM.muscleStrengthAndMuscleEnduranceComments = physicalFitnessLevelVM.muscleStrengthAndMuscleEnduranceLevel == 0 ? "未測試" : new NormUtils().FractionConvertComments(physicalFitnessLevelVM.muscleStrengthAndMuscleEnduranceLevel, Enum.GetName(NormEnum.YoungManMuscleStrengthAndMuscleEndurance));
                        physicalFitnessLevelVM.softnessComments = physicalFitnessLevelVM.softnessLevel == 0 ? "未測試" : new NormUtils().FractionConvertComments(physicalFitnessLevelVM.softnessLevel, Enum.GetName(NormEnum.YoungManSoftness));
                        physicalFitnessLevelVM.balanceComments = physicalFitnessLevelVM.balanceLevel == 0 ? "未測試" : new NormUtils().FractionConvertComments(physicalFitnessLevelVM.balanceLevel, Enum.GetName(NormEnum.YoungManBalance));
                    }
                    else
                    {
                        // 如果體適能-心肺適能 有資料
                        if (firstCardiorespiratoryFitness != null)
                        {
                            physicalFitnessLevelVM.cardiorespiratoryFitnessLevel = new NormUtils().PhysicalFitnessStandingKneeRaiseLevel(gender, age, firstCardiorespiratoryFitness.kneeLiftFrequency);
                        }

                        // 如果體適能-肌力與肌耐力 有資料
                        if (firstMuscleStrengthAndMuscleEndurance != null)
                        {
                            int armCurlUpperBodyFrequency = new NormUtils().PhysicalFitnessArmCurlUpperBodyLevel(gender, age, firstMuscleStrengthAndMuscleEndurance.armCurlUpperBodyFrequency);
                            int armCurlLowerBodyFrequency = new NormUtils().PhysicalFitnessArmCurlLowerBodyFrequencyLevel(gender, age, firstMuscleStrengthAndMuscleEndurance.armCurlLowerBodyFrequency);
                            physicalFitnessLevelVM.muscleStrengthAndMuscleEnduranceLevel = (armCurlLowerBodyFrequency + armCurlUpperBodyFrequency) / 2;
                        }

                        // 如果體適能-柔軟度 有資料
                        if (firstPhysicalFitnessSoftness != null)
                        {
                            int chairSeatedForwardBend = new NormUtils().PhysicalFitnessChairSeatedForwardBend(gender, age, firstPhysicalFitnessSoftness.chairSeatedForwardBendOne, firstPhysicalFitnessSoftness.chairSeatedForwardBendTwo, firstPhysicalFitnessSoftness.chairSeatedForwardBendThree); ;
                            int pullBackTest = new NormUtils().PhysicalFitnessPullBackTest(gender, age, firstPhysicalFitnessSoftness.pullBackTestOne, firstPhysicalFitnessSoftness.pullBackTestTwo); ;
                            physicalFitnessLevelVM.softnessLevel = (chairSeatedForwardBend + pullBackTest) / 2;
                        }

                        // 如果體適能-平衡 有資料
                        if (firstPhysicalFitnessBalance != null)
                        {
                            int elderlyEyeOpeningMonopod = new NormUtils().PhysicalFitnessElderlyEyeOpeningMonopodSecond(gender, age, firstPhysicalFitnessBalance.elderlyEyeOpeningMonopodSecond);
                            int sittingAround = new NormUtils().PhysicalFitnessSittingAroundSecond(gender, age, firstPhysicalFitnessBalance.sittingAroundOneSecond, firstPhysicalFitnessBalance.sittingAroundTwoSecond);
                            physicalFitnessLevelVM.softnessLevel = (elderlyEyeOpeningMonopod + sittingAround) / 2;
                        }

                        physicalFitnessLevelVM.cardiorespiratoryFitnessComments = physicalFitnessLevelVM.cardiorespiratoryFitnessLevel == 0 ? "未測試" : new NormUtils().FractionConvertComments(physicalFitnessLevelVM.cardiorespiratoryFitnessLevel, Enum.GetName(NormEnum.ElderlyCardiorespiratoryFitness));
                        physicalFitnessLevelVM.muscleStrengthAndMuscleEnduranceComments = physicalFitnessLevelVM.muscleStrengthAndMuscleEnduranceLevel == 0 ? "未測試" : new NormUtils().FractionConvertComments(physicalFitnessLevelVM.muscleStrengthAndMuscleEnduranceLevel, Enum.GetName(NormEnum.ElderlyMuscleStrengthAndMuscleEndurance));
                        physicalFitnessLevelVM.softnessComments = physicalFitnessLevelVM.softnessLevel == 0 ? "未測試" : new NormUtils().FractionConvertComments(physicalFitnessLevelVM.softnessLevel, Enum.GetName(NormEnum.ElderlySoftness));
                        physicalFitnessLevelVM.balanceComments = physicalFitnessLevelVM.balanceLevel == 0 ? "未測試" : new NormUtils().FractionConvertComments(physicalFitnessLevelVM.balanceLevel, Enum.GetName(NormEnum.ElderlySoftness));
                    }

                    result.isSuccess = true;
                    result.data = physicalFitnessLevelVM;
                    return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.OK, result));
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, result));
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
