using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

using TrainingApi.Models;
using TrainingApi.Models.BodyMass;
using TrainingApi.Settings;
using TrainingApi.Utils;
using TrainingApi.VM.BodyMass;

/*
 * 身體質量紀錄 api
 */
namespace TrainingApi.Controllers.BodyMass
{
    [Route("Training")]
    [ApiController]
    public class BodyCompositionController : ControllerBase
    {
        private readonly ILogger<BodyCompositionController> logger;
        private MySqlConnection? conn;

        public BodyCompositionController(ILogger<BodyCompositionController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得最新身體質量紀錄檔
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/bodyComposition/latest")]
        public Task<IActionResult> LatestBodyCompositionRecord()
        {
            ResultModels result = new ResultModels();

            try
            {
                MysqlSetting setting = new MysqlSetting();
                string connectionString = setting.GetConnectionString();

                using (conn = new MySqlConnection(connectionString))
                {
                    conn.Open();

                    var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                    var memberInfo = new JwtUtils().DecryptJwtKey(accessToken);

                    // 拿取使用者 id
                    int studentId = Convert.ToInt32(memberInfo["id"]);
                    string gender = memberInfo["_gender"].ToString();
                    int age = Convert.ToInt32(memberInfo["age"]);

                    LatestBodyCompositionVM latestBodyComposition = new LatestBodyCompositionVM();

                    // 取得最新 bmi 紀錄
                    var bodyCompositionBmi = new BodyCompositionModels(conn).GetLatestBodyCompositionBmi<BodyCompositionBmiVM>(studentId);

                    if (bodyCompositionBmi != null)
                    {
                        bodyCompositionBmi.bmiComment = new NormUtils().BmiPercentageLevel(gender, age, bodyCompositionBmi.bmi);
                        latestBodyComposition.bmi = bodyCompositionBmi.bmi;
                        latestBodyComposition.bmiComment = bodyCompositionBmi.bmiComment;
                    }
                    else
                    {
                        latestBodyComposition.bmi = 0;
                        latestBodyComposition.bmiComment = "未測試";
                    }

                    // 取得最新 體脂肪 紀錄
                    var bodyCompositionBodyFat = new BodyCompositionModels(conn).GetLatestBodyCompositionBodyFat<BodyCompositionBodyFatVM>(studentId);

                    if (bodyCompositionBodyFat != null)
                    {
                        bodyCompositionBodyFat.bodyFatComment = new NormUtils().BodyFatPercentageLevel(gender, age, bodyCompositionBmi.bmi);
                        latestBodyComposition.bodyFat = Math.Round(bodyCompositionBodyFat.bodyFat, 1);
                        latestBodyComposition.bodyFatComment = bodyCompositionBodyFat.bodyFatComment;
                    }
                    else
                    {
                        latestBodyComposition.bodyFat = 0.0;
                        latestBodyComposition.bodyFatComment = "未測試";
                    }

                    // 取得最新 內臟脂肪 紀錄
                    var bodyCompositionVisceralFat = new BodyCompositionModels(conn).GetLatestBodyCompositionVisceralFat<BodyCompositionVisceralFatVM>(studentId);

                    if (bodyCompositionVisceralFat != null)
                    {
                        bodyCompositionVisceralFat.visceralFatComment = new NormUtils().VisceralFatPercentageLevel(bodyCompositionVisceralFat.visceralFat);
                        latestBodyComposition.visceralFat = bodyCompositionVisceralFat.visceralFat;
                        latestBodyComposition.visceralFatComment = bodyCompositionVisceralFat.visceralFatComment;
                    }
                    else
                    {
                        latestBodyComposition.visceralFat = 0.0;
                        latestBodyComposition.visceralFatComment = "未測試";
                    }

                    var bodyCompositionSkeletalMuscleRate = new BodyCompositionModels(conn).GetLatestBodyCompositionSkeletalMuscleRate<BodyCompositionSkeletalMuscleRateVM>(studentId);

                    if (bodyCompositionSkeletalMuscleRate != null)
                    {
                        bodyCompositionSkeletalMuscleRate.skeletalMuscleRateComment = new NormUtils().SkeletalMuscleRatePercentageLevel(gender, bodyCompositionSkeletalMuscleRate.skeletalMuscleRate);
                        latestBodyComposition.skeletalMuscleRate = bodyCompositionSkeletalMuscleRate.skeletalMuscleRate;
                        latestBodyComposition.skeletalMuscleRateLevel = bodyCompositionSkeletalMuscleRate.skeletalMuscleRateComment;
                    }
                    else
                    {
                        latestBodyComposition.skeletalMuscleRate = 0.0;
                        latestBodyComposition.skeletalMuscleRateLevel = "未測試";
                    }

                    result.isSuccess = true;
                    result.data = latestBodyComposition;
                    logger.LogInformation("GetLatestBodyMassRecord");
                    return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.OK, result));
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, result));
            }
        }
    }
}
