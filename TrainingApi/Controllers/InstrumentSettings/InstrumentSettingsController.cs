using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Net;
using TrainingApi.Models;
using TrainingApi.Models.InstrumentSettings;
using TrainingApi.Settings;
using TrainingApi.Utils;
using TrainingApi.VM.InstrumentSettings;

/*
 * 儀器設定
 */
namespace TrainingApi.Controllers.InstrumentSettings
{
    [Route("Training")]
    [ApiController]
    public class InstrumentSettingsController : ControllerBase
    {
        private MySqlConnection? conn;
        private ILogger<InstrumentSettingsController> logger;

        public InstrumentSettingsController(ILogger<InstrumentSettingsController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得學員是否使用此台機器
        /// </summary>
        /// <param name="machinaryCode"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpGet]
        [Route("api/instrumentSettingsExistMachinary/{machinaryCode:int}/fetch")]
        public Task<IActionResult> GetInstrumentSettingsExistMachinary(int machinaryCode)
        {
            ResultModels result = new ResultModels();

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var memberInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int studentId = Convert.ToInt32(memberInfo["id"]);

                MysqlSetting setting = new MysqlSetting();

                using (conn = new MySqlConnection(setting.GetConnectionString()))
                {
                    conn.Open();

                    LatestInstrumentSettingsSportsPerformanceVM latestInstrumentSettingsSportsPerformance = new LatestInstrumentSettingsSportsPerformanceVM();

                    // 檢查學員是否使用此機器
                    var instrumentSettings = new InstrumentSettingsModels(conn).InspectInstrumentSettingsExistMachinary<FirstInstrumentSettingsSportsPerformanceVM>(studentId, machinaryCode);
                    
                    if (instrumentSettings == null)
                    {
                        latestInstrumentSettingsSportsPerformance.level = 0;
                        latestInstrumentSettingsSportsPerformance.maxRange = 0;
                        latestInstrumentSettingsSportsPerformance.maxRange = 0;
                    }
                    else
                    {
                        List<int> speed = JsonConvert.DeserializeObject<List<int>>(instrumentSettings.speed);
                        for (int i = 0; i < speed.Count(); i++)
                        {
                            speed[i] = Math.Abs(speed[i]);
                        }

                        speed.Sort();
                        latestInstrumentSettingsSportsPerformance.maxSpeed = speed[speed.Count - 1];
                        latestInstrumentSettingsSportsPerformance.level = instrumentSettings.level;
                        latestInstrumentSettingsSportsPerformance.maxRange = instrumentSettings.maxRange;
                    }

                    // 取得運動處方-肌力與肌耐力訓練目標次數
                    var targetNumber = new InstrumentSettingsModels(conn).GetExercisePrescriptionMuscleStrengthAndMuscleEnduranceTargetNumber<ExercisePrescriptionMuscleStrengthAndMuscleEnduranceTargetNumber>(studentId);
                    
                    // 如果沒有開立運動處方
                    if (targetNumber == null)
                    {
                        latestInstrumentSettingsSportsPerformance.trainingUnitNumber = 12;
                    }
                    else
                    {
                        latestInstrumentSettingsSportsPerformance.trainingUnitNumber = targetNumber.trainingUnitNumber;
                    }

                    result.isSuccess = true;
                    result.data = latestInstrumentSettingsSportsPerformance;
                    return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.OK, result));
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, result));
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增或更新儀器設定 最大運動表現
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpPut]
        [Authorize]
        [Route("api/instrumentSettings/maxSportsPerformance/add")]
        public Task<IActionResult> InsertInstrumentSettingsSportsPerformance(RequestInsertInstrumentSettingsSportsPerformanceModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.BadRequest, result));
            }
            
            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var memberInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int studentId = Convert.ToInt32(memberInfo["id"]);

                MysqlSetting setting = new MysqlSetting();

                using (conn = new MySqlConnection(setting.GetConnectionString()))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    string today = new DayUtils().GetFullDate();

                    // 取得今日是否有最大運動表現
                    var instrumentSettings = new InstrumentSettingsModels(conn).GetTodayInstrumentSettingsSportsPerformance<TodayInstrumentSettingsSportsPerformanceVM>(studentId, today, body);

                    if (instrumentSettings == null)
                    {
                        new InstrumentSettingsModels(conn).InsertInstrumentSettingsSportsPerformance(studentId, body);

                        transaction.Commit();

                        result.isSuccess = true;
                        result.message = "InsertInsertInstrumentSettingsSuccess";
                        logger.LogInformation("InsertInsertInstrumentSettingsSuccess");
                        return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.Created, result));
                    }
                    else
                    {
                        new InstrumentSettingsModels(conn).UpdateInstrumentSettingsSportsPerformance(studentId, today, body);

                        transaction.Commit();

                        result.isSuccess = true;
                        result.message = "UpdateInsertInstrumentSettingsSuccess";
                        logger.LogInformation("UpdateInsertInstrumentSettingsSuccess");
                        return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.OK, result));
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, result));
            }
            finally
            {
                conn?.Dispose();
            }
        }

        /// <summary>
        /// 新增儀器設定 正式訓練
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [HttpPost]
        [Authorize]
        [Route("api/instrumentSettings/formalTraining/add")]
        public Task<IActionResult> InsertInstrumentSettingsFormalTraining(RequestInsertInstrumentSettingFormalTrainingModels body)
        {
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "BadRequest";
                logger.LogError("BadRequest");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.BadRequest, result));
            }

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var memberInfo = new JwtUtils().DecryptJwtKey(accessToken);
                int studentId = Convert.ToInt32(memberInfo["id"]);

                MysqlSetting setting = new MysqlSetting();

                using (conn = new MySqlConnection(setting.GetConnectionString()))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    // 新增正式訓練
                    var insertInstrumentSettingsRow = new InstrumentSettingsModels(conn).InsertInstrumentSettingsFormalTraining(studentId, body);

                    if (insertInstrumentSettingsRow == 1)
                    {
                        transaction.Commit();

                        result.isSuccess = true;
                        result.message = "InsertInstrumentSettingsSuccess";
                        logger.LogInformation("InsertInstrumentSettingsSuccess");
                        return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.Created, result));
                    }
                    else
                    {
                        transaction.Rollback();

                        result.isSuccess = true;
                        result.message = "InsertInstrumentSettingsFail";
                        logger.LogError("InsertInstrumentSettingsFail");
                        return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.Forbidden, result));
                    }
                }
            }
            catch(Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogError($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, result));
            }
            finally
            {
                conn?.Dispose();
            }
        }
    }
}
