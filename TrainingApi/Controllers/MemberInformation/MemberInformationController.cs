using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

using TrainingApi.Models;
using TrainingApi.Utils;

/*
 * 會員資訊 api
 */
namespace TrainingApi.Controllers.MemberInformation
{
    [Route("Training")]
    [ApiController]
    public class MemberInformationController : ControllerBase
    {
        private ILogger<MemberInformationController> logger;

        public MemberInformationController(ILogger<MemberInformationController> _logger)
        {
            logger = _logger;
        }

        /// <summary>
        /// 取得會員資料
        /// </summary>
        /// <returns></returns>
        [EnableCors("trainingBackstage")]
        [Authorize]
        [HttpPost]
        [Route("api/member/information")]
        public Task<IActionResult> MemberInformation()
        {
            ResultModels result = new ResultModels();

            try
            {
                var accessToken = Request.Headers.Authorization.ToString().Replace("Bearer ", "");
                var memberInfo = new JwtUtils().DecryptJwtKey(accessToken);

                if (memberInfo.Count == 0)
                {
                    result.isSuccess = false;
                    result.message = "TokenHasExpired";
                    logger.LogError("TokenHasExpired");
                    return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.Unauthorized, result));
                }

                result.isSuccess = true;
                result.data = memberInfo;
                
                logger.LogInformation($@"DecryptJwtKeySuccess: {memberInfo["account"]}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.OK, result));
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = "InternalServerError";
                logger.LogInformation($@"InternalServerError: {ex.ToString()}");
                return Task.FromResult<IActionResult>(StatusCode((int)HttpStatusCode.InternalServerError, ex.ToString()));
            }
        }
    }
}
