using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingApi.Tests.Models
{

    /// <summary>
    /// 統一回傳模型
    /// </summary>
    public class ResultModels
    {
        /// <summary>
        /// 使否成功
        /// </summary>
        public bool isSuccess { get; set; }

        /// <summary>
        /// 訊息
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 資料
        /// </summary>
        public object data { get; set; }

        public ResultModels()
        {
            isSuccess = false;
            message = string.Empty;
            data = new object();
        }
    }
}
