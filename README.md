# Training Api

<div>
    <img src="https://img.shields.io/badge/C%23-6.0.0-blue">
    <img src="https://img.shields.io/badge/.NetCore-6.0.0-blue">
    <img src="https://img.shields.io/badge/WebApi-6.0.0-blue">
</div>

## 簡介

Training Api 是一個等速肌力訓練後端解決方案，他基於 C#、.Net Core 6、Web Api 實現，他使用最新後端技術，內置了 token 登入權限解決方案，資料庫連線，跨域連線，Swagger，輕量級、高效能且模組化的 HTTP 要求管線，跨平台相容性，提供了許多後端處理方法，他可以幫助你快速搭建後端原型，可以幫助你解決許多需求。本專案針對等速肌力專案釋出相對應 api，提供主機板前台需求，操作相關資料

## 功能

```tex
- 會員登入與註冊
    - 會員帳密登入
    - 會員手機登入
- 會員資訊
    - 取得會員資訊
- 身體質量
    - 取得最新身體質量紀錄
- 體適能訓練
    - 取得最新體適能百分等級
- 運動處方
    - 取得最新運動處方資訊
    - 取得最新運動處方-肌力與肌耐力的訓練部位資訊
- 儀器設定
    - 取得學員是否使用此台機器
    - 新增儀器設定 最大運動表現
    - 新增儀器設定 正式訓練
```

## 開發

```git
git clone http://192.168.18.72/emgtech/training_api.git
```

## 本地發佈

```docker
# development(開發機)
docker-compose -f docker-compose.development.yml build && docker-compose -f docker-compose.development.yml up -d

# staging(demo 機)
docker-compose -f docker-compose.staging.yml build && docker-compose -f docker-compose.staging.yml up -d

# production(正式機)
docker-compose -f docker-compose.production.yml build && docker-compose -f docker-compose.production.yml up -d

# ship(出貨機)
docker-compose -f docker-compose.ship.yml build && docker-compose -f docker-compose.ship.yml up -d
```

## 佈署伺服器

| 環境          | 自動化佈署 | 必須設定 SSH | 必須安裝 docker | 必須安裝 docker-compose |
| ----------- | ----- | -------- | ----------- | ------------------- |
| development | √     | √        | √           | √                   |
| staging     | √     | √        | √           | √                   |
| production  | √     | √        | √           | √                   |
| ship        | √     | √        | √           | √                   |

## 自動化佈署流程

| 階段名稱                    | 工作名稱                              | 作用                          |
| ----------------------- | --------------------------------- | --------------------------- |
| testing                 | unit-tests                        | 單元測試方法                      |
| testing                 | test-build                        | 測試打包                        |
| build_development_image | build-to-docker-development-image | 打包 development docker image |
| deploy_development      | deploy-to-development             | 佈署開發機                       |
| build_staging_image     | build-to-docker-staging-image     | 打包 staging docker image     |
| deploy_staging          | deploy-to-staging                 | 佈署 demo 機                   |
| build_production_image  | build-to-docker-production-image  | 打包 production docker image  |
| deploy_production       | deploy-to-production              | 佈署正式機                       |
| build_ship_image        | build-to-docker-ship-image        | 打包 ship docker image        |
| deploy_ship             | deploy-to-ship                    | 佈署出貨機                       |

## GitLab Commit 格式

| 標籤       | 更改類型                        | 範例                                     |
| -------- | --------------------------- | -------------------------------------- |
| feat     | (feature)                   | feat(page): add page                   |
| fix      | (bug fix)                   | fix(page): add page bug                |
| docs     | (documentation)             | docs(documentation): add documentation |
| style    | (formatting, css, sass)     | style(sass): add page style            |
| refactor | (refactor)                  | refactor(page): refactor page          |
| test     | (when adding missing tests) | test(function): add function test      |
| chore    | (maintain)                  | chore(style): modify sass chore        |
