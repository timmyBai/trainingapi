export FILTER_DOCKER_NETWORK_NAME=`docker network ls | grep $DOCKER_NETWORK_NAME | awk '{ print $2 }'`

if ["$FILTER_DOCKER_NETWORK_NAME" == ""]; then
    docker network create --driver=bridge $DOCKER_NETWORK_NAME
fi